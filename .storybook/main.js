module.exports = {
  stories: ['../src/lib/**/*stories.tsx'],
  addons: ['@storybook/addon-docs', '@storybook/addon-controls'],
  webpackFinal: async (config, { configType }) => {
    config.resolve.alias = {
      ...config.resolve.alias,
    };
    config.resolve.modules.push(process.cwd() + '/src');
    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      loader: require.resolve('babel-loader'),
      options: {
        presets: [['react-app', { flow: false, typescript: true }]],
      },
    });
    config.resolve.extensions.push('.ts', '.tsx');
    // Return the altered config
    config.resolve.symlinks = false;
    return config;
  },
};
