import React from 'react';

import { addDecorator } from '@storybook/react';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';

//import store from 'store';

import theme from '../src/lib/theme';
import 'semantic-ui-css/semantic.min.css';

addDecorator((StoryFn) => (
  <ThemeProvider theme={theme}>
    <StoryFn />
  </ThemeProvider>
));

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    //expanded: true,
    presetColors: Object.keys(theme.colors)
      .filter((key) => typeof theme.colors[key] === 'string')
      .map((colorKey) => {
        return { color: theme.colors[colorKey], title: colorKey };
      }),
  },
};
