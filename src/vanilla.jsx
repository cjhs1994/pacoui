import React, { useState } from 'react';
import ReactDOM from 'react-dom';

import * as PacoUI from './index';
import Pjson from "../package.json";

export default(()=> {
  const Wrapper = ({ domElement }) => {
    const getProps = () => {
      return domElement && domElement?.props ? domElement.props : null;
    };

    const getComponent = () => {
      return domElement && domElement?.dataset?.component
        ? PacoUI[domElement.dataset.component]
        : null;
    };

    const [initialProps, setInitialProps] = useState(getProps());

    const observer = new MutationObserver(function (mutations) {
      mutations.forEach(function (mutation) {
        if (mutation.type === 'attributes') {
          setInitialProps({ ...getProps() });
        }
      });
    });

    observer.observe(domElement, {
      attributes: true, //configure it to listen to attribute changes
    });

    const Component = getComponent();

    if (!Component) return null;
    return <Component {...initialProps} />;
  };

  for (let i = 0; i < document.querySelectorAll("[id^='paco_ui']").length; i++) {
    const element = document.querySelectorAll("[id^='paco_ui']")[i];

    ReactDOM.render(
      <React.StrictMode>
        <PacoUI.ThemeProvider theme={PacoUI.Theme}>
          <Wrapper domElement={element} />
        </PacoUI.ThemeProvider>
      </React.StrictMode>,
      element,
      console.log(
        `%c ${Pjson?.name || 'paco_ui'}@${Pjson?.version || '0'}, Ubiwhere. All rights reserved.`,
        'background: white; color: #1B57FD; font-size: 20px;'
      )
    );
  }
})

