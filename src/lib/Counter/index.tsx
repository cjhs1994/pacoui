/**
 * Counter component
 *
 * @author Diogo Guedes <dguedes@ubiwhere.com>
 *
 */

import React, { useContext, useEffect, useState } from 'react';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faMinus } from '@fortawesome/pro-light-svg-icons';

import Button from '../Button';
import Text from '../Text';
import theme from '../theme';

//import { debounce } from 'utils';

interface IProps extends React.HTMLAttributes<HTMLInputElement> {
  error?: string;
  defaultValue?: any;
  name: string;
  onChange: (value: any) => void; // used to hook the form elements with useState
}

const Counter: React.FC<IProps> = ({ error, defaultValue, onChange, ...props }) => {
  /*const debounceChange = useMemo(
    debounce((value: string) => {
      onChange && onChange(value);
    }, 400),
    []
  );*/

  const [counter, setCounter] = useState(defaultValue || 1);

  useEffect(() => {
    onChange(counter);
  }, [counter]);

  return (
    <Wrapper>
      <Content>
        <CounterWrapper>
          <Button
            noPadding
            color={theme.colors.plusDarkGrey}
            borderless
            size="20px"
            onClick={() => {
              const count = counter - 1;
              if (count > 0) {
                setCounter(count);
              }
            }}
          >
            <ButtonPadding>
              <FontAwesomeIcon
                icon={faMinus}
                size={'xs'}
                color={theme.colors.plusDarkGrey}
                fontWeight={'bold'}
              />
            </ButtonPadding>
          </Button>
          <VerticalSeparator />
          <InputFieldWrapper>
            <Text color={'primary'} size={'small'} weight="regular">
              {counter}
            </Text>
          </InputFieldWrapper>
          <VerticalSeparator />
          <Button
            noPadding
            color={theme.colors.plusDarkGrey}
            borderless
            size="20px"
            onClick={() => {
              setCounter(counter + 1);
            }}
          >
            <ButtonPadding>
              <FontAwesomeIcon
                icon={faPlus}
                size={'xs'}
                color={theme.colors.plusDarkGrey}
                fontWeight={'bold'}
              />
            </ButtonPadding>
          </Button>
        </CounterWrapper>
      </Content>

      <MessageArea>
        {error && (
          <Text size="xSmall" weight="medium" color="dangerRed">
            {error}
          </Text>
        )}
      </MessageArea>
    </Wrapper>
  );
};

export default Counter;

const CounterWrapper = styled.div`
  display: flex;
  border: solid 1px ${({ theme }) => theme.colors.plusDarkGrey};
  height: 40px;
`;

const Content = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: 14px;
`;

const VerticalSeparator = styled.div`
  border-left: 1px solid ${({ theme }) => theme.colors.darkGrey};
  width: 0px;
  &&& {
    margin-top: 8px;
    margin-bottom: 8px;
  }
`;

const InputFieldWrapper = styled.div`
  width: 64px;
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ButtonPadding = styled.div`
  display: flex;
  align-items: center;
  padding: 0px 16px;
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const MessageArea = styled.div``;
