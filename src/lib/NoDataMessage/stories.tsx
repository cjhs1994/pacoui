import React from 'react';
import { storiesOf } from '@storybook/react';

import NoDataMessage from './index';

storiesOf('Components/NoDataMessage', module).add('No Data Message', () => (
  <NoDataMessage header={'Sem unidades curriculares'} />
));
