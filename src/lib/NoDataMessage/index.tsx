/**
 * NoDataMessage component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/pro-light-svg-icons';

import Text from '../Text';
import Segment from '../Segment';
import theme from '../theme';

interface INoDataMessage {
  header: string;
  body?: string | React.ReactNode;
  width?: string;
  marginTop?: string;
  type?: 'segment';
}

const NoDataMessage: React.FC<INoDataMessage> = ({ type, header, body, width, marginTop }) => {
  if (type === 'segment') {
    return (
      <Segment padding="50px 50px 50px 30px">
        <IconSegment>
          <FontAwesomeIcon icon={faInfoCircle} />
        </IconSegment>
        <HeaderSegment>
          <Text size={'xLarge'} lineHeight="1.2" weight={'regular'}>
            {header}
          </Text>
        </HeaderSegment>
        <BodySegment>
          <Text size={'article'} weight={'light'}>
            {body}
          </Text>
        </BodySegment>
      </Segment>
    );
  }

  return (
    <Wrapper width={width} marginTop={marginTop} type={type}>
      <Icon>
        <FontAwesomeIcon icon={faInfoCircle} />
      </Icon>
      <Header>
        <Text color="plusDarkGrey" size={'xLarge'} weight={'regular'}>
          {header}
        </Text>
      </Header>
      <Body>
        <Text color="plusDarkGrey" size={'article'} weight={'light'}>
          {body}
        </Text>
      </Body>
    </Wrapper>
  );
};

export default NoDataMessage;

const Wrapper = styled.div<{ width?: string; marginTop?: string; type?: string }>`
  width: ${({ width }) => (width ? width : `100%`)};
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
  flex: 1;
  margin-top: ${({ marginTop }) => marginTop};
  padding-top: ${({ type }) => (type === 'segment' ? '0px' : '100px')};
`;

const Icon = styled.div`
  font-size: 60px;
  color: ${({ theme }) => theme.colors.primary};
`;

const IconSegment = styled.div`
  font-size: 40px;
  color: ${({ theme }) => theme.colors.plusDarkGrey};
`;

const Header = styled.div`
  margin-top: 40px;
  text-align: center;
`;

const Body = styled.div`
  margin-top: 30px;
  text-align: center;
`;

const HeaderSegment = styled.div`
  margin-top: 20px;
  text-align: left;
  margin-bottom: 10px;
`;

const BodySegment = styled.div`
  text-align: left;
`;
