/**
 * FormInput container
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faCheck } from '@fortawesome/pro-regular-svg-icons';

import Button from '../Button';
import Text from '../Text';
import Input from '../Input';
import theme from '../theme';

interface IProps extends Partial<Omit<React.ComponentProps<typeof Input>, ''>> {
  decisionActions?: boolean;
  onCancel?: Function;
  onChangeClick?: Function;
  messageOffset?: number;
  error?: any;
  success?: any;
  border?: boolean;
  fontSize?:
    | 'xxSmall'
    | 'xSmall'
    | 'small'
    | 'article'
    | 'mediumSmall'
    | 'medium'
    | 'large'
    | 'xLarge'
    | 'xxLarge'
    | 'xxxLarge';
}

const FormInput: React.FC<IProps> = ({
  decisionActions,
  onCancel,
  messageOffset,
  onChangeClick,
  error,
  success,
  border,
  fontSize,
  ...props
}) => {
  return (
    <Wrapper>
      <InputWrapper border={border}>
        <InputAreaWrapper>
          <InputArea
            {...props}
            onEnter={() => {
              onChangeClick && onChangeClick();
            }}
            decisionActions={decisionActions}
            fontSize={fontSize}
          />
        </InputAreaWrapper>
        {decisionActions && (
          <ButtonWrapper>
            <Button
              color={theme.colors.plusDarkGrey}
              onClick={() => {
                onCancel && onCancel();
              }}
              borderless
            >
              <FontAwesomeIcon size="lg" icon={faTimes} />
            </Button>

            <Button
              color={theme.colors.plusDarkGrey}
              onClick={() => {
                onChangeClick && onChangeClick();
              }}
              borderless
            >
              <FontAwesomeIcon size="lg" icon={faCheck} />
            </Button>
          </ButtonWrapper>
        )}
      </InputWrapper>
      <MessageArea>
        {error && (
          <Error messageOffset={messageOffset || 0}>
            <Text size="xSmall" weight="medium" color="dangerRed">
              * {error}
            </Text>
          </Error>
        )}
        {success && (
          <Success messageOffset={messageOffset || 0}>
            <Text size="xSmall" weight="medium" color="successGreen">
              * {success}
            </Text>
          </Success>
        )}
      </MessageArea>
    </Wrapper>
  );
};

export default FormInput;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const MessageArea = styled.div``;

const Error = styled.div<{ messageOffset: number }>`
  margin-top: ${({ messageOffset }) => 4 + messageOffset}px;
`;

const Success = styled.div<{ messageOffset: number }>`
  margin-top: ${({ messageOffset }) => 4 + messageOffset}px;
`;

const InputWrapper = styled.div<{ border?: boolean }>`
  display: flex;
  align-items: center;
  border: ${({ border, theme }) => (border ? 'thin solid ' + theme.colors.darkGrey : '')};
  padding: ${({ border }) => (border ? '0px 4px 0px 4px' : '')};
`;

interface IInputArea {
  color?: string;
  decisionActions?: boolean;
  fontSize?: string;
}

const InputAreaWrapper = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  align-items: center;
`;

const InputArea = styled(Input)<IInputArea>`
  ${({ color, fontSize, theme }) => `
    ${color ? `color:${color};` : ''}
    font-size: ${fontSize ? theme.sizes[fontSize] : theme.sizes.medium};

    ::placeholder {
      font-size: ${fontSize ? theme.sizes[fontSize] : theme.sizes.medium};
    }
  `}
  height: 40px;
`;

const ButtonWrapper = styled.div`
  display: flex;
`;
