/**
 * Text component
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import AnimatedBackground from '../AnimatedBackground';

import theme from '../theme';

interface IProps {
  fontWeight?: string;
  lineHeight?: string;
  loading?: boolean;
  loadingProps?: any;
  loadingWidth?: string;
  children?: React.ReactNode;
  weight?: 'light' | 'regular' | 'medium' | 'bold' | String;
  color?: string;
  as?: string;
  ellipsize?: boolean;
  breakWord?: boolean;
  ellipsisCrop?: string;
  icon?: React.ReactNode;
  iconPadding?: string;
  iconWidth?: string;
  underline?: boolean;
  style?: any;
  onClick?: () => void;
  transform?: 'uppercase' | 'lowercase' | 'capitalize' | 'none';
  size?:
    | 'xxSmall'
    | 'xSmall'
    | 'small'
    | 'article'
    | 'mediumSmall'
    | 'medium'
    | 'large'
    | 'xLarge'
    | 'xxLarge'
    | 'xxxLarge'
    | string;
}

const Text: React.FC<IProps> = ({
  loading,
  loadingProps,
  children,
  color,
  icon,
  iconPadding,
  iconWidth,
  transform,
  as,
  underline,
  weight,
  ellipsize,
  ellipsisCrop,
  breakWord,
  size,
  ...props
}) => {
  const getLoadingSize = () => {
    return size ? theme.sizes[size] : '10px';
  };

  const text = (
    <TextWrapper
      ellipsize={ellipsize}
      ellipsisCrop={ellipsisCrop}
      transform={transform}
      weight={weight}
      color={color}
      size={size}
      icon={icon ? true : false}
      breakWord={breakWord}
      underline={underline}
    >
      {children}
    </TextWrapper>
  );

  return (
    <>
      {loading && (
        <AnimatedBackground
          height={getLoadingSize()}
          {...{ ...loadingProps, width: loadingProps?.width ? loadingProps.width : '100%' }}
        />
      )}

      {!loading && (
        <>
          {icon ? (
            <Wrapper>
              <Icon iconPadding={iconPadding} iconWidth={iconWidth}>
                {icon}
              </Icon>
              {text}
            </Wrapper>
          ) : (
            text
          )}
        </>
      )}
    </>
  );
};

export default Text;

const Wrapper = styled.div`
  display: inline-flex;
  align-items: center;
`;

interface ITextWrapper {
  weight?: string;
  color?: string;
  breakWord?: boolean;
  ellipsize?: boolean;
  ellipsisCrop?: string;
  icon?: boolean;
  underline?: boolean;
  childOf?: string;
}

const TextWrapper = styled.div<any>`
  display: ${({ breakWord }) => (!breakWord ? 'inline' : 'block')};
  max-width: ${({ breakWord }) => (!breakWord ? '100%' : '650px')};
  word-wrap: break-word;
  line-height: 1.3;
  ${({ icon }) => icon && 'line-height:initial;'}

  ${({ theme, weight }) => weight && `font-weight: ${theme.weight[weight]};`}
  ${({ color, theme }) => (color ? `color: ${theme.colors[color]};` : 'color: unset !important;')}
  ${({ transform }) => transform && ` text-transform: ${transform};`}
  ${({ size }) => size === 'xxSmall' && ` font-size: 0.625rem;`}
  ${({ underline }) => underline && ` text-decoration: underline;`}

  ${({ theme, underline, color }) => `
    a{
      color: ${theme.colors.primary};
      cursor: pointer !important;
      text-decoration: ${underline ? 'underline' : 'none !important'};

      :hover{
        text-decoration: underline !important;
      }
    }
  `}

  ${({ as, theme, color, underline }) =>
    as === 'a' &&
    `
      color: ${color ? theme.colors[color] : theme.colors.primary};
      cursor: pointer !important;
      text-decoration: ${underline ? 'underline' : 'none !important'};

      :hover{
        color: ${color ? theme.colors[color] : theme.colors.primary};
        text-decoration: underline !important;
      }
  `}

  ${({ ellipsize, ellipsisCrop }) =>
    ellipsize &&
    `
    max-width: calc(100% - ${ellipsisCrop ? ellipsisCrop : '0'});
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  `}
`;

const Icon = styled.div<{ iconPadding?: string; iconWidth?: string }>`
  margin-right: ${({ iconPadding }) => (iconPadding ? iconPadding : '8px')};
  display: flex;
  text-align: center;
  align-items: center;

  ${({ iconWidth }) =>
    iconWidth &&
    `
      width: ${iconWidth};

      svg {
        width: 100% !important;
      }
  `}
`;
