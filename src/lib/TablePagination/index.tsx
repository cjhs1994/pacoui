/**
 * Table Pagination Component
 *
 * @author Nuno Gago <ngago@ubiwhere.com>
 */

import React from 'react';
import styled from 'styled-components';
import { Menu, Icon } from 'semantic-ui-react';

import Text from '../Text';
import { default as DropdownComponent } from '../Dropdown';
import theme from '../theme';

import { ITablePagination, IRowsPerPage } from '../types';

export const ROWS_PER_PAGE: IRowsPerPage[] = [
  { text: '5', value: 5 },
  { text: '10', value: 10 },
  { text: '20', value: 20 },
];

const Pagination: React.FC<ITablePagination> = ({
  offset,
  limit,
  total,
  hasPrevious,
  hasNext,
  onChangeRows,
  onChangePage,
  translations,
}) => {
  const handleLabel = (): string => {
    const start = offset || 1;
    let end = offset + limit;
    end = end > total ? total : end;

    if (translations) {
      return translations('generic.tablePagination', {
        textOnly: true,
        total,
        start,
        end,
      });
    }

    return `${start} a ${end} de ${total}`;
  };

  return (
    <PaginationMenu floated="right">
      <PaginationMenuItem as="div">
        <Text size={'article'} color={theme.colors.grey}>
          {translations && translations('generic.tableResultsPerPage', { textOnly: true })}
        </Text>
        <Dropdown
          inline
          borderless
          options={ROWS_PER_PAGE}
          value={limit}
          onChange={(e, { value }) => onChangeRows(value)}
        />
        <Text size={'article'} color={theme.colors.grey}>
          {handleLabel()}
        </Text>
      </PaginationMenuItem>

      <PaginationMenuItem icon disabled={!hasPrevious} onClick={() => onChangePage('prev')}>
        <Icon name="chevron left" />
      </PaginationMenuItem>

      <PaginationMenuItem icon disabled={!hasNext} onClick={() => onChangePage('next')}>
        <Icon name="chevron right" />
      </PaginationMenuItem>
    </PaginationMenu>
  );
};

export default Pagination;

const PaginationMenu = styled(Menu)`
  &&&& {
    width: 100%;
    border: none;
    box-shadow: none;
    justify-content: flex-end;
    background: #f8f8f8;
  }
`;

const Dropdown = styled(DropdownComponent)`
  padding-left: 10px !important;
  padding-right: 10px !important;
  margin-right: 8px;
  margin-left: 4px;

  &:hover {
    background: rgba(0, 0, 0, 0.03);

    div:first-child {
      color: ${({ theme }) => theme.colors.primary} !important;
    }

    div:nth-child(2) svg path {
      fill: ${({ theme }) => theme.colors.primary};
    }
  }

  div:first-child {
    color: ${({ theme }) => theme.colors.grey} !important;
    font-weight: 400 !important;
  }

  div:nth-child(2) {
    margin-left: 8px;

    svg path {
      fill: ${({ theme }) => theme.colors.grey};
    }
  }
`;

const PaginationMenuItem = styled(Menu.Item)`
  &&&& {
    display: flex;
    align-items: center;

    &:before {
      display: none;
    }

    i {
      color: ${({ theme }) => theme.colors.grey} !important;
      font-size: 14px !important;
    }

    &.disabled {
      i {
        color: #ddd !important;
      }
    }

    &:hover:not(.disabled) {
      i {
        color: ${({ theme }) => theme.colors.primary} !important;
      }
    }
  }
`;
