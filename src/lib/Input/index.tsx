/**
 * Input component
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import theme from '../theme';

interface IProps {
  onEnter?: () => void;
}

const Input: React.FC<any> = ({ onEnter, register, ...props }) => {
  return (
    <InputArea
      {...props}
      {...register}
      type="text"
      onKeyDown={({ key }) => {
        key === 'Enter' && onEnter && onEnter();
      }}
    />
  );
};

export default Input;

const InputArea = styled.input`
  width: 100%;
  height: 100%;
  padding: 8px;
  border: none;
  background-color: ${({ theme }) => theme.colors.white};
  outline: none;

  &:focus {
    border: none;
    outline: none;
  }
`;
