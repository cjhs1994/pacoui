/**
 * Checkbox component
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import Text from '../Text';

import theme from '../theme';

interface IProps extends React.HTMLAttributes<HTMLInputElement> {
  inverted?: boolean;
  children?: string | React.ReactNode;
  disabled?: boolean;
}

const Checkbox: React.FC<any> = ({ disabled, inverted, children, ...props }) => {
  return (
    <Label hasLabel={children ? true : false} disabled={disabled}>
      {children && typeof children === 'string' ? (
        <Text color={inverted ? theme.colors.white : theme.colors.plusDarkGrey}>{children}</Text>
      ) : (
        children
      )}
      <Input inverted={inverted} type="checkbox" disabled={disabled ? true : false} {...props} />
      <CustomInput inverted={inverted} />
    </Label>
  );
};
export default Checkbox;

const Label = styled.label<{ hasLabel: boolean; disabled?: boolean }>`
  display: block;
  position: relative;
  cursor: pointer;
  min-height: 24px;
  padding-left: ${({ hasLabel }) => (hasLabel ? `36px` : `24px`)};
  user-select: none;
  display: flex;
  align-items: center;
  user-select: none;

  ${({ disabled }) =>
    disabled &&
    `
    opacity:0.4;
    :hover {
      cursor: not-allowed;
    }
  `}
`;

const CustomInput = styled.span<{ inverted?: boolean }>`
  position: absolute;
  top: 0;
  left: 0;
  height: 24px;
  width: 24px;
  background-color: transparent;
  border: 1px solid ${({ theme }) => theme.colors.softRegularGrey};
  ${({ theme, inverted }) => (inverted ? theme.colors.softGrey : theme.colors.plusDarkGrey)};
  opacity: ${({ inverted }) => (inverted ? 0.6 : 1)};
  background-color: ${({ theme, inverted }) =>
    inverted ? theme.colors.primary : theme.colors.white};

  :hover {
    border: 1px solid
      ${({ theme, inverted }) => (inverted ? theme.colors.white : theme.colors.primary)};
    opacity: 1;
  }

  :after {
    content: '';
    position: absolute;
    display: none;
    left: 8px;
    top: 3px;
    width: 5px;
    height: 12px;
    border: solid ${({ theme, inverted }) => (inverted ? theme.colors.white : theme.colors.primary)};
    border-width: 0 1px 1px 0;
    transform: rotate(45deg);
  }
`;

const Input = styled.input<{ inverted?: boolean }>`
  opacity: 0;
  position: absolute;
  cursor: pointer;
  height: 0;
  width: 0;

  &:checked + ${CustomInput} {
    border: 1px solid
      ${({ theme, inverted }) => (inverted ? theme.colors.white : theme.colors.primary)};
    opacity: 1;

    :after {
      display: block;
    }
  }
`;
