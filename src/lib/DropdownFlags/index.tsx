/**
 * DropdownFlags component
 *
 * @author Diogo Guedes <dguedes@ubiwhere.com>
 *
 */

import React from 'react';
import { Dropdown } from 'semantic-ui-react';
import styled from 'styled-components';

import PhoneNumber from 'awesome-phonenumber';

import { countries } from '../../utils';

interface IProps {
  onChange?: any;
  defaultValue: string;
  displayLocale?: boolean;
}

const DropdownFlags: React.FC<IProps> = ({ defaultValue, onChange, displayLocale }) => {
  return (
    <CustomDropdown
      onChange={(e, data) => {
        onChange(data.value);
      }}
      borderless
      search
      scrolling
      defaultValue={defaultValue}
      options={countries.map((lang) => {
        return {
          key: lang.countryCode,
          value: lang.countryCode,
          text: displayLocale
            ? lang['countryCode']
            : `+ ${PhoneNumber.getCountryCodeForRegionCode(lang['countryCode'])}`,
          flag: { name: lang['countryCode'] },
        };
      })}
    />
  );
};

export default DropdownFlags;

const CustomDropdown = styled(Dropdown)`
  &&& input {
    cursor: pointer !important;
  }
  padding: 0px;
  width: 100%;

  display: flex !important;
  justify-content: space-between !important;
  align-items: center !important;

  height: 100%;
`;
