export type ITranslations = (
  key: string,
  options?: {
    textOnly: boolean;
    count?: number | string;
    start?: number;
    end?: number;
    total?: number;
  }
) => string;

export interface ITable {
  disabledAccordion?: boolean;
  selectable?: boolean;
  selectedInfoTitle?: string | React.ReactElement;
  selectedInfoSubTitle?: (value: number) => string | React.ReactElement;
  selectedInfoAction?: string | React.ReactElement;
  onChangeSelected?: (arg: any, arg2?: any) => void;
  pointer?: boolean;
  collapsedColor?: string;
  fixed?: boolean;
  navigatable?: boolean;
  stackingFrom?: number;
  translations?: ITranslations;
  defaultSelectedRows?: { id?: number; value: any; disabled?: boolean }[];
  useValueAsId?: string;
  loading?: boolean;
  onChangeRows?: any; //TODO add proper type
  onChangePage?: any; //TODO add proper type
  pagination?: {
    offset: number;
    limit: number;
    total: number;
    hasNext: boolean;
    hasPrevious: boolean;
  };
  resetSelectedIds?: boolean;
  setResetSelectedIdsFunction?: () => void;
  name?: string; // to identify the checkboxes of the current table
  checkboxTable?: boolean;
  headerColor?: string;
  noTopBorder?: boolean;
  noAutoEmptyCells?: boolean;
  error?: string;
  structure: ITableStructure;
  headerCellPadding?: string;
  lockCollapsableInPlace?: boolean; // should not be used in conjunction with selectable prop
}

export interface ITableStructure {
  header: {
    type: string;
    titles?: {
      text: string;
      style?: any;
    }[];
    title?: JSX.Element | string;
    icon?: JSX.Element;
    label?: JSX.Element;
    labelOffset?: number;
  };
  rows: {
    value?: any;
    defaultSelected?: boolean;
    disabled?: boolean;
    removeCheckbox?: boolean;
    rowProps?: any;
    cells: {
      content: string | number | JSX.Element | null;
      cellProps?: any;
    }[];
    collapsableOpen?: boolean;
    collapsableRows?: {
      rowProps?: any;
      cells: {
        content: string | number | JSX.Element | null;
        cellProps?: any;
      }[];
    }[];
  }[];
}

export interface IPagination {
  offset: number;
  limit: number;
  total: number;
  hasNext: boolean;
  hasPrevious: boolean;
  page?: number;
}

export interface ITablePagination extends IPagination {
  onChangeRows(value: any): void; //TODO add proper type
  onChangePage(page: string): void;
  translations?: ITranslations;
}

export interface IRowsPerPage {
  text: string;
  value: number;
}

export interface IScheduleClass {
  classId: number;
  weekday: number;
  startTime: string;
  endTime: string;
  period: string | null;
  classType: string;
  periodDays?: string[];
  className: string;
  classRoom: string;
  irregularMessage?: string;
  irregularSituation?: 'noSlots' | 'noLessons';
  overlaps: boolean;
  locked: boolean;
  disabled?: boolean;
  full?: string;
  selected: boolean;
  selectedAt?: number | null | undefined;
  filtered: boolean;
  allocated: {
    state: 'assigned' | 'automatic' | 'notAssigned';
    previousTries?: [] | null;
  };
  allocation?: 'likely' | 'uncertain' | 'unlikely';
  selectedStudents?: number;
  freeSlots?: number;
  slots?: number;
  studentsLowerRanking?: number;
  studentsHigherRanking?: number;
  associatedClasses?: IAssocClasses | null;
}

export interface IAssocClasses {
  classes: number[];
  aggregator: number;
}

export interface IUc {
  ucInitials: string;
  ucColor: string;
  ucId: number;
  disabled?: boolean;
  full?: boolean;
  groupId: number | null;
  ucFullName: string;
  availableSlots: number;
  ucTotalChoices: number;
  classSchedule: IScheduleClass[];
}

export interface IMySchedule {
  periods: string;
  id: number;
  preference: number;
  phase: number;
  name: string;
  classes: {
    id: number;
    weeks: string;
    startHour: string;
    endHour: string;
    day: number;
    type: string;
    locked: true;
    room: string;
    number: string;
    ucCode: number;
    ucName: string;
    mandatoryGroup: number;
    initials: string;
    mandatoryGroupName: string;
    ucColor: string;
  }[];
}

export interface IUo {
  ucInitials: string;
  ucColor: string;
  ucId: number;
  ucFullName: string;
  ucTotalChoices: number;
  classSchedule: IScheduleClass[];
}

export interface IClassTypology {
  name: string;
  initials: string;
}

export interface ISelectedClasses {
  ucId: number;
  ucInitials: string;
  ucColor: string;
  ucFullName: string;
  classType: string;
  className: string;
  classId: number;
  classesDays: { startTime: string; endTime: string; periodDays: string[] }[];
  locked: boolean;
  overlaps?: boolean;
  selectedStudents?: number;
  selectedAt?: number | null | undefined;
  freeSlots?: number;
  studentsLowerRanking?: number;
  studentsHigherRanking?: number;
  allocated: {
    state: 'assigned' | 'automatic' | 'notAssigned';
    previousTries?: [] | null;
  };
  slots?: number;
  seriationTries?: {
    classId: number;
    name: string;
    freeSlotsStudent: number;
    occupiedSlotsStudent: number;
    totalSlots: number;
  }[];
}

export interface IClassSelectionPayload {
  ucId: number;
  groupId?: number | null;
  classId: number;
  type: string;
}

export interface IDropdownOption {
  key?: string;
  id?: number;
  text: string | any;
  value?: string | number;
  disabled?: boolean;
  onClick?: () => void;
  className?: string | 'subItem';
}
export interface ICreatedSchedule {
  id: number;
  preference: number;
  name: string;
  state: 'automatic' | 'assigned' | 'notAssigned';
  registrationId: number;
  classes: {
    id: number;
    number: string;
    ucColor: string;
  }[];
}

export interface ICreatedScheduleClass {
  id: number;
  number: string;
  ucColor: string;
}

export interface IProvisorySeriationResult {
  classId: number;
  allocation: 'likely' | 'uncertain' | 'unlikely';
  selectedStudents: number;
  freeSlots: number;
  studentsLowerRanking: number;
  studentsHigherRanking: number;
  placed: 'automatic' | 'manual' | 'notplaced' | null;
}

export interface ISeriationResults {
  date: string;
  name: string;
  number: number;
  placed: string;
  phase: number;
  type: string;
  seriationTries: string[];
}
export interface IRegistration {
  nmec: number;
  registrationId: number;
  course: number;
  courseCode: number;
  courseName: string;
  branch: number;
  branchName: string;
  registrationDate: string;
  academicYear: string;
  schoolYear: string;
  registrationState: string | null;
  eligibilityState: number | null;
}
