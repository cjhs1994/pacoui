import React from 'react';
import { storiesOf } from '@storybook/react';

import ClassCard from './index';

storiesOf('Components/ClassCard', module)
  .add('Allocation - Likely', () => (
    <ClassCard
      style={{ width: '160px', height: '120px' }}
      classItem={{
        ucId: 1,
        ucInitials: 'CR',
        color: '#B8B3BA',
        classType: 'TP',
        className: '1',
        classRoom: '04.1.02',
        classId: 1,
        selected: false,
        filtered: false,
        allocation: 'likely',
        allocated: { state: 'notAssigned' },
      }}
      detailsOrientation={'vertical'}
    />
  ))
  .add('Allocation - Uncertain', () => (
    <ClassCard
      style={{ width: '160px', height: '120px' }}
      classItem={{
        ucId: 1,
        ucInitials: 'CR',
        color: '#B8B3BA',
        classType: 'TP',
        className: '1',
        classRoom: '04.1.02',
        classId: 1,
        selected: false,
        filtered: false,
        allocation: 'uncertain',
        allocated: { state: 'notAssigned' },
      }}
      detailsOrientation={'vertical'}
    />
  ))
  .add('Allocation - Unlikely ', () => (
    <ClassCard
      style={{ width: '160px', height: '120px' }}
      classItem={{
        ucId: 1,
        ucInitials: 'CR',
        color: '#B8B3BA',
        classType: 'TP',
        className: '1',
        classRoom: '04.1.02',
        classId: 1,
        selected: false,
        filtered: false,
        allocation: 'unlikely',
        allocated: { state: 'notAssigned' },
      }}
      detailsOrientation={'vertical'}
    />
  ))
  .add('Placed - Automatic', () => (
    <ClassCard
      style={{ width: '160px', height: '120px' }}
      classItem={{
        ucId: 1,
        ucInitials: 'CR',
        color: '#B8B3BA',
        classType: 'TP',
        className: '1',
        classRoom: '04.1.02',
        classId: 1,
        selected: false,
        filtered: false,
        allocated: { state: 'assigned' },
      }}
      detailsOrientation={'vertical'}
    />
  ))
  .add('Placed - Manual', () => (
    <ClassCard
      style={{ width: '160px', height: '120px' }}
      classItem={{
        ucId: 1,
        ucInitials: 'CR',
        color: '#B8B3BA',
        classType: 'TP',
        className: '1',
        classRoom: '04.1.02',
        classId: 1,
        selected: false,
        filtered: false,
        allocated: { state: 'assigned' },
      }}
      detailsOrientation={'vertical'}
    />
  ));
