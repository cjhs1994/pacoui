/**
 * ClassCard component
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import Text from '../Text';
import Tooltip from '../Tooltip';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSmile, faFrown, faMeh, faExclamationCircle } from '@fortawesome/pro-regular-svg-icons';

import theme from '../theme';

interface IProps {
  locked?: boolean;
  startTime?: string;
  endTime?: string;
  onSelect?: Function;
  style?: any;
  disabled?: boolean;
  full?: string;
  classItem: {
    ucId: number;
    groupId?: number;
    ucInitials: string;
    color?: string;
    classType: string;
    irregularMessage?: string;
    periodDays?: string[];
    className: string;
    classRoom: string;
    classId: number;
    selected: boolean;
    filtered: boolean;
    allocation?: 'likely' | 'uncertain' | 'unlikely';
    allocated: {
      state: 'assigned' | 'automatic' | 'notAssigned';
      previousTries?: [] | null;
    };
  };
  detailsOrientation: string;
  onHover?: (id: null | number) => void;
  isHovered?: boolean;
}

const ClassCard: React.FC<IProps> = ({
  disabled,
  full,
  locked,
  classItem,
  style,
  onSelect,
  detailsOrientation,
  onHover,
  isHovered,
  startTime,
  endTime,
}) => {
  const handleOnClick = () => {
    if (!disabled) {
      onSelect && onSelect(classItem);
    }
  };

  const getAllocationState = () => {
    if (classItem.allocation === 'likely') {
      return <FontAwesomeIcon icon={faSmile} />;
    } else if (classItem.allocation === 'uncertain') {
      return <FontAwesomeIcon icon={faMeh} />;
    } else if (classItem.allocation === 'unlikely') {
      return <FontAwesomeIcon icon={faFrown} />;
    }
  };

  return (
    <Tooltip
      content={
        <TooltipWrapper>
          <div>
            {classItem.ucInitials} | {classItem.className}
          </div>

          {startTime && endTime && (
            <div>
              {startTime} - {endTime}
            </div>
          )}

          {classItem.periodDays && classItem.periodDays.length > 1 && (
            <>
              <br />
              {classItem.periodDays.map((period, key) => (
                <div key={`class_card_periods_${key}`}>{period}</div>
              ))}
            </>
          )}

          {full && (
            <>
              <br />
              <IrregularMessage
                icon={<FontAwesomeIcon size="lg" icon={faExclamationCircle} />}
                color="white"
              >
                {full}
              </IrregularMessage>
            </>
          )}

          {classItem.irregularMessage && (
            <>
              <br />
              <IrregularMessage
                color="white"
                icon={<FontAwesomeIcon size="lg" icon={faExclamationCircle} />}
              >
                {classItem.irregularMessage}
              </IrregularMessage>
            </>
          )}
        </TooltipWrapper>
      }
      trigger={
        <Wrapper
          colorId={classItem.color}
          disabled={disabled}
          style={style}
          onClick={() => handleOnClick()}
          selected={classItem.selected}
          filtered={classItem.filtered}
          allocated={
            classItem.allocated.state === 'assigned' || classItem.allocated.state === 'automatic'
          }
          placed={classItem.allocated.state}
          onMouseEnter={() => onHover && onHover(classItem.classId)}
          onMouseLeave={() => onHover && onHover(null)}
        >
          <ColorBackground selected={classItem.selected} disabled={disabled} isHovered={isHovered}>
            <ClassDetails contentOrientation={detailsOrientation}>
              <UcDetails
                ellipsize={true}
                ellipsisCrop="40px"
                weight={'medium'}
                color={theme.colors.plusDarkGrey}
                size={'article'}
              >
                {classItem.ucInitials} - {classItem.ucId}
              </UcDetails>
              <ClassDetailsSpacing />
              <ClassNameText weight={'medium'} color={theme.colors.plusDarkGrey} size={'xxSmall'}>
                {classItem.className} - {classItem.classRoom}
              </ClassNameText>
            </ClassDetails>
          </ColorBackground>

          <Details
            allocated={
              classItem.allocated.state === 'assigned' || classItem.allocated.state === 'automatic'
            }
            colorId={classItem.color}
          />
          <AllocationState>{getAllocationState()}</AllocationState>
        </Wrapper>
      }
    />
  );
};

export default ClassCard;

interface IWrapper {
  selected: boolean;
  disabled?: boolean;
  filtered: boolean;
  allocated?: boolean;
  placed: 'assigned' | 'automatic' | 'notAssigned' | null;
  colorId?: string;
}

const TooltipWrapper = styled.div`
  text-align: center;
`;

const Wrapper = styled.div<IWrapper>`
  width: 100%;
  height: 100%;
  position: relative;
  box-shadow: ${({ theme, selected }) => (selected ? theme.shadows.medium : `none`)};
  cursor: ${({ disabled }) => (disabled ? `not-allowed` : `pointer`)};
  display: ${({ filtered }) => (filtered ? `none` : `flex`)};
  flex-direction: column;
  justify-content: center;
  align-items: center;
  cursor: ${({ disabled }) => (disabled ? `not-allowed` : `pointer`)};
  transition: box-shadow 0.3s ease-in-out;
  border: ${({ selected, placed, allocated }) =>
    placed === 'automatic' || placed === 'assigned' || allocated || selected ? '1px solid' : '0'};
  border-color: ${({ colorId, theme, selected, allocated, placed }) => {
    if (placed === 'automatic') {
      return `${theme.colors.primary};`;
    } else if (placed === 'assigned' || allocated) {
      return `${theme.colors.successGreen};`;
    } else if (selected) {
      return colorId;
    }
  }};
`;

interface IColorBackground {
  selected: boolean;
  disabled?: boolean;
  isHovered?: boolean;
}

const AllocationState = styled.div`
  position: absolute;
  top: 4px;
  left: 4px;
`;

const ColorBackground = styled.div<IColorBackground>`
  position: absolute;
  background-color: ${({ theme }) => theme.colors.white};
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  opacity: ${({ selected }) => (selected ? `1` : `0.4`)};

  opacity: ${({ isHovered, disabled, selected }) => {
    if (isHovered) {
      if (disabled) {
        if (selected) {
          return `1`;
        } else {
          return `0.4`;
        }
      } else {
        return `1`;
      }
    }
  }};

  &:hover {
    opacity: ${({ disabled, selected }) => {
      if (disabled) {
        if (selected) {
          return `1`;
        } else {
          return `0.4`;
        }
      } else {
        return `1`;
      }
    }};
  }
`;

interface IDetails {
  colorId?: string;
  allocated: boolean;
}

const Details = styled.div<IDetails>`
  position: absolute;
  width: 100%;
  height: 100%;

  &:after {
    width: 12px;
    background: ${({ colorId, theme }) => (colorId ? colorId : theme.colors.darkGrey)};
    height: 100%;
    content: '';
    position: absolute;
    right: 0;
  }
`;

const ClassDetails = styled.div<{ contentOrientation: string }>`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  flex-direction: ${({ contentOrientation }) =>
    contentOrientation === 'horizontal' ? `row` : `column`};

  ${({ contentOrientation }) =>
    contentOrientation === 'horizontal' &&
    `
      >div:not(:first-child) {
        margin-left: 5px;
      }
  `}
`;

const ClassDetailsSpacing = styled.div`
  max-height: 8px;
  flex-grow: 1;
  min-height: 0;
`;

const ClassNameText = styled(Text)`
  max-width: calc(100% - 40px);
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const UcDetails = styled(Text)`
  line-height: 1rem;
`;

const IrregularMessage = styled(Text)`
  &&& {
    align-items: flex-start;

    svg {
      padding-top: 2px;
    }
  }
`;
