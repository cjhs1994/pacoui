export default {
  medias: {
    xss: '@media (max-width: 360px)',
    xs: '@media (max-width: 767px)',
    sm: '@media (min-width: 768px) and (max-width: 1023px)',
    md: '@media (min-width: 1024px) and (max-width: 1439px)',
    lg: '@media (min-width: 1440px)',
  },
  header: {
    height: '80px',
  },
  sidebar: {
    width: '250px',
    collapsedWidth: '80px',
  },
  breadcrumb: {
    marginTop: '32px',
    marginBottom: '40px',
  },
  weight: {
    light: 300,
    regular: 400,
    medium: 500,
    bold: 600,
  },
  colors: {
    primary: '#0EB4BD',
    primaryShadow: '#0CA0A8',
    institutional: '#95CA3D',
    primaryRGB: ['14', '180', '189'],
    blackLight: '#333333',
    plusDarkGrey: '#292929',
    darkGrey: '#757575',
    grey: '#9C9C9C',
    regularGrey: '#C2C2C2',
    softRegularGrey: '#D5D5D5',
    softLightGrey: '#ECECEC',
    softGrey: '#F8f8f8',
    lightGrey: '#F2F2F2',
    white: '#FFFFFF',
    black: '#000',
    red: '#DB2828',
    dangerRed: '#DB2828B3',
    successGreen: '#21BA45B3',
    decisionYellow: '#FBAA08B3',
    textBody: '#292929',
    text: {
      caption: '#3B3B3B',
    },
    systemBar: {
      grey: '#e6e6e6',
    },
    sideBar: {
      lightGrey: '#575252',
    },
    dropdown: {
      defaultColor: '#292929',
      subItemColor: '#989898',
      defaultBorder: '#292929',
      hoverBorder: '#C7C7C7',
      activeBorder: '#292929',
    },
    sidebar: {
      backgroundColor: '#2a2a2a',
      borderColorDark: '#1f1f1f',
      borderColorLight: '#2a2a2a',
      subItemColor: '#383838',
    },
  },
  shadows: {
    strong: '0px 2px 4px #00000029',
    medium: '0px 3px 6px #00000029',
    soft: '0px 3px 6px #0000001a',
  },
  classCardDetails: {
    colors: {
      darkGrey: '#F2F2F2',
    },
  },
  scheduleGrid: {
    colors: {
      darkGrey: '#F2F2F2',
      lightGrey: '#FBFBFB',
    },
  },
  type: {
    base: "'Roboto', sans-serif",
    serif: "'Playfair Display', 'Roboto', sans-serif",
    icons: "'FontAwesome'",
  },
  sizes: {
    xxSmall: '0.625rem',
    xSmall: '0.75rem',
    small: '0.875rem',
    article: '0.938rem',
    mediumSmall: '1rem',
    medium: '1.125rem',
    large: '1.375rem',
    xLarge: '1.75rem',
    xxLarge: '2rem',
    xxxLarge: '3rem',
  },
};
