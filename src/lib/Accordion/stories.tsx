import React from 'react';
import { storiesOf } from '@storybook/react';

import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown } from '@fortawesome/pro-light-svg-icons';

import Accordion from './index';

const InnerWrapper = styled.div`
  width: ${({ theme }) => theme.sidebar.width};
  display: flex;
  flex-direction: column;
`;

const Row = styled.div`
  display: flex;
  align-items: center;
`;

const AccordionHeader = styled.div`
  margin-top: 20px;
  color: ${({ theme }) => theme.colors.darkGrey};
  font-size: 14px;
  font-weight: 300;
  text-align: left;

  svg {
    margin-left: 12px;
  }

  &:hover {
    color: ${({ theme }) => theme.colors.primary};
    cursor: pointer;
  }
`;

const Text = styled.div<{
  color?: string;
  marginTop?: number;
}>`
  font-size: 14px;
  font-weight: ${({ theme }) => theme.weight.light};
  text-align: left;
  ${({ marginTop }) => marginTop && `margin-top: ${marginTop}px;`}
  ${({ color }) => color && `color: ${color};`}
`;

const Number = styled.div<{ fontWeight?: string }>`
  font-size: 28px;
  text-align: left;
  margin-top: 17px;
  margin-right: 10px;
  color: ${({ theme }) => theme.colors.plusDarkGrey};
  ${({ theme, fontWeight }) => fontWeight && `font-weight: ${theme.weight[fontWeight]};`}
`;

const structureBasic = [
  {
    parent: (
      <AccordionHeader>
        Colocação automática
        <FontAwesomeIcon icon={faAngleDown} />
      </AccordionHeader>
    ),
    children: [
      <InnerWrapper key={'accordiion-inner-wrapper'}>
        <Row>
          <Text color={'#757575'} marginTop={20}>
            <b>1. </b>
            {`O número de vagas da Turma 1, foi preenchido por alunos com melhor ranking que o seu:`}
            .
          </Text>
        </Row>

        <Row>
          <Number fontWeight={'light'}>20</Number>
          <Text color={'#757575'} marginTop={20}>
            (em 40)
          </Text>
        </Row>

        <Row>
          <Text color={'#DB2828'} marginTop={5}>
            20 vagas disponíveis
          </Text>
        </Row>
      </InnerWrapper>,
    ],
  },
];

storiesOf('Components/Accordion', module).add('Basic', () => (
  <Accordion structure={structureBasic} />
));
