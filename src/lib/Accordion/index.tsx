/**
 * Accordion component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React, { useState } from 'react';
import styled from 'styled-components';

interface IAccordion {
  structure: {
    defaultOpen?: boolean;
    parent: JSX.Element;
    children: JSX.Element[];
  }[];
  onOpen?: () => void;
}

const Accordion: React.FC<IAccordion> = ({ structure, onOpen }) => {
  const getDefaultOpenAccordion = () => {
    return structure.reduce((acc: number[], item, index: number) => {
      if (item.defaultOpen) {
        return [...acc, index];
      }
      return acc;
    }, []);
  };

  const [accordionOpen, setAccordionOpeningStatus] = useState(
    getDefaultOpenAccordion() as number[]
  );

  const handleOnClick = (index) => {
    if (accordionOpen.includes(index)) {
      setAccordionOpeningStatus(accordionOpen.filter((accordion) => accordion !== index));
    } else {
      setAccordionOpeningStatus([...accordionOpen, index]);
    }
    if (onOpen) {
      onOpen();
    }
  };

  return (
    <Wrapper>
      {structure.map((element, index) => {
        return (
          <InnerWrapper key={`accordion-${index}`}>
            <HeaderWrapper
              show={accordionOpen.includes(index)}
              onClick={() => handleOnClick(index)}
            >
              {element.parent}
            </HeaderWrapper>
            <ChildrenWrapper show={accordionOpen.includes(index)}>
              {element.children.map((child) => child)}
            </ChildrenWrapper>
          </InnerWrapper>
        );
      })}
    </Wrapper>
  );
};

export default Accordion;

const Wrapper = styled.div``;

const InnerWrapper = styled.div``;

const HeaderWrapper = styled.div<{ show: boolean }>`
  svg {
    transform: ${({ show }) => (show ? `rotate(180deg);` : `rotate(0)`)};
  }

  &:hover {
    cursor: pointer;
  }
`;

const ChildrenWrapper = styled.div<{ show: boolean }>`
  display: ${({ show }) => (show ? `block` : `none`)};
`;
