/**
 * GoToTopButton component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';
import { Transition } from 'semantic-ui-react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowUp } from '@fortawesome/pro-light-svg-icons';

import theme from '../theme';

interface IGoToTopButton {
  showButton?: boolean;
  onClick?: any;
}

const GoToTopButton: React.FC<IGoToTopButton> = ({ showButton, onClick }) => {
  return (
    <Transition.Group animation={'fly up'} duration={1200}>
      {showButton && (
        <Wrapper onClick={onClick && onClick}>
          <Button>
            <FontAwesomeIcon icon={faArrowUp} />
          </Button>
        </Wrapper>
      )}
    </Transition.Group>
  );
};

export default GoToTopButton;

const Wrapper = styled.div<{ showButton?: boolean; stickyPositionFromTop?: number }>`
  display: flex;
  justify-content: flex-end;
  position: fixed;
  bottom: 30px;
  right: 50px;
`;

const Button = styled.div`
  width: 64px;
  height: 64px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  background-color: ${({ theme }) => theme.colors.grey};
  box-shadow: ${({ theme }) => theme.shadows.medium};

  svg {
    font-size: 28px;
    color: ${({ theme }) => theme.colors.white};
  }

  &:hover {
    cursor: pointer;
    background-color: ${({ theme }) => theme.colors.primary};
  }
`;
