import React from 'react';
import { storiesOf } from '@storybook/react';

import GoToTopButton from './index';

storiesOf('Components/GoToTopButton', module).add('GoToTopButton', () => <GoToTopButton />);
