import React from 'react';
import { storiesOf } from '@storybook/react';
import Caption from './index';

const references = [
  { title: 'Reference 1', color: '#21BA45' },
  { title: 'Reference 2', color: '#FBAA08' },
  { title: 'Reference 3', color: '#DB2828' },
];

storiesOf('Components/Caption', module).add('Caption', () => <Caption references={references} />);
