/**
 * Caption component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import Text from '../Text';

interface IReference {
  title: React.ReactNode | string;
  icon?: React.ReactNode;
  color?: string;
}

interface ICaption {
  title?: string;
  references: IReference[];
}

const Caption: React.FC<ICaption> = ({ title, references }) => {
  return (
    <Wrapper>
      {title && (
        <Title>
          <Text transform="uppercase" weight="regular" color="plusDarkGrey">
            {title}
          </Text>
        </Title>
      )}
      <ReferencesWrapper>
        {references.map((reference, index) => {
          return (
            <Reference key={`CaptionReference-${index}`}>
              {reference.icon && (
                <ReferenceIcon color={reference.color}>{reference.icon}</ReferenceIcon>
              )}
              {typeof reference.title === 'string' && (
                <ReferenceTitle>{reference.title}</ReferenceTitle>
              )}
              {typeof reference.title !== 'string' && reference.title}
            </Reference>
          );
        })}
      </ReferencesWrapper>
    </Wrapper>
  );
};

export default Caption;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const ReferencesWrapper = styled.div`
  display: inline-flex;
  align-items: center;
  background: ${({ theme }) => theme.colors.white};
  opacity: 1;

  > div:not(:last-child) {
    margin-right: 16px;
  }
`;

const Title = styled.div`
  margin-bottom: 8px;
`;

const Reference = styled.div`
  position: relative;
  display: flex;
  align-items: center;
`;

const ReferenceIcon = styled.span<{ color?: string }>`
  margin-right: 8px;
  color: ${({ theme, color }) => (color ? color : theme.colors.plusDarkGrey)};
`;

const ReferenceTitle = styled.span`
  color: ${({ theme }) => theme.colors.text.caption};
  font-family: 'Roboto';
  font-weight: 300;
`;
