import React from 'react';

import AnimatedBackground from '../AnimatedBackground';

import styled from 'styled-components';

import theme from '../theme';
interface IProps {
  loading?: boolean;
  onNavigate: (url: string) => void;
  paths: {
    name: string;
    key: string;
    url: string;
  }[];
}

const Breadcrumb: React.FC<IProps> = ({ loading, onNavigate, paths }) => {
  return (
    <BreadcrumbContainer>
      {loading && <AnimatedBackground height={theme.sizes.medium} width="200px" />}
      {!loading &&
        paths.map((path, key) => (
          <PathElementWrapper key={`breadcrumb_link_id_${key}`}>
            <PageLinkContainer
              last={key === paths.length - 1}
              clickable={!!path.url}
              onClick={() => key !== paths.length - 1 && onNavigate && onNavigate(path.key)}
              data-testid={`breadcrumb_elem_path`}
            >
              {path.name}
            </PageLinkContainer>
            {key !== paths.length - 1 && <IconWrapper>/</IconWrapper>}
          </PathElementWrapper>
        ))}
    </BreadcrumbContainer>
  );
};

export default Breadcrumb;

const BreadcrumbContainer = styled.div`
  display: flex;
  align-items: center;
  margin-top: ${({ theme }) => theme.breadcrumb.marginTop};
  margin-bottom: ${({ theme }) => theme.breadcrumb.marginBottom};
`;

const PathElementWrapper = styled.div`
  display: flex;
`;

const PageLinkContainer = styled.div<{ clickable?: boolean; last: boolean }>`
  cursor: ${({ last, clickable }) => (last ? `auto` : !clickable ? `not-allowed` : `pointer`)};
  display: flex;
  font-size: 12px;
  font-weight: ${({ last }) => (last ? `500` : `300`)};
  text-transform: lowercase;

  &:hover {
    text-decoration: ${({ last, clickable }) => (last ? `initial` : clickable && `underline`)};
  }
`;

const IconWrapper = styled.div`
  padding: 0px 6px;
  font-size: 11px;
`;
