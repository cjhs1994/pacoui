import React from 'react';
import { storiesOf } from '@storybook/react';

import Breadcrumb from './index';

storiesOf('Components/Breadcrumb', module)
  .add('Breadcrumb', () => (
    <Breadcrumb paths={[{ name: 'teste', key: 'teste', url: '/teste' }]} onNavigate={alert} />
  ))
  .add('Breadcrumb loading', () => (
    <Breadcrumb
      loading
      paths={[{ name: 'teste', key: 'teste', url: '/teste' }]}
      onNavigate={alert}
    />
  ));
