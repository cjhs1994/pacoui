/**
 * EditableFieldGroup component
 *
 * @author Diogo Guedes <dguedes@ubiwhere.com>
 *
 */

import React, { useContext, useState, useEffect, useRef } from 'react';
import { useFormContext } from 'react-hook-form';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisH } from '@fortawesome/pro-regular-svg-icons';
import { faPen } from '@fortawesome/free-solid-svg-icons';

import { Grid } from 'semantic-ui-react';

import Text from '../Text';
import Button from '../Button';

import theme from '../theme';

interface IProps {
  t?: any;
  name: string;
  errors?: (nameKeys: string[]) => any;
  title?: string;
  disableEdit?: boolean;
  padding?: string;
  form?: any;
  values: {
    nameKey: string;
    type?: string;
    disable?: boolean;
    placeholderKey?: string;
    value: string;
    validations?: [];
    visibleIf?: any;
  }[];
  register?: any;
  children?: any;
  labels: (nameKeys: string[]) => any;
}

const EditableFieldsGroup: React.FC<IProps> = ({
  t,
  name,
  errors,
  values,
  title,
  padding,
  disableEdit,
  labels,
  children,
  register = {},
  ...props
}) => {
  const form = useFormContext();

  const triggerSave = useRef(false);

  const [editable, setEditable] = useState(false);

  const getValuesBeforeSave = () =>
    Object.keys(values).map((key) => ({
      nameKey: values[key].nameKey,
      value: form.getValues(`${name}.${values[key].nameKey}`),
    }));

  const valueBeforeSave = useRef(getValuesBeforeSave());

  useEffect(() => {
    if (editable) {
      valueBeforeSave.current = getValuesBeforeSave();
    }
  }, [editable]);

  const getLabels = labels(Object.keys(values).map((key) => values[key].nameKey));
  let getErrors = {};
  if (errors) {
    getErrors = errors(Object.keys(values).map((key) => values[key].nameKey));
  }
  useEffect(() => {
    if (triggerSave.current) {
      if (Object.keys(getErrors).length <= 0) {
        triggerSave.current = false;
        setEditable(false);
      }
    }
  }, [getErrors]);

  return (
    <Wrapper>
      <HeaderWrapper>
        <EditableBlockTitle>
          <Text transform={'uppercase'} size="xSmall" color={'grey'} weight={'regular'}>
            {title}
          </Text>
        </EditableBlockTitle>
        {!editable && !disableEdit && (
          <EditButtonWrapper>
            <Button
              onClick={() => {
                setEditable(true);
              }}
              borderless
              data-testid={`personaldata_edit_button`}
            >
              <FontAwesomeIcon icon={faPen} color={theme.colors.plusDarkGrey} size="lg" />
            </Button>
          </EditButtonWrapper>
        )}
      </HeaderWrapper>

      <Grid>
        {Object.keys(values).map((key) => {
          return (
            <Column
              paddingtop={padding ? padding : undefined}
              paddingbottom={padding ? padding : undefined}
              key={`editableBlock_${values[key].nameKey}`}
              mobile={16}
              tablet={8}
              computer={8}
              largeScreen={8}
              widescreen={8}
            >
              <TextFieldTitle>
                <Text color="grey" size="xSmall" fontWeight="400">
                  {getLabels[values[key].nameKey]}
                </Text>
              </TextFieldTitle>

              <VisibilityWrapper visible={editable}>{children && children[key]}</VisibilityWrapper>

              <VisibilityWrapper visible={!editable}>
                <Text fontWeight="400" color="black" size="mediumSmall">
                  {!form.getValues(`${name}.${values[key].nameKey}`) ? (
                    <FontAwesomeIcon icon={faEllipsisH} />
                  ) : (
                    form.getValues(`${name}.${values[key].nameKey}`)
                  )}
                </Text>
              </VisibilityWrapper>
            </Column>
          );
        })}
      </Grid>

      {editable && (
        <ButtonWrapper>
          <Button
            decision
            onClick={() => {
              valueBeforeSave.current.forEach((item) => {
                form.setValue(`${name}.${item.nameKey}`, item.value);
              });
              setEditable(false);
            }}
          >
            {t ? t('generic.cancel') : 'Cancelar'}
          </Button>
          <Button
            success
            onClick={() => {
              if (form) {
                triggerSave.current = true;
                form.trigger(name);
              }
            }}
          >
            {t ? t('generic.save') : 'Guardar'}
          </Button>
        </ButtonWrapper>
      )}
    </Wrapper>
  );
};
export default EditableFieldsGroup;

const Wrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  flex-direction: column;

  .ui.grid {
    flex-grow: 1;
  }
`;

const VisibilityWrapper = styled.div<{ visible: boolean }>`
  display: ${({ visible }) => (visible ? 'block' : 'none')};
`;

const Column = styled(Grid.Column)<{ paddingtop?: string; paddingbottom?: string }>`
  &&&&& {
    padding-top: ${({ paddingtop }) => (paddingtop ? paddingtop : '1rem')};
    padding-bottom: ${({ paddingbottom }) => (paddingbottom ? paddingbottom : '1rem')};
  }
`;

const TextFieldTitle = styled.div`
  margin: 0px 0px 12px 0px;
`;

const EditButtonWrapper = styled.div`
  z-index: 100;
`;

const EditableBlockTitle = styled.div`
  margin-bottom: 17px;
`;

const HeaderWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin-left: auto;
  margin-top: 28px;

  > Button:not(:last-child) {
    margin-right: 8px;
  }
`;
