/**
 * EventCalendar component
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */

import React, { useContext, useState, useEffect } from 'react';
import styled from 'styled-components';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faChevronRight } from '@fortawesome/pro-regular-svg-icons';

import AnimatedBackground from '../AnimatedBackground';

import { IDropdownOption } from '../types';

import Text from '../Text';
import Button from '../Button';
import Tooltip from '../Tooltip';
import SearchBox from '../SearchBox';
import Dropdown from '../Dropdown';

import EventPopup from './components/EventPopup';
import MoreEventsPopup from './components/MoreEventsPopup';

import theme from '../theme';

const localizer = momentLocalizer(moment);

interface IProps {
  events: any;
  captions?: { label: string; title: string }[];
  leftButtonTooltip?: string;
  rightButtonTooltip?: string;
  todayButtonText?: string;
  loading?: boolean;
  seeMore: string;
  seeLocation: string;
  onSeeLocation?: (any) => void;
  addFilters?: {
    searchPlaceholder: string;
    dropdownPlaceholder: string;
    filterTypes: IDropdownOption[];
    onSearch: any;
    onFilterChange: any;
    onFilterClear: any;
  };
  updateCalendarCurrentYearMonth: (date) => void;
}

const EventCalendar: React.FC<IProps> = ({
  events,
  seeMore,
  seeLocation,
  onSeeLocation,
  leftButtonTooltip,
  rightButtonTooltip,
  addFilters,
  loading,
  todayButtonText,
  updateCalendarCurrentYearMonth,
}) => {
  const [calendarEvents, setCalendarEvents] = useState(
    events.sort((a, b) => moment(a.start).diff(b.start))
  );
  const [selectedEvent, setSelectedEvent] = useState(null as null | number);
  const [currentDate, setCurrentDate] = useState(new Date());

  useEffect(() => {
    if (events) {
      setCalendarEvents(events.sort((a, b) => moment(a.start).diff(b.start)));
    }
  }, [events]);

  const Event = ({ event }) => {
    const isInCurrentMonth = moment(event.start).isSame(currentDate, 'month');
    let currentEventInDay = 0;

    events.some((filterEvent) => {
      // Passing in day will check day, month, and year.
      if (moment(filterEvent.start).isSame(event.start, 'day')) {
        currentEventInDay += 1;
      }
      if (filterEvent.id === event.id) {
        return true;
      }
    });

    if (currentEventInDay >= 3) {
      return (
        <MoreEventsPopup
          onOpen={() => {
            setSelectedEvent(null);
          }}
          seeLocation={seeLocation}
          onSeeLocation={onSeeLocation}
          seeMore={seeMore}
          events={calendarEvents.filter((calendarEvent) =>
            moment(calendarEvent.start).isSame(event.start, 'day')
          )}
        />
      );
    } else {
      return (
        <EventPopup
          selected={selectedEvent === event.id}
          event={event}
          seeLocation={seeLocation}
          onSeeLocation={onSeeLocation}
          onClick={setSelectedEvent}
          inCurrentMonth={isInCurrentMonth}
        />
      );
    }
  };

  return (
    <Wrapper>
      <CalendarTitle>
        <EventNavigation>
          {todayButtonText && (
            <TodayButtonWrapper>
              <Button
                action
                data-testid={`event_calendar_today_month`}
                onClick={() => {
                  setCurrentDate(new Date(moment().format()));
                  updateCalendarCurrentYearMonth('');
                }}
              >
                {todayButtonText}
              </Button>
            </TodayButtonWrapper>
          )}
          <Tooltip
            content={leftButtonTooltip}
            trigger={
              <Button
                data-testid={`event_calendar_prev_month`}
                onClick={() => {
                  setSelectedEvent(null);
                  setCurrentDate(new Date(moment(currentDate).subtract(1, 'months').format()));
                  updateCalendarCurrentYearMonth(
                    moment(currentDate).subtract(1, 'months').format('MM-YYYY')
                  );
                }}
                borderless
              >
                <FontAwesomeIcon icon={faChevronLeft} color={theme.colors.plusDarkGrey} />
              </Button>
            }
          />
          <Tooltip
            content={rightButtonTooltip}
            trigger={
              <Button
                data-testid={`event_calendar_next_month`}
                onClick={() => {
                  setSelectedEvent(null);
                  setCurrentDate(new Date(moment(currentDate).add(1, 'months').format()));
                  updateCalendarCurrentYearMonth(
                    moment(currentDate).add(1, 'months').format('MM-YYYY')
                  );
                }}
                borderless
              >
                <FontAwesomeIcon icon={faChevronRight} color={theme.colors.plusDarkGrey} />
              </Button>
            }
          />
          <Text weight="regular" transform="uppercase" size="medium">
            {moment(currentDate).format('MMMM YYYY')}
          </Text>
        </EventNavigation>
        {addFilters && (
          <SearchAndFiltersWrapper>
            <StyledSearchBox
              placeholder={addFilters?.searchPlaceholder}
              borderColor={theme.colors.plusDarkGrey}
              iconColor={theme.colors.plusDarkGrey}
              delay={500}
              onSearch={(value) => {
                addFilters.onSearch(value);
              }}
            />
            <StyledDropdown
              placeholder={addFilters.dropdownPlaceholder}
              canBeCleared
              options={addFilters.filterTypes}
              selectionWeight={'400'}
              selectionFontSize={'medium'}
              onChange={(e, { value }) => {
                addFilters.onFilterChange(value);
              }}
              onClear={() => {
                addFilters.onFilterChange('');
              }}
            />
          </SearchAndFiltersWrapper>
        )}
      </CalendarTitle>

      {loading && (
        <LoadingWrapper>
          <AnimatedBackground height={theme.sizes.large} width="20%" />
          <AnimatedBackground height={theme.sizes.large} width="50%" />
          <AnimatedBackground height={theme.sizes.large} width="80%" />
          <AnimatedBackground height={theme.sizes.large} width="80%" />
          <AnimatedBackground height={theme.sizes.large} width="80%" />
          <AnimatedBackground height={theme.sizes.large} width="80%" />
          <AnimatedBackground height={theme.sizes.large} width="50%" />
        </LoadingWrapper>
      )}

      {!loading && (
        <CalendarList
          localizer={localizer}
          events={calendarEvents}
          startAccessor="start"
          endAccessor="end"
          views={['month']}
          defaultView="month"
          toolbar={false}
          date={currentDate}
          onNavigate={() => {}}
          components={{
            event: Event,
          }}
        />
      )}
    </Wrapper>
  );
};

export default EventCalendar;

const Wrapper = styled.div``;

const EventNavigation = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 18px;
`;

const TodayButtonWrapper = styled.div`
  margin-right: 12px;
`;

const CalendarList = styled(Calendar)`
  height: 800px;

  &&& {
    .rbc-header {
      height: 38px;
      display: flex;
      justify-content: flex-end;
      align-items: center;
      background-color: ${({ theme }) => theme.colors.softGrey};
      color: ${({ theme }) => theme.colors.darkGrey};
      font-size: 0.75rem;
      text-transform: uppercase;
      border: 0;
      padding-right: 9px;
    }

    .rbc-month-header {
      border: 1px solid ${({ theme }) => theme.colors.softRegularGrey};
      border-bottom: 3px solid ${({ theme }) => theme.colors.primary};
    }

    .rbc-month-row {
      border: 0;
    }

    .rbc-month-view {
      border: none;
    }

    .rbc-event {
      background-color: transparent;
      outline: none;

      &:focus {
        outline: 0;
      }
    }

    .rbc-event-content {
      div:nth-child(2) {
        div:nth-child(2) {
          margin-top: 2px;
        }
      }
    }

    .rbc-date-cell {
      color: ${({ theme }) => theme.colors.plusDarkGrey};
      font-size: 0.75rem;
      display: flex;
      justify-content: flex-end;
      align-items: center;
      padding-top: 4px;

      &.rbc-off-range {
        opacity: 0.4;
      }
    }

    .rbc-now {
      & a {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 18px;
        height: 18px;
        background-color: ${({ theme }) => theme.colors.plusDarkGrey};
        color: ${({ theme }) => theme.colors.white};
        font-weight: 400;
      }
    }

    .rbc-day-bg {
      background-color: ${({ theme }) => theme.colors.white};
      border: 1px solid ${({ theme }) => theme.colors.softGrey};

      &.rbc-off-range-bg {
        background-color: ${({ theme }) => theme.colors.lightGrey};
      }
    }
  }
`;

const SearchAndFiltersWrapper = styled.div`
  width: 50%;
  display: flex;
  margin-bottom: 24px;

  > div {
    width: 50%;
  }

  > div:last-child {
    margin-left: 16px;
  }
`;

const CalendarTitle = styled.div`
  display: flex;
  justify-content: space-between;
`;

const StyledDropdown = styled(Dropdown)`
  /*max-width: 268px;*/
`;

const StyledSearchBox = styled(SearchBox)`
  /*max-width: 268px;*/
`;

const LoadingWrapper = styled.div`
  width: 100%;
  height: 300px;

  * {
    margin: 8px 0px;
  }
`;
