/**
 * MoreEventsPopup component
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */

import React, { useContext, useState } from 'react';
import styled from 'styled-components';
import { Popup } from 'semantic-ui-react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/pro-light-svg-icons';

import Text from '../../Text';
import Button from '../../Button';
import theme from '../../theme';

import EventPopup from './EventPopup';

interface IProps {
  events: any;
  onOpen?: () => void;
  seeMore: string;
  seeLocation: string;
  onSeeLocation?: (any) => void;
}

const MoreEventsPopup: React.FC<IProps> = ({
  events,
  onOpen,
  seeMore,
  seeLocation,
  onSeeLocation,
}) => {
  const [selectedEvent, setSelectedEvent] = useState(null as null | number);
  const [isOpen, setIsOpen] = useState(false as boolean);

  return (
    <Wrapper
      basic
      open={isOpen}
      trigger={
        <EventSeeMore>
          <Button
            color={theme.colors.plusDarkGrey}
            noPadding
            data-testid={`event_calendar_see_more`}
            onClick={() => {
              setIsOpen(true);
            }}
            borderless
          >
            <Text weight="bold" size="xSmall">
              {seeMore}
            </Text>
          </Button>
        </EventSeeMore>
      }
      content={
        <EventsPopupContent>
          <EventsPopupContentHeader>
            <Button
              color={theme.colors.plusDarkGrey}
              noPadding
              data-testid={`event_calendar_close_moreEventsPopup`}
              onClick={() => {
                setIsOpen(false);
                setSelectedEvent(null);
              }}
              borderless
            >
              <FontAwesomeIcon icon={faTimes} />
            </Button>
          </EventsPopupContentHeader>

          {events.map((event, key) => (
            <EventPopup
              key={`event_calendar_close_moreEventsPopup_event_${key}`}
              selected={selectedEvent === event.id}
              event={event}
              onClick={setSelectedEvent}
              inCurrentMonth={true}
              seeLocation={seeLocation}
              onSeeLocation={onSeeLocation}
            />
          ))}
        </EventsPopupContent>
      }
      position="bottom left"
    />
  );
};

export default MoreEventsPopup;

const Wrapper = styled(Popup)`
  &&& {
    border-radius: 0;
  }
`;

const EventsPopupContent = styled.div``;

const EventsPopupContentHeader = styled.div`
  padding-bottom: 8px;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`;

const EventSeeMore = styled.div`
  float: right;
`;
