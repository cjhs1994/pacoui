/**
 * EventWrapper component
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';
import moment from 'moment';

import Text from '../../Text';
import theme from '../../theme';

interface IProps {
  event: any;
  inCurrentMonth?: boolean;
  onClick?: (any) => void;
  selected?: boolean;
}

const EventWrapper: React.FC<IProps> = ({ event, selected, inCurrentMonth, onClick }) => {
  return (
    <Wrapper
      inCurrentMonth={inCurrentMonth}
      selected={selected}
      onClick={() => {
        if (selected) {
          onClick && onClick(null);
        } else if (inCurrentMonth) {
          onClick && onClick(event.id);
        }
      }}
    >
      {event.icon && <EventIconWrapper>{event.icon}</EventIconWrapper>}
      <EventDetails>
        <Text ellipsize color={selected ? 'white' : 'plusDarkGrey'} weight="bold" size="xSmall">
          {event.title}
        </Text>
        {event.start && (
          <Text
            ellipsize
            color={selected ? 'white' : 'plusDarkGrey'}
            weight="regular"
            size="xSmall"
          >
            {moment(event.start).format('hh:mm')}
            {event.location && ` | ${event.location}`}
          </Text>
        )}
      </EventDetails>
    </Wrapper>
  );
};

export default EventWrapper;

const Wrapper = styled.div<{ selected?: boolean; inCurrentMonth?: boolean }>`
  display: flex;
  align-items: center;
  background-color: ${({ selected, theme }) => (selected ? theme.colors.primary : 'transparent')};
  padding: 4px;
  opacity: ${({ inCurrentMonth }) => (inCurrentMonth ? 1 : 0.5)};
  cursor: ${({ inCurrentMonth }) => (inCurrentMonth ? 'pointer' : ' not-allowed')};

  &:hover {
    background-color: ${({ selected, theme }) =>
      selected ? theme.colors.primary : theme.colors.softGrey};
  }
`;

const EventIconWrapper = styled.div`
  margin-right: 8px;
`;

const EventDetails = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 0;
`;
