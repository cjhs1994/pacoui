/**
 * EventPopup component
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
import { Popup } from 'semantic-ui-react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkerAlt } from '@fortawesome/pro-regular-svg-icons';
import { faTimes } from '@fortawesome/pro-light-svg-icons';

import Text from '../../Text';
import Button from '../../Button';
import theme from '../../theme';

import EventWrapper from './EventWrapper';

interface IProps {
  event: any;
  inCurrentMonth?: boolean;
  onClick?: (any) => void;
  selected?: boolean;
  seeLocation: string;
  onSeeLocation?: (any) => void;
}

const EventPopup: React.FC<IProps> = ({
  event,
  inCurrentMonth,
  selected,
  onClick,
  seeLocation,
  onSeeLocation,
}) => {
  return (
    <Wrapper
      basic
      open={selected}
      trigger={
        <EventWrapper
          event={event}
          inCurrentMonth={inCurrentMonth}
          selected={selected}
          onClick={() => {
            if (selected) {
              onClick && onClick(null);
            } else if (inCurrentMonth) {
              onClick && onClick(event.id);
            }
          }}
        />
      }
      content={
        <EventPopupContent>
          <EventPopupContentHeader>
            <Button
              color={theme.colors.plusDarkGrey}
              noPadding
              data-testid={`event_calendar_close_popup`}
              onClick={() => {
                onClick && onClick(null);
              }}
              borderless
            >
              <FontAwesomeIcon icon={faTimes} />
            </Button>
          </EventPopupContentHeader>

          <EventPopupContentTitle>
            {event.icon && <EventIconWrapper>{event.icon}</EventIconWrapper>}
            <Text color={'plusDarkGrey'} weight="medium" size="small">
              {event.title}
            </Text>
          </EventPopupContentTitle>

          {event.start && (
            <EventPopupContentDesc>
              <Text color={'plusDarkGrey'} weight="regular" size="xSmall">
                {moment(event.start).format('dddd, LL')} | {moment(event.start).format('hh:mm')}
              </Text>
            </EventPopupContentDesc>
          )}

          {event.location && (
            <EventPopupContentLocation>
              <Text
                icon={<FontAwesomeIcon size="lg" icon={faMapMarkerAlt} />}
                color="plusDarkGrey"
                size="xSmall"
                weight="bold"
                onClick={() => {}}
              >
                {event.location}
              </Text>

              <Text
                as="a"
                size="xSmall"
                onClick={() => {
                  onSeeLocation && onSeeLocation(event);
                }}
              >
                {seeLocation}
              </Text>
            </EventPopupContentLocation>
          )}

          {event.popupFooter && (
            <EventPopupContentFooter>{event.popupFooter}</EventPopupContentFooter>
          )}
        </EventPopupContent>
      }
      position="left center"
    />
  );
};

export default EventPopup;

const Wrapper = styled(Popup)`
  &&& {
    border-radius: 0;
    max-width: 300px;
  }
`;

const EventIconWrapper = styled.div`
  margin-right: 8px;
`;

const EventPopupContent = styled.div``;

const EventPopupContentHeader = styled.div`
  padding-bottom: 8px;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`;

const EventPopupContentTitle = styled.div`
  display: flex;
  align-items: center;
`;

const EventPopupContentDesc = styled.div`
  margin-top: 6px;
`;

const EventPopupContentLocation = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 24px;
`;

const EventPopupContentFooter = styled.div`
  padding-top: 16px;
  margin-top: 16px;
  border-top: 2px solid ${({ theme }) => theme.colors.softGrey};
`;
