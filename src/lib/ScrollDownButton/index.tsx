/**
 * ScrollDownButton component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/pro-regular-svg-icons';

import theme from '../theme';

interface IScrollDownButton {
  onClick: any;
}

const ScrollDownButton: React.FC<IScrollDownButton> = ({ onClick }) => {
  return (
    <Wrapper>
      <FontAwesomeIcon icon={faChevronDown} onClick={onClick} />
    </Wrapper>
  );
};

export default ScrollDownButton;

const Wrapper = styled.div`
  width: 32px;
  height: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.regularGrey};
  border-radius: 50%;
  color: ${({ theme }) => theme.colors.white};
  font-size: 15px;
  cursor: pointer;
`;
