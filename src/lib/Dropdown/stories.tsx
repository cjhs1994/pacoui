import React from 'react';
import { storiesOf } from '@storybook/react';

import Dropdown from './index';

const options = [
  {
    key: 'Jenny Hess',
    text: 'Jenny Hess',
    value: 'Jenny Hess',
  },
  {
    key: 'Elliot Fu',
    text: 'Elliot Fu',
    value: 'Elliot Fu',
  },
  {
    key: 'Stevie Feliciano',
    text: 'Stevie Feliciano',
    value: 'Stevie Feliciano',
  },
  {
    key: 'Christian',
    text: 'Christian',
    value: 'Christian',
  },
  {
    key: 'Matt',
    text: 'Matt',
    value: 'Matt',
  },
  {
    key: 'Justen Kitsune',
    text: 'Justen Kitsune',
    value: 'Justen Kitsune',
  },
];

storiesOf('Components/Dropdown', module)
  .add('Dropdown', () => <Dropdown options={options} />)
  .add('Borderless Dropdown', () => (
    <Dropdown
      borderless
      options={options}
      selectionWeight={'800'}
      selectionFontSize={'mediumSmall'}
    />
  ));
