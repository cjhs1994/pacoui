/**
 * Dropdown component
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */

import React, { useEffect, useState, useContext } from 'react';
import styled from 'styled-components';

import { Dropdown as SemanticDropdown } from 'semantic-ui-react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faTimes } from '@fortawesome/pro-regular-svg-icons';

import { IDropdownOption } from '../types';

import theme from '../theme';

interface IDropdown extends Omit<React.ComponentProps<typeof SemanticDropdown>, ''> {
  t?: any;
  placeholder?: string;
  options: IDropdownOption[];
  value?: string | number | boolean;
  defaultValue?: string | number | boolean;
  fitToContent?: boolean;
  description?: boolean;
  canBeCleared?: boolean;
  disable?: boolean;
  textColor?: string;
  borderless?: boolean;
  borderColor?: string;
  selectionWeight?: string;
  selectionFontSize?:
    | 'xxSmall'
    | 'xSmall'
    | 'small'
    | 'article'
    | 'mediumSmall'
    | 'medium'
    | 'large'
    | 'xLarge'
    | 'xxLarge'
    | 'xxxLarge';
  onClear?: () => void;
  onChange?: (event: React.SyntheticEvent<HTMLElement>, data: { [key: string]: any }) => void;
}

const Dropdown: React.FC<IDropdown> = ({
  t,
  placeholder,
  options,
  value,
  defaultValue,
  fitToContent,
  description,
  disable,
  canBeCleared,
  borderless,
  borderColor,
  selectionWeight,
  selectionFontSize,
  onClear,
  onChange,
  textColor,
  ...rest
}) => {
  const [currentValue, setCurrentValue] = useState((value || undefined) as any);

  const defaultOption = [
    {
      text: t ? t('generic.optionsNotFound', { textOnly: true }) : 'Nenhuma opção encontrada',
      disabled: true,
    },
  ];

  useEffect(() => {
    setCurrentValue(value);
  }, [value]);

  return (
    <Wrapper
      description={description}
      borderless={borderless}
      borderColor={borderColor}
      selectionWeight={selectionWeight}
      selectionFontSize={selectionFontSize}
      fitToContent={fitToContent}
      textColor={textColor}
    >
      <StyledDropdown
        disabled={disable}
        selectOnBlur={false}
        placeholder={
          placeholder ? placeholder : t ? t('generic.select', { textOnly: true }) : 'Selecione'
        }
        defaultValue={defaultValue}
        value={currentValue?.value || currentValue}
        {...rest}
        icon={
          <>
            {!canBeCleared && (
              <DropdownToggleIcon borderless={borderless}>
                <FontAwesomeIcon icon={faChevronDown} />
              </DropdownToggleIcon>
            )}
            {canBeCleared && (currentValue === undefined || currentValue === '') && (
              <DropdownToggleIcon borderless={borderless}>
                <FontAwesomeIcon icon={faChevronDown} />
              </DropdownToggleIcon>
            )}
          </>
        }
        options={options.length > 0 ? options : defaultOption}
        onChange={(event, data) => {
          setCurrentValue(data);
          onChange && onChange(event, data);
        }}
      />

      {currentValue !== undefined && canBeCleared && currentValue !== '' && (
        <ClearWrapper disable={disable}>
          <DropdownToggleIcon
            disable={disable}
            borderless={borderless}
            onClick={() => {
              if (!disable) {
                setCurrentValue('');
                onClear && onClear();
              }
            }}
          >
            <FontAwesomeIcon icon={faTimes} />
          </DropdownToggleIcon>
        </ClearWrapper>
      )}
    </Wrapper>
  );
};

export default Dropdown;

const Wrapper = styled.div<{
  description?: boolean;
  borderless?: boolean;
  borderColor?: string;
  fitToContent?: boolean;
  textWeight?: string;
  textColor?: string;
  selectionWeight?: string;
  selectionFontSize?: string;
}>`
  &&& {
    position: relative;
    
    .ui.dropdown {
      height:40px;
      padding: ${({ borderless }) => (borderless ? `10px 0` : `10px`)};
      display: flex;
      justify-content: ${({ borderless }) => (borderless ? `flex-start` : `space-between`)};
      align-items: center;
      border: ${({ theme, description, borderless, borderColor }) => {
        if (borderColor) {
          return `1px solid ${borderColor};`;
        } else if (description) {
          return `1px solid ${theme.colors.regularGrey};`;
        } else if (borderless) {
          return `none;`;
        } else {
          return `1px solid ${theme.colors.dropdown.defaultBorder};`;
        }
      }}
      border-radius: 0;
  
      &:hover {
        border: ${({ theme, description, borderless }) => {
          if (borderless) {
            return `none;`;
          } else {
            return `1px solid ${theme.colors.dropdown.hoverBorder};`;
          }
        }}
        box-shadow: none;
      }
    }

    .ui.dropdown > .text {
      color: ${({ theme, textColor }) =>
        textColor ? textColor : theme.colors.dropdown.defaultColor};
           
      max-width: calc(100% - 18px);
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      font-weight: 500;
      overflow: visible;
      
      font-size: ${({ theme, selectionFontSize }) =>
        selectionFontSize ? theme.sizes[selectionFontSize] : theme.sizes.mediumSmall}
      ${({ selectionWeight }) => selectionWeight && `font-weight: ${selectionWeight}};`}
      ${({ description }) => description && `text-transform: uppercase};`}
    }

    .ui.dropdown .menu > .item {
      ${({ theme, description }) => description && `background-color: ${theme.colors.lightGrey};`}
      padding: 10px 0;
      border: none;
      color: ${({ theme, description }) =>
        description ? `${theme.colors.plusDarkGrey};` : `${theme.colors.dropdown.defaultColor};`}
        font-size: ${({ theme, selectionFontSize }) =>
          selectionFontSize ? theme.sizes[selectionFontSize] : theme.sizes.mediumSmall}
      font-weight: ${({ description }) => (description ? `700;` : `300;`)}
      letter-spacing: 0.14px;

    
      &:hover{
        &:not(.disabled):not(.subItem){
          background-color: ${({ theme, description }) => theme.colors.primary};
          color: ${({ theme }) => theme.colors.white};
        }
      }

      &.disabled{
        &:not(.subItem){
          ${({ theme, description }) =>
            description && `background-color: ${theme.colors.softLightGrey};`}
          font-weight: 500;
        }
        opacity:0.4;
        cursor: not-allowed;
      }

      &.disabled{
        opacity:0.6;
        cursor: not-allowed;
      }
    }

    .ui.dropdown .menu > * {
      white-space: normal;
    }

    .ui.dropdown .menu > .disabled {
      opacity: 1;
      background-color: ${({ theme }) => theme.colors.white};
      font-weight: 300;
    }

    .ui.dropdown .menu > .subItem {
      opacity: 1;
      background-color: ${({ theme }) => theme.colors.white};
      font-weight: 300;
      padding: 12px 24px 12px 24px !important;
      font-size: ${({ theme }) => theme.sizes.xSmall};;

      &:hover{
        background-color: ${({ theme }) => theme.colors.white};
        color: ${({ theme, description }) =>
          description ? `${theme.colors.plusDarkGrey};` : `${theme.colors.dropdown.defaultColor};`}
      }

      &.info{
        padding: 6px 18px 6px 18px !important;
        opacity: 1;
      }
    }

    .ui.active.dropdown {
      // hack to avoid top dropdown to grow
      // whenever 2px border is given
      background-color: ${({ theme, description }) =>
        description ? theme.colors.primary : `transparent`};
      color: ${({ theme, description }) =>
        description ? theme.colors.white : theme.colors.dropdown.defaultColor};
      border: ${({ theme, description, borderless }) => {
        if (description) {
          return `1px solid ${theme.colors.primary};`;
        } else if (borderless) {
          return `none;`;
        } else {
          return `2px solid ${theme.colors.dropdown.activeBorder};`;
        }
      }}
    }

    ${({ theme, description }) =>
      description &&
      `
      .ui.active.dropdown > .text {
        color: ${theme.colors.white} !important;
      }
    `}

    .ui.dropdown > .text {
      font-size: ${({ selectionFontSize }) =>
        selectionFontSize ? `${selectionFontSize};` : `14px;`} !important;
    }

    .visible.menu.transition {
      width:${({ fitToContent }) => (fitToContent ? `100%` : `0`)};
      max-height:320px;
      overflow-x: auto;
      margin-top: 6px;
      border: 1px solid ${({ theme }) => theme.colors.regularGrey};
      border-top-width: 1px !important;
      border-radius: 0;
    }
  }
`;

const StyledDropdown = styled(SemanticDropdown)``;

const DropdownToggleIcon = styled.div<{ borderless?: boolean; disable?: boolean }>`
  display: inline-block;
  ${({ borderless }) => borderless && `margin-left: 16px;`}
  svg {
    font-size: 16px;
    cursor: ${({ disable }) => (!disable ? 'pointer' : 'default')};
  }
`;

const ClearWrapper = styled.div<{ disable?: boolean }>`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding-right: 12px;

  color: ${({ theme, disable }) =>
    !disable ? theme.colors.plusDarkGrey : theme.colors.softRegularGrey};
`;
