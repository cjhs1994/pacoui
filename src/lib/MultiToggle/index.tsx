/**
 * MultiToggle component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React, { useState, useLayoutEffect, useRef, useContext } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft, faAngleRight } from '@fortawesome/pro-light-svg-icons';

import Button from '../Button';
import theme from '../theme';

interface IButtons {
  onClick?: () => void;
  children: string | JSX.Element;
  selected: boolean;
  testId?: string;
}
interface IMultiToggle {
  buttons: IButtons[];
  content: 'buttons' | 'text' | 'icons';
  borderHighlight?: boolean;
  fontSize?: string;
  fontWeight?: string;
  textTransform?: string;
  scrollableAtWidth?: string | number;
  grow?: boolean;
  buttonsPadding?: string;
}

const MultiToggle: React.FC<IMultiToggle> = ({
  buttons,
  content,
  fontSize,
  borderHighlight,
  fontWeight,
  textTransform,
  scrollableAtWidth,
  buttonsPadding,
  grow,
}) => {
  const [leftScroll, setLeftScroll] = useState(false);
  const [rightScroll, setRightScroll] = useState(false);

  const childrenWrapper = useRef<HTMLDivElement>(null);

  const handleOnClick = (value) => {
    if (childrenWrapper.current !== null) {
      childrenWrapper.current.scrollBy({ left: value, behavior: 'smooth' });
    }
  };

  useLayoutEffect(() => {
    let cleanupFunction: any = null;

    const onScroll = ({ target }) => {
      if (target.scrollLeft > 0) {
        setLeftScroll(true);
      } else {
        setLeftScroll(false);
      }

      if (childrenWrapper.current && scrollableAtWidth) {
        if (
          childrenWrapper.current.scrollWidth <
          Math.ceil(scrollableAtWidth + target.scrollLeft) + 3
        ) {
          setRightScroll(false);
        } else {
          setRightScroll(true);
        }
      }
    };

    if (childrenWrapper.current && scrollableAtWidth) {
      cleanupFunction && cleanupFunction.removeEventListener('scroll', onScroll);
      cleanupFunction = childrenWrapper.current.addEventListener('scroll', onScroll);
      if (childrenWrapper.current.scrollWidth > scrollableAtWidth) {
        setRightScroll(true);
      }
    }

    return () => {
      cleanupFunction && cleanupFunction.removeEventListener('scroll', onScroll);
    };
  }, [buttons, scrollableAtWidth]);

  return (
    <Wrapper scrollableAtWidth={scrollableAtWidth}>
      <WrapperButtons
        content={content}
        ref={childrenWrapper}
        scrollableAtWidth={scrollableAtWidth}
        grow={grow}
      >
        {buttons &&
          buttons.map((button: IButtons, index: number) => {
            return (
              <WrapperButton
                borderHighlight={borderHighlight}
                key={`MultiToggle-${index}`}
                onClick={() => {
                  button.onClick && button.onClick();
                }}
                selected={button.selected}
                content={content}
                fontSize={fontSize}
                fontWeight={fontWeight}
                textTransform={textTransform}
                data-testid={button.testId}
              >
                <ButtonContainer padding={buttonsPadding} key={`MultiToggleButton-${index}`}>
                  {button.children}
                </ButtonContainer>
              </WrapperButton>
            );
          })}
      </WrapperButtons>
      {scrollableAtWidth && (
        <>
          {leftScroll && (
            <NavigationIcon direction="to left">
              <Button
                noPadding
                color={theme.colors.darkGrey}
                borderless
                size="26px"
                onClick={() => handleOnClick(-50)}
              >
                <FontAwesomeIcon icon={faAngleLeft} />
              </Button>
            </NavigationIcon>
          )}

          {rightScroll && (
            <NavigationIcon direction="to right">
              <Button
                noPadding
                color={theme.colors.darkGrey}
                borderless
                size="26px"
                onClick={() => handleOnClick(50)}
              >
                <FontAwesomeIcon icon={faAngleRight} />
              </Button>
            </NavigationIcon>
          )}
        </>
      )}
    </Wrapper>
  );
};

export default MultiToggle;

const Wrapper = styled.div<{ scrollableAtWidth?: number | string; grow?: boolean }>`
  position: relative;
  max-width: ${({ scrollableAtWidth }) =>
    scrollableAtWidth
      ? typeof scrollableAtWidth == 'number'
        ? `${scrollableAtWidth}px`
        : scrollableAtWidth
      : `100%`};
  margin-bottom: -1px;
`;

const WrapperButtons = styled.div<{
  content: 'buttons' | 'text' | 'icons';
  scrollableAtWidth?: number | string;
  grow?: boolean;
}>`
  display: flex;
  align-items: center;
  ${({ scrollableAtWidth }) =>
    scrollableAtWidth &&
    `
  overflow-y: hidden;
  overflow-x: auto;
  `};

  // Hide scrollbar
  // Chrome, Safari and Opera
  &::-webkit-scrollbar {
    display: none;
  }

  // IE and Edge
  -ms-overflow-style: none;

  // Firefox
  scrollbar-width: none;

  ${({ content }) =>
    content !== 'buttons' &&
    `
    > div:first-child {
      button {
        svg {
          margin-left: 0;
        }
      }
    }

    > div:last-child {
      button {
        svg {
          margin-right: 0;
        }
      }
    }
  `}

  justify-content: ${({ grow }) => (grow ? 'space-between' : '')};
  width: ${({ grow }) => (grow ? '100%' : '')};
`;

const WrapperButton = styled.div<{
  selected: boolean;
  content: 'buttons' | 'text' | 'icons';
  fontSize?: string;
  fontWeight?: string;
  textTransform?: string;
  borderHighlight?: boolean;
}>`
  && {
    button {
      margin: 0;

      ${({ theme }) => `
        color: ${theme.colors.primary};
        border: 1px solid ${theme.colors.primary};
      `}

      ${({ theme, selected }) =>
        selected &&
        `
        color: ${theme.colors.white};
        background: ${theme.colors.primary};
      `}
    }

    &:not(:first-child) {
      button {
        border-left: 0;
      }
    }

    ${({ theme, content, selected, borderHighlight, fontSize, fontWeight, textTransform }) =>
      content !== 'buttons' &&
      `
      button {
        padding-right: 10px;
        padding-left: 10px;
        color: ${selected ? theme.colors.white : theme.colors.primary};
        border: none;
        box-shadow: ${
          selected && borderHighlight ? `0px -3px 0px 0px ${theme.colors.primary} inset` : 'none'
        };
        
        background: ${selected ? theme.colors.primary : theme.colors.white};
        font-size: ${fontSize ? fontSize : `21px`};
        font-weight: ${fontWeight ? fontWeight : `400`};
        text-align: center;
        text-transform: ${textTransform ? textTransform : `none`};
      }
    `}

    button:hover {
      ${({ theme }) => `
      color: ${theme.colors.white};
      background-color: ${theme.colors.primary};
    `}
    }
  }
`;

const NavigationIcon = styled.div<{ direction: 'to right' | 'to left' }>`
  position: absolute;
  bottom: 0;
  top: 0;
  left: ${({ direction }) => (direction === 'to right' ? `unset` : `0`)};
  right: ${({ direction }) => (direction === 'to left' ? `unset` : `0`)};
  display: flex;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 32px;
  height: 100%;
  width: 20px;
  background-color: ${({ theme }) => theme.colors.blackLight};
  cursor: pointer;

  svg {
    color: ${({ theme }) => theme.colors.white};
  }
`;

const ButtonContainer = styled(Button)<{ padding?: string }>`
  padding: ${({ padding }) => (padding ? padding : '')};
`;
