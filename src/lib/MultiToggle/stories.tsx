import React from 'react';
import { storiesOf } from '@storybook/react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExchange, faTable, faList, faExpand } from '@fortawesome/pro-light-svg-icons';

import MultiToggle from './index';

// TO-DO: refactor the onClicks approach
// get a cleaner/handy solution

const multiToggle = [
  {
    onClick: () => console.log('Button 1'),
    children: 'Button 1',
    selected: true,
  },
  {
    onClick: () => console.log('Button 2'),
    children: 'Button 2',
    selected: false,
  },
];

const icons = [
  {
    onClick: () => console.log('Exchange'),
    children: <FontAwesomeIcon icon={faExchange} />,
    selected: true,
  },
  {
    onClick: () => console.log('Table'),
    children: <FontAwesomeIcon icon={faTable} />,
    selected: false,
  },
  {
    onClick: () => console.log('List'),
    children: <FontAwesomeIcon icon={faList} />,
    selected: false,
  },
  {
    onClick: () => console.log('Expand'),
    children: <FontAwesomeIcon icon={faExpand} />,
    selected: false,
  },
];

storiesOf('Components/MultiToggle', module)
  .add('MultiToggle', () => <MultiToggle buttons={multiToggle} content={'buttons'}></MultiToggle>)
  .add('MultiToggle Icons', () => <MultiToggle buttons={icons} content={'icons'}></MultiToggle>);
