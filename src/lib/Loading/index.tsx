import React from 'react';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/pro-regular-svg-icons';

import theme from '../theme';

interface IProps {
  size: '1x' | '2x' | '3x' | '4x' | '5x';
  overlay?: boolean;
  invert?: boolean;
}

const Loading: React.FC<IProps> = ({ size, overlay, invert }) => {
  return (
    <WrapperOverlay overlay={overlay}>
      <FontAwesomeIcon
        icon={faSpinner}
        spin
        size={size}
        color={invert ? theme.colors.white : theme.colors.primary}
      />
    </WrapperOverlay>
  );
};

export default Loading;

const WrapperOverlay = styled.div<{ overlay?: boolean }>`
  ${({ overlay }) =>
    `position: absolute;
    z-index: 799;
    top: 0;
    left: 0;
    ${
      overlay &&
      `
      width: 100%;
      height: 100%;
      background: rgba(255, 255, 255, 0.5) center center no-repeat;
      display:flex;
      justify-content:center;
      align-items:center;
  `
    }`}
`;
