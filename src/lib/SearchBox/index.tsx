/**
 * SearchBox component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React, { useRef } from 'react';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/pro-light-svg-icons';

import { debounce } from './utils';

import theme from '../theme';

interface IProps {
  borderColor?: string;
  iconColor?: string;
  height?: number;
  placeholder?: string;
  onSearch?: (value: string) => void;
  delay?: number;
  defaultValue?: string;
}

const SearchBox: React.FC<IProps> = ({
  borderColor,
  iconColor,
  height,
  placeholder,
  onSearch,
  delay,
  defaultValue,
}) => {
  const debounceSearch: (value: string) => void = useRef(
    debounce((value: string) => {
      onSearch && onSearch(value);
    }, delay || 0)
  ).current;

  return (
    <Wrapper borderColor={borderColor} iconColor={iconColor} height={height}>
      <input
        type="text"
        placeholder={placeholder}
        defaultValue={defaultValue ? defaultValue : undefined}
        onChange={({ target }) => {
          debounceSearch(target.value);
        }}
      ></input>
      <FontAwesomeIcon icon={faSearch} />
    </Wrapper>
  );
};

export default SearchBox;

const Wrapper = styled.div<{ borderColor?: string; iconColor?: string; height?: number }>`
  width: 100%;
  height: ${({ height }) => (height ? `${height}px` : `40px`)};
  position: relative;
  border: ${({ theme, borderColor }) => (borderColor ? `1px solid ${borderColor}` : `none`)};

  input {
    width: 100%;
    height: 100%;
    padding: 5px 15px;
    border: 0;
    background-color: ${({ theme }) => theme.colors.white};

    &:focus {
      outline: none;
    }
  }

  svg {
    position: absolute;
    top: 10px;
    right: 15px;
    color: ${({ theme, iconColor }) => (iconColor ? iconColor : theme.colors.primary)};
    font-size: 18px;
  }

  &:hover {
    border: ${({ theme }) => {
      return `1px solid ${theme.colors.dropdown.hoverBorder};`;
    }};
  }
`;
