/**
 * AnimatedBackground component
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */

import React from 'react';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';

import theme from '../theme';

interface IProps {
  height?: string;
  width?: string;
}

const AnimatedBackground: React.FC<IProps> = ({ height, width }) => {
  return (
    <SkeletonTheme color={theme.colors.lightGrey} highlightColor={theme.colors.softRegularGrey}>
      <Skeleton style={{ borderRadius: 0 }} height={height || '12px'} width={width || '100%'} />
    </SkeletonTheme>
  );
};

export default AnimatedBackground;
