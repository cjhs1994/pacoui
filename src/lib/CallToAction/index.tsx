/**
 * CallToAction container
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleRight } from '@fortawesome/pro-light-svg-icons';

import theme from '../theme';

interface IProps {
  body?: React.ReactNode;
  footer?: React.ReactNode;
  topBorder?: boolean;
  topBorderFooter?: boolean;
  padding?: string;
  noPointer?: boolean;
  onAction?: () => void;
  footerIcon?: React.ReactNode;
  footerArrow?: boolean | React.ReactNode;
}
const CallToAction: React.FC<IProps> = ({
  body,
  footer,
  topBorder,
  padding,
  noPointer,
  onAction,
  footerIcon,
  footerArrow,
}) => {
  return (
    <Wrapper topBorder={topBorder}>
      {body && <WrapperBody>{body}</WrapperBody>}
      <WrapperFooter
        onClick={() => {
          onAction && onAction();
        }}
        padding={padding}
        noPointer={noPointer}
      >
        {footerIcon && <FooterIcon>{footerIcon}</FooterIcon>}

        <FooterContent>{footer}</FooterContent>

        {footerArrow && typeof footerArrow === 'boolean' && (
          <FooterArrow>
            <FontAwesomeIcon color={theme.colors.darkGrey} size="2x" icon={faAngleRight} />
          </FooterArrow>
        )}
        {footerArrow && typeof footerArrow !== 'boolean' && (
          <FooterArrow>{footerArrow}</FooterArrow>
        )}
      </WrapperFooter>
    </Wrapper>
  );
};

export default CallToAction;

// border-top: 4px solid ${({ theme }) => theme.colors.primary};
const Wrapper = styled.div<{ topBorder?: boolean }>`
  display: flex;
  flex-direction: column;
  background-color: ${({ theme }) => theme.colors.white};
  color: ${({ theme }) => theme.colors.grey};
  z-index: 1;
  border: 1px solid ${({ theme }) => theme.colors.lightGrey};
  border-top: ${({ theme, topBorder }) => (topBorder ? '4px solid ' + theme.colors.primary : '')};
`;

const WrapperBody = styled.div`
  height: 100%;
  flex: 0.7;
  padding: 60px 30px 70px 30px;
`;

const WrapperFooter = styled.div<{
  topBorderFooter?: boolean;
  padding?: string;
  noPointer?: boolean;
}>`
  flex: 0.3;
  border-top: ${({ theme, topBorderFooter }) =>
    topBorderFooter ? '1px solid ' + theme.colors.regularGrey : ''};
  background-color: ${({ theme }) => theme.colors.softGrey};
  padding: ${({ padding }) => (padding ? padding : '14px 24px')};
  cursor: ${({ noPointer }) => (noPointer ? 'initial' : 'pointer')};
  display: flex;
  align-items: center;

  div:nth-child(3) {
    display: flex;
    justify-content: flex-end;
    align-items: center;
  }

  :hover {
    opacity: ${({ noPointer }) => (noPointer ? 1 : 0.8)};
  }
`;

const FooterContent = styled.div`
  flex: 1;
`;

const FooterIcon = styled.div`
  margin-right: 24px;
`;

const FooterArrow = styled.div`
  margin-left: 24px;
`;
