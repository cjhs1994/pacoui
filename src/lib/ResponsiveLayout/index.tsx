/**
 * ResponsiveLayout container
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */
import React, { useState, useEffect } from 'react';
interface IProps extends Omit<React.ComponentProps<any>, ''> {
  web: any;
  mobile?: any;
  tablet?: any;
  largeScreen?: any;
  wideScreen?: any;
  contextualProps?: {
    web?: any;
    mobile?: any;
    tablet?: any;
    largeScreen?: any;
    wideScreen?: any;
  };
}
const AppMedia = {
  mobile: 320,
  tablet: 768,
  web: 1320,
  largeScreen: 1920,
};

const ResponsiveLayout: any = ({
  web,
  mobile,
  tablet,
  largeScreen,
  wideScreen,
  contextualProps,
  ...props
}) => {
  const [width, setWidth] = useState(window.innerWidth);
  const checkSize = () => {
    if (width > AppMedia.largeScreen && wideScreen) {
      const WideScreen = wideScreen;
      const contextProps = contextualProps?.wideScreen ? contextualProps.wideScreen : {};
      return <WideScreen {...contextProps} {...props} />;
    }
    if (width <= AppMedia.largeScreen && width > AppMedia.web && largeScreen) {
      const LargeScreen = largeScreen;
      const contextProps = contextualProps?.largeScreen ? contextualProps.largeScreen : {};
      return <LargeScreen {...contextProps} {...props} />;
    }
    if (width <= AppMedia.web && width > AppMedia.tablet) {
      const Web = web;
      const contextProps = contextualProps?.web ? contextualProps.web : {};
      return <Web {...contextProps} {...props} />;
    }
    if (width <= AppMedia.tablet && width > AppMedia.mobile && tablet) {
      const Tablet = tablet ? tablet : web;
      const contextProps = contextualProps?.tablet ? contextualProps.tablet : {};
      return <Tablet {...contextProps} {...props} />;
    }
    if (width <= AppMedia.mobile && mobile) {
      const Mobile = mobile ? mobile : web;
      const contextProps = contextualProps?.mobile ? contextualProps.mobile : {};
      return <Mobile {...contextProps} {...props} />;
    }

    const Web = web;
    return <Web {...props} />;
  };
  useEffect(() => {
    const resize = () => {
      setWidth(window.innerWidth);
    };
    window.addEventListener('resize', resize);
    return () => {
      window.removeEventListener('resize', resize);
    };
  }, []);

  return checkSize();
};
export default ResponsiveLayout;
