/**
 * Segment component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import theme from '../theme';

interface ISegment {
  children: JSX.Element | JSX.Element[] | any; // TODO: remove any
  borderTopColor?: string;
  padding?: string;
  footer?: React.ReactNode;
  grow?: boolean;
  backgroundColor?: string;
  borderColor?: string;
  verticalCenter?: boolean;
}

const Segment: React.FC<ISegment> = ({
  grow,
  children,
  footer,
  padding,
  borderTopColor,
  backgroundColor,
  borderColor,
  verticalCenter,
}) => {
  return (
    <Wrapper grow={grow} backgroundColor={backgroundColor}>
      <Content
        grow={grow}
        padding={padding}
        verticalCenter={verticalCenter}
        borderTopColor={borderTopColor}
        borderColor={borderColor}
      >
        {children}
      </Content>
      {footer && <Footer>{footer}</Footer>}
    </Wrapper>
  );
};

export default Segment;

const Wrapper = styled.div<{ grow?: boolean; backgroundColor?: string }>`
  ${({ grow }) =>
    grow &&
    `
    display:flex;
    flex-direction: column;
    height:100%;
  `}

  background-color: ${({ backgroundColor }) => (backgroundColor ? backgroundColor : 'undefined')}
`;

const Content = styled.div<{
  padding?: string;
  borderTopColor?: string;
  grow?: boolean;
  borderColor?: string;
  verticalCenter?: boolean;
}>`
  padding: ${({ padding }) => (padding ? padding : `38px`)};
  border: 1px solid
    ${({ theme, borderColor }) => (borderColor ? borderColor : theme.colors.lightGrey)};
  border-top: ${({ theme, borderTopColor, borderColor }) =>
    borderTopColor
      ? `3px solid  ${borderTopColor}`
      : borderColor
      ? `1px solid ${borderColor}`
      : `1px solid  ${theme.colors.lightGrey}`};

  ${({ verticalCenter }) =>
    verticalCenter &&
    `
      display: flex;
      align-items: center;
    `}

  ${({ grow }) =>
    grow &&
    `
    flex-grow:1;
  `}
`;

const Footer = styled.div`
  margin-top: -1px;
`;
