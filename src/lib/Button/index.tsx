/**
 * Button component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import AnimatedBackground from '../AnimatedBackground';

import theme from '../theme';

interface IButton {
  disabled?: boolean;
  primary?: boolean;
  full?: boolean;
  decision?: boolean;
  borderless?: boolean;
  borderlessHoverColor?: string;
  color?: string;
  success?: boolean;
  danger?: boolean;
  action?: boolean;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  children?: React.ReactNode;
  leftIcon?: React.ReactNode;
  rightIcon?: React.ReactNode;
  noPadding?: boolean;
  size?: string;
  loading?: boolean | undefined;
  loadingProps?: any;
  label?: string;
  labelColor?: 'primary' | 'dangerRed' | 'successGreen';
  className?: any;
}

const Button: React.FC<IButton> = ({
  disabled,
  primary,
  full,
  decision,
  danger,
  success,
  color,
  action,
  onClick,
  borderless,
  children,
  leftIcon,
  rightIcon,
  noPadding,
  size,
  loading,
  loadingProps,
  label,
  labelColor,
  borderlessHoverColor,
  className,
  ...rest
}) => {
  const textOnly = typeof children === 'string' && !leftIcon && !rightIcon;

  // loading height
  const getLoadingSize = () => {
    return size ? theme.sizes[size] : '10px';
  };

  const getInverseColor = () => {
    const colors = {
      [theme.colors.plusDarkGrey]: theme.colors.primary,
      [theme.colors.blackLight]: theme.colors.primary,
      [theme.colors.black]: theme.colors.primary,
      [theme.colors.darkGrey]: theme.colors.primary,
      [theme.colors.grey]: theme.colors.primary,
      [theme.colors.white]: theme.colors.plusDarkGrey,
      [theme.colors.regularGrey]: theme.colors.plusDarkGrey,
      [theme.colors.softRegularGrey]: theme.colors.plusDarkGrey,
      [theme.colors.softLightGrey]: theme.colors.plusDarkGrey,
      [theme.colors.softGrey]: theme.colors.plusDarkGrey,
      [theme.colors.lightGrey]: theme.colors.plusDarkGrey,
    };

    if (color && colors[color]) {
      return colors[color];
    }

    return null;
  };

  return (
    <StyledButton
      primary={primary}
      full={full}
      disabled={disabled}
      textOnly={textOnly}
      decision={decision}
      danger={danger}
      success={success}
      action={action ? 1 : 0}
      onClick={(e) => {
        !disabled && onClick && onClick(e);
      }}
      borderless={borderless}
      color={color}
      borderlessHoverColor={borderlessHoverColor}
      noPadding={noPadding}
      size={size}
      inverseColor={getInverseColor()}
      className={className}
      {...rest}
    >
      {label && <Label color={labelColor}>{label}</Label>}

      {!loading && (
        <Content loading={loading ? 1 : 0}>
          {leftIcon && <LeftIcon>{leftIcon}</LeftIcon>}
          {children}
          {rightIcon && <RightIcon>{rightIcon}</RightIcon>}
        </Content>
      )}

      {loading && (
        <AnimatedBackground
          height={getLoadingSize()}
          {...{ ...loadingProps, width: loadingProps?.width ? loadingProps.width : '100%' }}
        >
          <Content loading={loading ? 1 : 0}>
            {leftIcon && <LeftIcon>{leftIcon}</LeftIcon>}
            {children}
            {rightIcon && <RightIcon>{rightIcon}</RightIcon>}
          </Content>
        </AnimatedBackground>
      )}
    </StyledButton>
  );
};

const StyledButton = styled.button<{
  primary?: boolean;
  full?: boolean;
  disabled?: boolean;
  decision?: boolean;
  success?: boolean;
  danger?: boolean;
  action?: number;
  borderless?: boolean;
  textOnly?: boolean;
  noPadding?: boolean;
  size?: string;
  borderlessHoverColor?: string;
  loading?: boolean;
  inverseColor?: string | null;
}>`
  position: relative;
  pointer-events: ${({ disabled }) => (disabled ? `none` : `auto`)};
  display: flex;
  height: ${({ noPadding }) => (noPadding ? `auto` : `40px`)};
  justify-content: center;
  align-items: center;
  padding: ${({ textOnly, noPadding }) => (noPadding ? `0` : textOnly ? `0 32px` : `0 12px`)};
  background: none;
  letter-spacing: 0.14px;
  font-size: ${({ size }) => (size ? size : `14px`)};
  color: ${({ theme, color }) => (color ? color : theme.colors.white)};
  font-weight: 400;
  border: ${({ theme, color, borderless }) =>
    borderless ? `none` : `1px solid ${color ? color : theme.colors.white}`};
  opacity: ${({ disabled }) => (disabled ? 0.5 : 1)};
  border-radius: 0;
  text-transform: capitalize;

  &: hover {
    cursor: pointer;
    color: ${({ borderless, theme, inverseColor, borderlessHoverColor, color }) =>
      borderless &&
      (borderlessHoverColor
        ? borderlessHoverColor
        : inverseColor
        ? inverseColor
        : color
        ? color
        : theme.colors.white)};
  }

  &: focus {
    outline: 0;
  }

  ${({ theme, decision, full, borderless }) =>
    decision &&
    `
    color: ${full ? theme.colors.white : theme.colors.regularGrey};
    border: ${borderless ? `none` : `1px solid ${theme.colors.regularGrey}`};
    background: ${!full ? `none` : theme.colors.regularGrey};

    &:hover {
      background: ${borderless ? `none` : theme.colors.regularGrey};
      color: ${borderless ? theme.colors.decisionYellow : theme.colors.white};
    }
  `}

  ${({ theme, success, full, borderless }) =>
    success &&
    `
    color: ${full ? theme.colors.white : theme.colors.successGreen};
    border: ${borderless ? `none` : `1px solid ${theme.colors.successGreen}`};
    background: ${!full ? `none` : theme.colors.successGreen};

    &:hover {
      background: ${borderless ? `none` : theme.colors.successGreen};
      color: ${borderless ? theme.colors.successGreen : theme.colors.white};
    }
  `}

  ${({ theme, danger, full, borderless }) =>
    danger &&
    `
    color: ${full ? theme.colors.white : theme.colors.dangerRed};
    border: ${borderless ? `none` : `1px solid ${theme.colors.dangerRed}`};
    background: ${!full ? `none` : theme.colors.dangerRed};

    &:hover {
      background: ${borderless ? `none` : theme.colors.dangerRed};
      color: ${borderless ? theme.colors.dangerRed : theme.colors.white};
    }
  `}

  ${({ theme, action, full, borderless, color }) =>
    action &&
    `
      color: ${color ? color : full ? theme.colors.white : theme.colors.plusDarkGrey};
      border: ${
        borderless
          ? `none`
          : color
          ? `1px solid ${color}`
          : `1px solid ${theme.colors.plusDarkGrey}`
      };
      background: ${!full ? `none` : theme.colors.primary};

      &:hover {
        background: ${borderless ? `none` : theme.colors.primary};
        color: ${borderless ? theme.colors.primary : theme.colors.white};
        border: ${borderless ? `none` : `1px solid ${theme.colors.primary}`};
      }
    `}

    ${({ theme, full, primary, borderless }) =>
    primary &&
    `
      color: ${full ? theme.colors.white : theme.colors.primary};
      border: ${borderless ? `none` : `1px solid ${theme.colors.primary}`};
      background: ${!full ? `none` : theme.colors.primary};

      div {
        color: ${full ? theme.colors.white : theme.colors.primary};
      }

      &:disabled {
        border: 1px solid ${theme.colors.regularGrey};

        div {
          color: ${theme.colors.regularGrey};
        }
      }

      &:hover {
        background: ${
          borderless ? `none` : full ? theme.colors.primaryShadow : theme.colors.primary
        };
        
        div {
          color: ${borderless ? theme.colors.primary : theme.colors.white};
        }
      }
    `}


  &: focus {
    outline: none;
  }

  &: active {
    outline: none;
  }
`;

const LeftIcon = styled.div`
  display: flex;
  align-items: center;
  margin-right: 10px;
`;
const RightIcon = styled.div`
  display: flex;
  align-items: center;
  margin-left: 10px;
`;

const Content = styled.div<{ loading?: number | boolean | undefined }>`
  visibility: ${({ loading }) => (loading ? `hidden` : `visible`)};
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Label = styled.div<{ color?: string }>`
  position: absolute;
  height: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 12px;
  padding: 4px 8px;
  background-color: ${({ theme, color }) =>
    color ? theme.colors[color] || theme.colors.darkGrey : theme.colors.darkGrey};
  top: -12px;
  color: ${({ theme }) => theme.colors.white};
  right: -12px;
  font-size: ${({ theme }) => theme.sizes.s};
`;

export default Button;
