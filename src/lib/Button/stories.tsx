import React from 'react';
import { storiesOf } from '@storybook/react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/pro-regular-svg-icons';
import { Story, Meta } from '@storybook/react/types-6-0';

import Button from './index';
import { argTypes } from './types';

export default {
  title: 'Components/Buttons/Default Buttons',
  component: Button,
  argTypes: argTypes,
} as Meta;

const Template: Story = (args) => (
  <Bg>
    <Button {...args}>{args.content}</Button>
  </Bg>
);

export const Default = Template.bind({});
Default.args = {
  content: 'Default Button',
};

export const DefaultDisabled = Template.bind({});
DefaultDisabled.args = {
  content: 'Default Button',
  disabled: true,
};

export const DefaultIconOnly = Template.bind({});
DefaultIconOnly.args = {
  content: <FontAwesomeIcon icon={faBars} />,
};

export const DefaultWithIconLeft = Template.bind({});
DefaultWithIconLeft.args = {
  content: 'Default Button',
  leftIcon: <FontAwesomeIcon icon={faBars} />,
};

export const DefaultWithIconRight = Template.bind({});
DefaultWithIconRight.args = {
  content: 'Default Button',
  rightIcon: <FontAwesomeIcon icon={faBars} />,
};

export const DefaultBorderless = Template.bind({});
DefaultBorderless.args = {
  content: 'Default Button',
  borderless: true,
};

export const DefaultBorderlessWithIconLeft = Template.bind({});
DefaultBorderlessWithIconLeft.args = {
  content: 'Default Button',
  borderless: true,
  leftIcon: <FontAwesomeIcon icon={faBars} />,
};

export const DefaultBorderlessWithIconRight = Template.bind({});
DefaultBorderlessWithIconRight.args = {
  content: 'Default Button',
  borderless: true,
  rightIcon: <FontAwesomeIcon icon={faBars} />,
};

export const DefaultBorderlesIconOnly = Template.bind({});
DefaultBorderlesIconOnly.args = {
  content: <FontAwesomeIcon icon={faBars} />,
  borderless: true,
};

const Bg = styled.div`
  background-color: ${({ theme }) => theme.colors.primary};
  height: 100vh;
`;
