export const argTypes = {
  disabled: {
    name: 'disabled',
    defaultValue: false,
    control: { type: 'boolean' },
  },
  primary: {
    name: 'primary',
    defaultValue: false,
    //description: 'Primary action button',
    control: { type: 'boolean' },
  },
  full: {
    name: 'full',
    defaultValue: false,
    control: { type: 'boolean' },
  },
  decision: {
    name: 'decision',
    defaultValue: false,
    control: { type: 'boolean' },
  },
  success: {
    name: 'success',
    defaultValue: false,
    control: { type: 'boolean' },
  },
  danger: {
    name: 'danger',
    defaultValue: false,
    control: { type: 'boolean' },
  },
  action: {
    name: 'action',
    defaultValue: false,
    control: { type: 'boolean' },
  },
  noPadding: {
    name: 'noPadding',
    defaultValue: false,
    control: { type: 'boolean' },
  },
  loading: {
    name: 'loading',
    defaultValue: false,
    control: { type: 'boolean' },
  },
  borderlessHoverColor: {
    name: 'borderlessHoverColor',
    defaultValue: undefined,
    control: { type: 'color' },
  },
  color: {
    name: 'color',
    defaultValue: undefined,
    control: { type: 'color' },
  },
  size: {
    name: 'size',
    defaultValue: undefined,
    control: { type: 'text' },
  },
  label: {
    name: 'label',
    defaultValue: undefined,
    control: { type: 'text' },
  },
  labelColor: {
    name: 'labelColor',
    defaultValue: undefined,
    options: ['primary', 'dangerRed', 'successGreen'],
    control: { type: 'select' },
  },
};
