import React from 'react';
import { storiesOf } from '@storybook/react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/pro-regular-svg-icons';
import { Story, Meta } from '@storybook/react/types-6-0';

import Button from './index';
import { argTypes } from './types';

export default {
  title: 'Components/Buttons/Action Buttons',
  component: Button,
  argTypes: argTypes,
} as Meta;

const Template: Story = (args) => <Button {...args}>{args.content}</Button>;

export const Action = Template.bind({});
Action.args = {
  content: 'Action Button',
  action: true,
};

export const ActionWithLabel = Template.bind({});
ActionWithLabel.args = {
  content: 'Action Button',
  action: true,
  label: 'label',
  labelColor: 'dangerRed',
};

export const Primary = Template.bind({});
Primary.args = {
  content: 'Action Button',
  primary: true,
};

export const Decision = Template.bind({});
Decision.args = {
  content: 'Action Button',
  decision: true,
};

export const Success = Template.bind({});
Success.args = {
  content: 'Action Button',
  success: true,
};

export const Danger = Template.bind({});
Danger.args = {
  content: 'Action Button',
  danger: true,
};
