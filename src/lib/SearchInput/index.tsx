/**
 * SearchInput component
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */

import React, { useRef, useContext } from 'react';
import styled from 'styled-components';
import { Transition, Dropdown } from 'semantic-ui-react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faSpinner } from '@fortawesome/pro-light-svg-icons';

import { debounce } from '../../utils';

import Button from '../Button';
import theme from '../theme';

interface IProps {
  borderColor?: string;
  iconColor?: string;
  height?: number;
  enterAnimation?: boolean;
  placeholder?: string;
  onSearch?: (value: string) => void;
  onCancel?: () => void;
  delay?: number;
  visible?: boolean;
  loadingSearchResults?: boolean;
  setLoadingSearchResults?: (state) => void;
  onSelect?: Function;
  results?: { key: string; text: string; value: string }[];
  resetSearchResults?: () => void;
}

const SearchInput: React.FC<IProps> = ({
  iconColor,
  height,
  placeholder,
  onSearch,
  enterAnimation,
  onCancel,
  results,
  delay,
  onSelect,
  resetSearchResults,
  loadingSearchResults,
  setLoadingSearchResults,
  visible,
}) => {
  const debounceSearch: (value: string) => void = useRef(
    debounce((value: string) => {
      onSearch && onSearch(value);
    }, delay || 0)
  ).current;

  return (
    <Transition.Group animation={'fade left'} duration={200}>
      {visible && (
        <Wrapper enterAnimation={enterAnimation} iconColor={iconColor} height={height}>
          <input
            type="text"
            placeholder={placeholder}
            onChange={({ target }) => {
              setLoadingSearchResults && setLoadingSearchResults(true);
              debounceSearch(target.value);
            }}
          />
          <CancelButton
            color={theme.colors.regularGrey}
            onClick={() => {
              !loadingSearchResults && onCancel && onCancel();
              !loadingSearchResults && resetSearchResults && resetSearchResults();
            }}
            borderless
          >
            <FontAwesomeIcon
              spin={loadingSearchResults}
              size="lg"
              icon={!loadingSearchResults ? faTimes : faSpinner}
            />
          </CancelButton>
          <DropdownWrapper>
            <Dropdown
              fluid
              selectOnNavigation={true}
              scrolling
              floating
              onChange={(e, data) => {
                onSelect && onSelect(data);
              }}
              icon={null}
              options={results || []}
              trigger={<></>}
              open={results && results.length > 0 ? true : false}
            />
          </DropdownWrapper>
        </Wrapper>
      )}
    </Transition.Group>
  );
};

export default SearchInput;

const Wrapper = styled.div<{
  iconColor?: string;
  height?: number;
  enterAnimation?: boolean;
}>`
  max-width: 360px;
  min-width: 120px;
  width: 360px;
  height: ${({ height }) => (height ? `${height}px` : `38px`)};
  position: relative;
  border: none;

  input {
    width: 100%;
    height: 100%;
    padding: 5px 15px 5px 2px;
    border: 0;
    border-bottom: 1px solid ${({ theme }) => theme.colors.institutional};
    background-color: transparent;

    &:focus {
      outline: none;
    }
  }
`;

const DropdownWrapper = styled.div`
  position: absolute;
  top: 42px;
  max-width: 360px;
  min-width: 120px;
  width: 360px;

  &&& {
    .menu {
      width: 100%;
      border-radius: 0 !important;
      color: ${({ theme }) => theme.colors.plusDarkGrey};

      .item {
        &.selected {
        }

        &:hover {
          background-color: ${({ theme }) => theme.colors.primary} !important;
          color: ${({ theme }) => theme.colors.white};
        }
      }
    }
  }
`;

const CancelButton = styled(Button)`
  position: absolute;
  right: -4px;
  top: 0px;
`;
