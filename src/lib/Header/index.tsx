/**
 * Header component
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import AnimatedBackground from '../AnimatedBackground';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/pro-light-svg-icons';

import Text from '../Text';
import Button from '../Button';
import theme from '../theme';

interface IProps {
  title?: any;
  onBack?: () => void;
  pullBackButton?: boolean;
  loading?: boolean;
}

const Header: React.FC<IProps> = ({ title, onBack, loading, pullBackButton }) => {
  return (
    <Wrapper backFunc={onBack ? true : false}>
      {loading && <AnimatedBackground height={theme.sizes.xxLarge} width="100%" />}
      {!loading && (
        <>
          {onBack && (
            <BackButton pullBackButton={pullBackButton}>
              <Button
                color={theme.colors.plusDarkGrey}
                borderless
                size="20px"
                onClick={() => {
                  onBack();
                }}
              >
                <FontAwesomeIcon icon={faArrowLeft} />
              </Button>
            </BackButton>
          )}
          {title &&
            (typeof title === 'string' ? (
              <Title
                backFunc={onBack ? true : false}
                weight="medium"
                color="plusDarkGrey"
                transform="capitalize"
                size="xLarge"
              >
                {title}
              </Title>
            ) : title.props.tKey ? (
              <Title
                backFunc={onBack ? true : false}
                weight="medium"
                transform="capitalize"
                color="plusDarkGrey"
                size="xLarge"
              >
                {title}
              </Title>
            ) : (
              title
            ))}
        </>
      )}
    </Wrapper>
  );
};

export default Header;

const Wrapper = styled.div<{ backFunc: boolean }>`
  display: flex;
  align-items: center;
  margin-left: ${({ backFunc }) => (backFunc ? `-12px` : `0`)};
  height: 40px;
  text-transform: capitalize;
`;

const Title = styled(Text)<{ backFunc: boolean }>`
  margin-left: ${({ backFunc }) => (backFunc ? `10px` : `0`)};
`;

const BackButton = styled.div<{ pullBackButton?: boolean }>`
  margin-left: ${({ pullBackButton }) => (pullBackButton ? `-40px` : `0`)};
`;
