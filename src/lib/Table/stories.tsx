import React, { useState } from 'react';
import styled from 'styled-components';
import { storiesOf } from '@storybook/react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHistory } from '@fortawesome/pro-regular-svg-icons';

import Table from './index';

storiesOf('Components/Table', module)
  .add('Table', () => {
    const structure = {
      header: {
        type: 'columnTitle',
        titles: [{ text: 'Column Title' }, { text: 'Column Title' }, { text: 'Column Title' }],
      },
      rows: [
        {
          cells: [
            {
              content: 'Content 1',
              cellProps: '',
            },
            {
              content: 'Content 2',
              cellProps: '',
            },
            {
              content: 'Content 3',
              cellProps: '',
            },
          ],
          collapsableRows: [],
        },
        {
          cells: [
            {
              content: 'Content 1',
              cellProps: '',
            },
            {
              content: 'Content 2',
              cellProps: '',
            },
            {
              content: 'Content 3',
              cellProps: '',
            },
          ],
          collapsableRows: [],
        },
      ],
    };

    return <Table structure={structure} />;
  })
  .add('Table with title', () => {
    const structure = {
      header: {
        type: 'tableTitle',
        title: 'Table Title',
        icon: <FontAwesomeIcon icon={faHistory} />,
      },
      rows: [
        {
          cells: [
            {
              content: 'Content 1',
              cellProps: '',
            },
            {
              content: 'Content 2',
              cellProps: '',
            },
            {
              content: 'Content 3',
              cellProps: '',
            },
          ],
          collapsableRows: [],
        },
        {
          cells: [
            {
              content: 'Content 1',
              cellProps: '',
            },
            {
              content: 'Content 2',
              cellProps: '',
            },
            {
              content: 'Content 3',
              cellProps: '',
            },
          ],
          collapsableRows: [],
        },
      ],
    };

    return <Table structure={structure} />;
  })
  .add('Table with collapsable content', () => {
    const structure = {
      header: {
        type: 'columnTitle',
        titles: [{ text: 'Column Title' }, { text: 'Column Title' }, { text: 'Column Title' }],
      },
      rows: [
        {
          cells: [
            {
              content: 'Content 1',
              cellProps: '',
            },
            {
              content: 'Content 2',
              cellProps: '',
            },
            {
              content: 'Content 3',
              cellProps: { colSpan: 2 },
            },
          ],
          collapsableRows: [],
        },
        {
          cells: [
            {
              content: 'Content 4',
              cellProps: '',
            },
            {
              content: 'Content 5',
              cellProps: '',
            },
            {
              content: 'Content 6',
              cellProps: '',
            },
          ],
          collapsableRows: [
            {
              cells: [
                {
                  content: 'Collapsed Content 1',
                  cellProps: '',
                },
                {
                  content: 'Collapsed Content 2',
                  cellProps: '',
                },
                {
                  content: 'Collapsed Content 3',
                  cellProps: { colSpan: 2 },
                },
              ],
            },
            {
              cells: [
                {
                  content: 'Collapsed Content 1',
                  cellProps: '',
                },
                {
                  content: 'Collapsed Content 2',
                  cellProps: '',
                },
                {
                  content: 'Collapsed Content 3',
                  cellProps: { colSpan: 2 },
                },
              ],
            },
          ],
        },
        {
          cells: [
            {
              content: 'Content 1',
              cellProps: '',
            },
            {
              content: 'Content 2',
              cellProps: '',
            },
            {
              content: 'Content 3',
              cellProps: { colSpan: 2 },
            },
          ],
          collapsableRows: [],
        },
        {
          cells: [
            {
              content: 'Content 4',
              cellProps: '',
            },
            {
              content: 'Content 5',
              cellProps: '',
            },
            {
              content: 'Content 6',
              cellProps: '',
            },
          ],
          collapsableRows: [
            {
              cells: [
                {
                  content: 'Collapsed Content 1',
                  cellProps: '',
                },
                {
                  content: 'Collapsed Content 2',
                  cellProps: '',
                },
                {
                  content: 'Collapsed Content 3',
                  cellProps: { colSpan: 2 },
                },
              ],
            },
            {
              cells: [
                {
                  content: 'Collapsed Content 1',
                  cellProps: '',
                },
                {
                  content: 'Collapsed Content 2',
                  cellProps: '',
                },
                {
                  content: 'Collapsed Content 3',
                  cellProps: { colSpan: 2 },
                },
              ],
            },
          ],
        },
      ],
    };

    return <Table structure={structure} />;
  });
