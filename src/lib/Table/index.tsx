/**
 * Table component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React, { useState, useRef, useEffect, useMemo } from 'react';
import styled from 'styled-components';

import { Table as SemanticTable } from 'semantic-ui-react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faChevronUp } from '@fortawesome/pro-regular-svg-icons';

import TablePagination from '../TablePagination';
import Checkbox from '../Checkbox';
import Text from '../Text';
import theme from '../theme';

import { ITable } from '../types';

import AnimatedBackground from '../AnimatedBackground';

const Table: React.FC<ITable> = ({
  disabledAccordion,
  structure,
  selectable,
  selectedInfoTitle,
  selectedInfoSubTitle,
  selectedInfoAction,
  onChangeSelected,
  defaultSelectedRows,
  resetSelectedIds,
  setResetSelectedIdsFunction,
  useValueAsId,
  pointer,
  collapsedColor,
  navigatable,
  stackingFrom,
  pagination,
  translations,
  onChangeRows,
  onChangePage,
  loading,
  headerColor,
  noTopBorder,
  name,
  checkboxTable,
  headerCellPadding,
  noAutoEmptyCells,
  lockCollapsableInPlace,
  ...props
}) => {
  const [openRows, setOpenRows] = useState([] as number[]);
  const didSetDefault = useRef(false);

  const [selectedRows, setSelectedRows] = useState(
    defaultSelectedRows
      ? defaultSelectedRows
      : ([] as { id?: number; value: any; disabled?: boolean }[])
  );

  const [initialCollapsableLocked, setInitialCollapsableLocked] = useState(false);

  const currentOffset = pagination?.offset || 0;
  const currentLimit = pagination?.limit || null;

  const setColNumbers = () =>
    structure.rows.reduce((acc, eachRow) => {
      if (eachRow?.cells.length > 0 && selectable) {
        return eachRow.cells.length;
      }
      return acc;
    }, 0);

  const colNumbers = useMemo(setColNumbers, [structure]);

  const handleOnCollapse = (id) => {
    if (!openRows.includes(id)) {
      setOpenRows([...openRows, id]);
    } else {
      setOpenRows(openRows.filter((r) => r !== id));
    }
  };

  // If the structure or current page offset are changed,
  // update rebuild the selected rows indeces
  useEffect(() => {
    fillSelectedRows(selectedRows);
  }, [currentOffset, structure]);

  const fillSelectedRows = (resetSelectedRows) => {
    const rowsToOpen = [] as number[];
    const rowsToSelect = [] as { id?: number; value: any }[];

    structure.rows.forEach((eachRow, i) => {
      if (eachRow.collapsableRows && eachRow.collapsableRows.length && eachRow.collapsableOpen) {
        if (useValueAsId) {
          selectedRows.forEach((item) => {
            if (item.value[useValueAsId] === eachRow.value[useValueAsId]) {
              if (item.id !== null && item.id !== undefined) {
                rowsToOpen.push(item.id);
              }
            }
          });
        } else {
          rowsToOpen.push(i + 1);
        }
      }

      if (useValueAsId) {
        // generate row ids for items that have undefined row ids
        const index = resetSelectedRows.findIndex(
          (item) => item.value[useValueAsId] === eachRow.value[useValueAsId]
        );

        if (index > -1) {
          rowsToSelect.push({
            id:
              resetSelectedRows[index].id !== undefined && resetSelectedRows[index].id !== null
                ? resetSelectedRows[index].id
                : i + currentOffset,
            value: eachRow.value,
          });
        }
      }
    });

    // add the rest of the selectedRows rows that do not appear in the current page
    if (useValueAsId) {
      resetSelectedRows.forEach((selRow) => {
        const index = rowsToSelect.findIndex(
          (item) => item.value[useValueAsId] === selRow.value[useValueAsId]
        );

        if (index === -1) {
          rowsToSelect.push(selRow);
        }
      });
    }

    if (!!structure.rows.length) {
      if (rowsToSelect.length > 0) {
        setSelectedRows(rowsToSelect);
      }
      if (!didSetDefault.current) {
        didSetDefault.current = true;
      }

      // prevent the use of lockCollapsableInPlace with selectable tables
      if (lockCollapsableInPlace && !selectable) {
        if (!initialCollapsableLocked) {
          setOpenRows(rowsToOpen);
          setInitialCollapsableLocked(true);
        }
      } else {
        setOpenRows(rowsToOpen);
      }
    }
  };

  useEffect(() => {
    if (resetSelectedIds && structure.rows.length > 0) {
      const resetSelectedRows = selectedRows.map((row) => {
        return { value: row.value };
      });

      fillSelectedRows(resetSelectedRows);

      if (setResetSelectedIdsFunction !== undefined) {
        setResetSelectedIdsFunction();
      }
    }
  }, [structure]);

  useEffect(() => {
    onChangeSelected &&
      onChangeSelected(
        selectedRows.map((selected) => selected.value),
        structure.rows
      );
  }, [selectedRows]);

  const checkAllSelectedItems = () => {
    let currentOffsetSelected = selectedRows;

    if (currentLimit) {
      // filter out all items hat do not have ids yet
      currentOffsetSelected = selectedRows.filter((selectedRow) => {
        if (selectedRow.id !== null && selectedRow.id !== undefined) {
          return selectedRow.id < currentOffset + currentLimit && selectedRow.id >= currentOffset;
        } else {
          // if id is undefined, it is a signal that the current row is not on the current page
          // and that it hasn't been shown yet
          return false;
        }
      });
    }

    if (currentOffsetSelected.length <= 0) {
      return false;
    }

    let rowsWithValues = 0;

    structure.rows.forEach((eachRow) => {
      if (eachRow.value !== null && eachRow.value !== undefined) {
        rowsWithValues += 1;
      }
    });

    return currentOffsetSelected.length === rowsWithValues && currentOffsetSelected.length > 0;
  };

  const checkOpenRowsIncludesItem = (row) => {
    if (!useValueAsId) {
      return false;
    }

    let result = false;

    // from the passed row, get the row id using the selectedRows list
    const selected = selectedRows.find(
      (item) => item.value[useValueAsId] === row.value[useValueAsId]
    );

    if (selected !== undefined) {
      // iterate through openRows and check if there the 'selected' row id is a element of openRows list
      openRows.forEach((openRowValue) => {
        if (openRowValue === selected.id) {
          result = true;
        }
      });
    }

    return result;
  };

  const allSelectedItems = checkAllSelectedItems();

  return (
    <Wrapper
      stackingFrom={stackingFrom}
      noTopBorder={noTopBorder}
      headerCellPadding={headerCellPadding}
    >
      <StyledTable {...props}>
        <Header>
          <Row>
            {selectable && (
              <HeaderCell headerColor={headerColor}>
                <HeaderWrapper>
                  <Checkbox
                    checked={allSelectedItems}
                    onChange={() => {
                      let cloneSelectedRows = [...selectedRows];

                      structure.rows.forEach((eachRow, index) => {
                        if (
                          eachRow.value !== null &&
                          eachRow.value !== undefined &&
                          !eachRow.disabled
                        ) {
                          if (!allSelectedItems) {
                            if (
                              !cloneSelectedRows.some(
                                (selectedRow) => selectedRow.id === index + currentOffset
                              )
                            ) {
                              cloneSelectedRows.push({
                                id: index + currentOffset,
                                value: eachRow.value,
                              });
                            }
                          } else {
                            const indexToRemove = cloneSelectedRows.findIndex(
                              (row) => row.id === index + currentOffset
                            );

                            if (indexToRemove > -1) {
                              cloneSelectedRows = [
                                ...cloneSelectedRows.slice(0, indexToRemove),
                                ...cloneSelectedRows.slice(indexToRemove + 1),
                              ];
                            }
                          }
                        }
                      });

                      setSelectedRows(cloneSelectedRows);
                    }}
                  />
                </HeaderWrapper>
              </HeaderCell>
            )}
            {structure.header.type === 'tableTitle' && (
              <HeaderCell colSpan="100%" headerColor={headerColor}>
                <HeaderWrapper>
                  <HeaderIcon>{structure.header.icon}</HeaderIcon>
                  <Text size={'article'} color={theme.colors.grey}>
                    {structure.header.title}
                  </Text>
                  {structure.header.label && (
                    <HeaderLabel labelOffset={structure.header.labelOffset || null}>
                      {structure.header.label}
                    </HeaderLabel>
                  )}
                </HeaderWrapper>
              </HeaderCell>
            )}
            {structure.header.type === 'columnTitle' &&
              structure.header.titles &&
              structure.header.titles.map(
                (title: { text: string; style?: { [key: string]: string } }, i: number) => {
                  const hasCollapsables = structure.rows.some(
                    (element) => element.collapsableRows && element.collapsableRows.length
                  );
                  const headerTitlesLength =
                    structure.header.titles && structure.header.titles.length;
                  // span last cell if there's collapsableChildren elements
                  // (since collasable icon adds an extra cells, th needs to colspan)
                  if (headerTitlesLength === i + 1 && hasCollapsables) {
                    return (
                      <HeaderCell
                        key={`title-${i}`}
                        colSpan={noAutoEmptyCells ? 1 : 2}
                        textAlign={'center'}
                        headerColor={headerColor}
                        style={title.style}
                      >
                        <Text size={'article'} color={theme.colors.grey} style={title.style}>
                          {title.text}
                        </Text>
                      </HeaderCell>
                    );
                  } else {
                    return (
                      <HeaderCell key={`title-${i}`} headerColor={headerColor} style={title.style}>
                        <Text size={'article'} color={theme.colors.grey} style={title.style}>
                          {title.text}
                        </Text>
                      </HeaderCell>
                    );
                  }
                }
              )}
          </Row>
        </Header>

        {loading && !pagination && (
          <Body>
            <React.Fragment key={`row-loading`}>
              <Row>
                <Cell colSpan="100%">
                  <AnimatedBackground height={'20px'}></AnimatedBackground>
                </Cell>
              </Row>
            </React.Fragment>
          </Body>
        )}

        {loading && pagination && (
          <Body>
            {[...Array(pagination.limit)].map((x, i) => (
              <React.Fragment key={`row-loading-${i}`}>
                <Row>
                  <Cell colSpan="100%">
                    <AnimatedBackground height={'20px'}></AnimatedBackground>
                  </Cell>
                </Row>
              </React.Fragment>
            ))}
          </Body>
        )}

        {!loading && (
          <Body>
            {!!structure.rows.length &&
              structure.rows.map((eachRow, index) => {
                return (
                  <React.Fragment key={`row-${index}`}>
                    <Row
                      onClick={(event) => {
                        // check if a button within a row was not clicked
                        // if so, then open/close the current row
                        if (event.target.closest('.rowBtn') === null) {
                          if (eachRow.collapsableRows && !!eachRow.collapsableRows.length) {
                            !disabledAccordion && handleOnCollapse(index + 1);
                          }
                        }
                      }}
                      collapsableChildren={
                        eachRow.collapsableRows && !!eachRow.collapsableRows.length
                      }
                      childrenCollapsed={
                        useValueAsId
                          ? checkOpenRowsIncludesItem(eachRow)
                          : openRows.includes(index + 1)
                      }
                      navigatable={eachRow?.rowProps?.cursor}
                      bgColor={eachRow?.rowProps?.color}
                      {...eachRow.rowProps}
                    >
                      {selectable && eachRow.value !== null && eachRow.value !== undefined && (
                        <Cell>
                          <Checkbox
                            disabled={eachRow.disabled}
                            checked={selectedRows.some((row) => row.id === index + currentOffset)}
                            onChange={() => {
                              const indexToRemove = selectedRows.findIndex(
                                (row) => row.id === index + currentOffset
                              );

                              if (indexToRemove > -1) {
                                setSelectedRows([
                                  ...selectedRows.slice(0, indexToRemove),
                                  ...selectedRows.slice(indexToRemove + 1),
                                ]);
                              } else {
                                setSelectedRows([
                                  ...selectedRows,
                                  { id: index + currentOffset, value: eachRow.value },
                                ]);
                              }
                            }}
                          />
                        </Cell>
                      )}

                      {selectable &&
                        (eachRow.value === null || eachRow.value === undefined) &&
                        !eachRow.removeCheckbox && <Cell />}

                      {eachRow.cells.map((eachCell, i) => {
                        return (
                          <Cell
                            key={`row-${index}-cell-${i}`}
                            navigatable={
                              eachRow?.rowProps?.cursor ? eachRow?.rowProps?.cursor : navigatable
                            }
                            {...eachCell.cellProps}
                          >
                            {eachCell.content}
                          </Cell>
                        );
                      })}

                      {!disabledAccordion &&
                        eachRow.collapsableRows &&
                        !!eachRow.collapsableRows.length && (
                          <Cell
                            colSpan="1"
                            navigatable={
                              eachRow?.rowProps?.cursor ? eachRow?.rowProps?.cursor : navigatable
                            }
                            style={{ textAlign: 'center' }}
                          >
                            <FontAwesomeIcon
                              icon={
                                (
                                  useValueAsId
                                    ? checkOpenRowsIncludesItem(eachRow)
                                    : openRows.includes(index + 1)
                                )
                                  ? faChevronUp
                                  : faChevronDown
                              }
                            />
                          </Cell>
                        )}

                      {!disabledAccordion &&
                        !eachRow.collapsableRows &&
                        structure.rows.some((otherRow) => otherRow.collapsableRows) &&
                        !noAutoEmptyCells && <Cell />}
                    </Row>

                    {eachRow.collapsableRows &&
                      (useValueAsId
                        ? checkOpenRowsIncludesItem(eachRow)
                        : openRows.includes(index + 1)) &&
                      eachRow.collapsableRows.map((eachCollapsedRow, index) => {
                        return (
                          <Row
                            collapsedRows={true}
                            key={`row-${index}`}
                            bgColor={eachCollapsedRow?.rowProps?.color}
                            navigatable={eachCollapsedRow?.rowProps?.cursor}
                            leftBorder={eachRow?.rowProps?.leftBorder}
                            {...eachCollapsedRow.rowProps}
                          >
                            {eachCollapsedRow.cells.map((eachCell, i) => {
                              return (
                                <Cell
                                  key={`row-${index}-cell-${i}`}
                                  bgColor={collapsedColor}
                                  navigatable={
                                    eachRow?.rowProps?.cursor
                                      ? eachRow?.rowProps?.cursor
                                      : navigatable
                                  }
                                  {...eachCell.cellProps}
                                >
                                  {eachCell.content}
                                </Cell>
                              );
                            })}
                          </Row>
                        );
                      })}
                  </React.Fragment>
                );
              })}

            {!!structure.rows.length && selectedRows.length > 1 && selectable && (
              <Row bgColor={theme.colors.primary} disableHover>
                <Cell colSpan={selectedInfoAction ? colNumbers : colNumbers + 1} singleLine={false}>
                  <ContentWrapper>
                    <div>{selectedInfoTitle || 'Seleção de múltiplos itens'}</div>
                    <div>
                      {selectedInfoSubTitle
                        ? selectedInfoSubTitle(selectedRows.length)
                        : `${selectedRows.length} itens selecionados`}
                    </div>
                  </ContentWrapper>
                </Cell>

                {selectedInfoAction && (
                  <Cell colSpan="1" singleLine={false}>
                    {selectedInfoAction}
                  </Cell>
                )}
              </Row>
            )}

            {!structure.rows.length && (
              <Row>
                <Cell colSpan="100%">
                  <Text size="article" color={theme.colors.grey}>
                    {translations && translations('generic.tableNoResults', { textOnly: true })}
                  </Text>
                </Cell>
              </Row>
            )}
          </Body>
        )}

        {pagination && !!pagination.total && (
          <Footer>
            <Row>
              <Cell colSpan="100%" pagination="true">
                <TablePagination
                  translations={translations}
                  onChangePage={onChangePage}
                  onChangeRows={onChangeRows}
                  {...pagination}
                />
              </Cell>
            </Row>
          </Footer>
        )}
      </StyledTable>
    </Wrapper>
  );
};

// Those weird styples inside the media query
// are there to force the table to stack earlier (in pixels)
// The library is stacking only from 768px and we want it to be from 992px
const Wrapper = styled.div<{
  stackingFrom?: number;
  noTopBorder?: boolean;
  headerCellPadding?: string;
}>`
  && {
    position: relative;

    table {
      border-top: ${({ theme, noTopBorder }) =>
        (noTopBorder ? '0px solid ' : '3px solid ') + theme.colors.primary};
      border-radius: 0;
    }

    th {
      padding: ${({ headerCellPadding }) =>
        headerCellPadding ? headerCellPadding : '20px 22px'} !important;
      color: ${({ theme }) => theme.colors.grey} !important;
      border-radius: 0 !important;
      font-size: 15px;
      font-weight: ${({ theme }) => theme.weight.regular} !important;
    }

    td {
      padding: ${({ headerCellPadding }) =>
        headerCellPadding ? headerCellPadding : '20px 22px'} !important;
    }

    ${({ stackingFrom }) =>
      stackingFrom &&
      `
        @media only screen and (max-width: ${stackingFrom}px) {
          .ui.table:not(.unstackable) table {
            padding: 0; !important;
            width: 100% !important;
          }
          .ui.table:not(.unstackable) thead {
            display: block !important;
          }
          .ui.table:not(.unstackable) tr {
            padding-top: 1em !important;
            padding-bottom: 1em !important;
            box-shadow: 0 -1px 0 0 rgba(0, 0, 0, 0.1) inset !important;
            width: auto !important;
            display: block !important;
          }
          .ui.table:not(.unstackable) tr > td, tr > th{
            background: 0 0 !important;
            border: none !important;
            padding: 0.25em 0.75em !important;
            box-shadow: none !important;
            width: auto !important;
            display: block !important;
          }
        }
      `}
  }
`;

const StyledTable = styled(SemanticTable)``;

const Header = styled(SemanticTable.Header)``;

const HeaderCell = styled(({ headerColor, ...rest }) => <SemanticTable.HeaderCell {...rest} />)<{
  headerColor: string;
}>`
  background-color: ${({ theme, headerColor }) =>
    headerColor ? headerColor : theme.colors.softGrey} !important;
`;

const HeaderWrapper = styled.div`
  display: flex;
  color: ${({ theme }) => theme.colors.plusDarkGrey};
  align-items: center;
`;

const HeaderIcon = styled.div`
  margin-right: 12px;
  font-size: 20px;
`;

const HeaderLabel = styled.div<{ labelOffset?: number | null }>`
  display: flex;
  justify-content: flex-end;
  flex: 1;
  margin-right: ${({ labelOffset }) => (labelOffset ? `${labelOffset}px` : `0`)};
`;

const Body = styled(SemanticTable.Body)``;

const Footer = styled(SemanticTable.Footer)``;

const Row = styled(
  ({
    openRows,
    childrenCollapsed,
    collapsableChildren,
    highlighted,
    bgColor,
    collapsedRows,
    navigatable,
    disableHover,
    leftBorder,
    ...rest
  }) => <SemanticTable.Row {...rest} />
)<{
  collapsedRows: boolean;
  childrenCollapsed: boolean;
  collapsableChildren: boolean;
  bgColor?: string;
  navigatable?: boolean;
  disableHover?: boolean;
  leftBorder?: boolean;
}>`
  &&& {
    td {
      background-color: ${({ theme, childrenCollapsed, bgColor, collapsedRows }) => {
        if (bgColor) {
          return `${bgColor}`;
        } else if (collapsedRows) {
          return theme.colors.softGrey;
        }
      }};
    }

    :hover {
      td {
        background-color: ${({ theme, collapsedRows, disableHover }) =>
          disableHover
            ? ''
            : collapsedRows
            ? theme.colors.softLightGrey
            : theme.colors.softGrey} !important;
      }
    }

    ${({ theme, highlighted, collapsedRows, leftBorder }) =>
      highlighted &&
      `
      td {
        border-top: ${collapsedRows ? `0` : `1px`} solid ${theme.colors.primary} !important;
        border-bottom: 1px solid ${theme.colors.primary} !important;
      }
  
  
      td:first-child {
        border-left: ${({ leftBorder }) =>
          leftBorder ? `1px solid ${theme.colors.primary}` : `none`};
      }
  
      td:last-child {
        border-right: 1px solid ${theme.colors.primary} !important;
      }
    `}

    td:first-child {
      border-left: ${({ theme, collapsedRows, leftBorder }) =>
        collapsedRows && leftBorder ? `2px solid ${theme.colors.primary}` : `none`};
    }
  }

  cursor: ${({ navigatable }) => (navigatable ? 'pointer' : 'default')};
`;

const Cell = styled(({ color, weight, navigatable, bgColor, ...rest }) => (
  <SemanticTable.Cell {...rest} />
))<{
  color: string;
  weight: string;
  navigatable: boolean;
  bgColor: string;
}>`
  background-color: ${({ theme, bgColor }) => (bgColor ? bgColor : theme.colors.white)};
  color: ${({ theme, color }) => (color ? color : theme.colors.plusDarkGrey)};
  font-weight: ${({ theme, weight }) => (weight ? weight : theme.weight.regular)};
  font-size: 15px;

  ${({ pagination }) =>
    pagination &&
    `
      &&& {
        border-top: 1px solid rgba(34,36,38,.1);
        padding-top: 0px !important;
        padding-bottom: 0px !important;
        background: #f8f8f8;
      }
      `}
`;

const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export default Table;
