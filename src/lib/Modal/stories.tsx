import React from 'react';
import { storiesOf } from '@storybook/react';

import Modal from './index';

storiesOf('Components/Modal', module).add('Confirmation Modal', () => (
  <Modal
    type={'confirmation'}
    header={'Deseja adicionar horário?'}
    border
    cancelButtonText={'cancelar'}
    submitButtonText={'adicionar'}
  />
));
