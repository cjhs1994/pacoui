/**
 * Modal component
 *
 * @author Rafael Guedes <rguedes@ubihwere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import { Modal as SemanticModal } from 'semantic-ui-react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/pro-regular-svg-icons';

import Button from '../Button';
import ResponsiveLayout from '../ResponsiveLayout';
import theme from '../theme';

interface IModal {
  type?: 'simple' | 'content' | 'confirmation' | 'danger';
  width?: string;
  header?: string | JSX.Element;
  children?: JSX.Element;
  message?: string;
  border?: boolean;
  cancelButtonText?: string;
  submitButtonText?: string;
  onClose?: (any) => void;
  onCancel?: (any) => void;
  onSubmit?: (any) => void;
  open?: boolean;
  callBackData?: any;
}

const Modal: React.FC<IModal> = ({
  type,
  header,
  open,
  children,
  message,
  border,
  cancelButtonText,
  submitButtonText,
  onCancel,
  onClose,
  callBackData,
  onSubmit,
  ...rest
}) => {
  const closeIcon = (callBackData) =>
    type && (
      <CloseIconWrapper type={type}>
        <Button
          borderless
          onClick={(e) => {
            onClose && onClose(callBackData);
          }}
          color={'#C2C2C2'}
        >
          <FontAwesomeIcon size="lg" icon={faTimes} />
        </Button>
      </CloseIconWrapper>
    );

  const setModalContent = () => {
    if (type === 'simple') {
      return (
        <>
          {onClose && !cancelButtonText && closeIcon(callBackData)}
          {children}
          {(cancelButtonText || submitButtonText) && (
            <StyledModalActions>
              {cancelButtonText && (
                <Button
                  decision
                  onClick={() => {
                    onCancel && onCancel(callBackData);
                  }}
                >
                  {cancelButtonText}
                </Button>
              )}

              {submitButtonText && (
                <Button
                  success
                  onClick={() => {
                    onSubmit && onSubmit(callBackData);
                  }}
                >
                  {submitButtonText}
                </Button>
              )}
            </StyledModalActions>
          )}
        </>
      );
    } else {
      return (
        <>
          <StyledModalHeader content={header} headerType={typeof header} message={message} />
          {onClose && !cancelButtonText && closeIcon(callBackData)}
          {message && <StyledModalContent>{message}</StyledModalContent>}
          {children && <StyledModalContent>{children}</StyledModalContent>}
          {(submitButtonText || cancelButtonText) && (
            <StyledModalActions>
              {cancelButtonText && (
                <Button
                  decision
                  onClick={() => {
                    onCancel && onCancel(callBackData);
                  }}
                >
                  {cancelButtonText}
                </Button>
              )}

              {submitButtonText && type === 'danger' && (
                <Button
                  danger
                  onClick={() => {
                    onSubmit && onSubmit(callBackData);
                  }}
                >
                  {submitButtonText}
                </Button>
              )}

              {submitButtonText && (type === 'confirmation' || type === 'content') && (
                <Button
                  success
                  onClick={() => {
                    onSubmit && onSubmit(callBackData);
                  }}
                >
                  {submitButtonText}
                </Button>
              )}
            </StyledModalActions>
          )}
        </>
      );
    }
  };

  const ModalContent = <ModalWrapper type={type}>{setModalContent()}</ModalWrapper>;

  return (
    <ResponsiveLayout
      contextualProps={{
        web: {
          size: (type === 'confirmation' && !children) || type === 'danger' ? 'small' : 'large',
          screen: 'web',
        },
        mobile: {
          size: 'fullscreen',
          screen: 'mobile',
          body: children ? 1 : 0,
        },
        tablet: {
          size:
            (type === 'confirmation' && !children) || type === 'danger' ? 'small' : 'fullscreen',
          screen: 'tablet',
        },
      }}
      open={open}
      children={ModalContent}
      border={border ? 1 : 0}
      type={type}
      web={StyledModal}
      mobile={StyledModal}
      tablet={StyledModal}
      {...rest}
    />
  );
};

export default Modal;

Modal.defaultProps = {
  callBackData: null,
};

const StyledModal = styled(SemanticModal)<{
  type?: 'simple' | 'content' | 'confirmation' | 'danger';
  body?: boolean;
  screen: 'mobile' | 'tablet' | 'web';
  width?: string;
  border: boolean;
}>`
  &&& { 
    max-height: calc(90vh);
    overflow-y: auto;
    overflow-x: hidden;
    border-top: ${({ theme, type }) =>
      type === 'simple' ? '4px solid ' + theme.colors.primary : ''};
    position: relative;
    border-radius: 0;
    box-shadow: 0px 3px 6px #00000029;
    ${({ theme, type, border }) =>
      border &&
      `border: 1px solid ${
        type === 'danger' ? theme.colors.dangerRed : theme.colors.successGreen
      };`}
    }
  }
`;

const ModalWrapper = styled.div<{
  type?: 'simple' | 'content' | 'confirmation' | 'danger';
}>`
  padding: ${({ type }) => (type === 'content' ? `98px 32px 52px 32px` : `30px 32px 22px 32px`)};
`;

const StyledModalHeader = styled(({ headerType, ...rest }) => <SemanticModal.Header {...rest} />)<{
  headerType?: string;
}>`
  &&& {
    padding: 0;
    background-color: ${({ theme }) => theme.colors.white};
    border: 0;
    font-family: Roboto;

    ${({ headerType }) =>
      headerType === 'string' &&
      `
      font-size: 16px !important;
      font-weight: 400 !important;
    `}

    ${({ message }) => !message && `margin-bottom: 26px;`}
  }
`;

const StyledModalContent = styled(SemanticModal.Content)`
  &&& {
    width: auto;
    padding: 0;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;

    p {
      margin-bottom: 0;
    }
  }
`;

const StyledModalActions = styled(SemanticModal.Actions)`
  &&& {
    display: flex;
    justify-content: flex-end;
    padding: 0;
    background-color: ${({ theme }) => theme.colors.white};
    border: 0;

    button:first-child {
      margin-right: 19px;
    }
  }
`;

const CloseIconWrapper = styled.div<{ type: 'simple' | 'content' | 'confirmation' | 'danger' }>`
  position: absolute;
  top: ${({ type }) => (type === 'content' ? `40px` : `22px`)};
  right: ${({ type }) => (type === 'content' ? `36px` : `24px`)};
`;
