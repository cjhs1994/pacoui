/**
 * DropdownSelector component
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */

import React, { useState, useEffect, useRef, useContext } from 'react';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons';
import { faTimes, faExclamationCircle } from '@fortawesome/pro-regular-svg-icons';

import Text from '../Text';
import SearchBox from '../SearchBox';

import theme from '../theme';

export interface Iitem {
  id: number;
  title: string;
  selected: boolean | null;
  value?: any;
  disabled?: boolean;
  info?: string;
}

interface IProps {
  title: string;
  dropdownOpenProp?: boolean;
  open?: boolean;
  onOpen?: Function;
  tip: string;
  search?: boolean;
  onSearch?: (value: string) => void;
  multiOption?: boolean;
  onSelect?: any;
  onRemove?: any;
  options: {
    title: string;
    items: Iitem[];
  }[];
}

const DropdownSelector: React.FC<IProps> = ({
  title,
  tip,
  search,
  onSearch,
  open: dropdownOpenProp,
  multiOption,
  options,
  onOpen,
  onSelect,
  onRemove,
}) => {
  const [open, setOpen] = useState<number | null>(null);
  const [dropdownOpen, setDropdownOpen] = useState<boolean>(dropdownOpenProp || false);
  const wrapperRef = useRef(null);

  useEffect(() => {
    dropdownOpenProp !== undefined && setDropdownOpen(dropdownOpenProp);
  }, [setDropdownOpen, dropdownOpenProp]);

  const useOutsideAlerter = (ref: React.RefObject<HTMLDivElement> | null) => {
    useEffect(() => {
      const handleClickOutside = (event: MouseEvent) => {
        if (ref && ref.current && !ref.current.contains(event.target as Node)) {
          setDropdownOpen(false);
        }
      };

      // Bind the event listener
      document.addEventListener('mousedown', handleClickOutside);
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener('mousedown', handleClickOutside);
      };
    }, [ref]);
  };

  useOutsideAlerter(wrapperRef);

  const getSelectedOptions = (option) => {
    return option.items.some((item) => item.selected);
  };

  const canSelect = (option) => multiOption || (!multiOption && !getSelectedOptions(option));

  const getSelectedItems = (option) => {
    let items: any[] = [];

    option.items.forEach((item, key) => {
      if (item.selected) {
        items.push({ ...item, itemIndex: key });
      }
    });

    return items;
  };

  const getUnselectedItems = (option) => {
    let items: any[] = [];

    option.items.forEach((item, key) => {
      if (!item.selected) {
        items.push({ ...item, itemIndex: key });
      }
    });

    return items;
  };

  return (
    <DropdownWrapper ref={dropdownOpen ? wrapperRef : null}>
      <DropdownTitle
        dropdownOpen={dropdownOpen}
        onClick={() => {
          onOpen && onOpen();
          setDropdownOpen(!dropdownOpen);
        }}
      >
        <Text size="small" color="primary" weight="regular">
          {title}
        </Text>

        <DropdownToggleIcon>
          <FontAwesomeIcon size="lg" icon={!dropdownOpen ? faAngleDown : faAngleUp} />
        </DropdownToggleIcon>
      </DropdownTitle>
      <DropdownContent dropdownOpen={dropdownOpen}>
        {search && (
          <SearchWrapper>
            <SearchBox
              borderColor={theme.colors.grey}
              delay={450}
              onSearch={(value) => {
                onSearch && onSearch(value);
              }}
            />
          </SearchWrapper>
        )}
        <DropdownTip>* {tip}</DropdownTip>

        {options.map((option, optionIndex) => (
          <DropdownItemWrapper key={`DropdownItem_${optionIndex}`}>
            <DropdownItem>
              <DropdownItemTitle
                role={'option'}
                showIcon={canSelect(option)}
                onClick={() =>
                  canSelect(option) && open === optionIndex ? setOpen(null) : setOpen(optionIndex)
                }
              >
                {option.title}
                <FontAwesomeIcon icon={open === optionIndex ? faAngleUp : faAngleDown} />
              </DropdownItemTitle>

              <DropdownSubItemWrapper dropdownItemOpen={open === optionIndex || !multiOption}>
                {getSelectedItems(option).map((item, itemIndex) => (
                  <DropdownSubItemContent
                    key={`DropdownSubItem_selected_${optionIndex}_${itemIndex}`}
                  >
                    <DropdownSubItem role={'option'} selected>
                      {!item.disabled && (
                        <FontAwesomeIcon
                          icon={faTimes}
                          onClick={() => onRemove && onRemove(optionIndex, item.itemIndex, item)}
                        />
                      )}
                      {item.title}
                    </DropdownSubItem>
                    {item.info && (
                      <InfoText>
                        <Text
                          color="grey"
                          size="xSmall"
                          transform="lowercase"
                          icon={<FontAwesomeIcon icon={faExclamationCircle} />}
                        >
                          {item.info}
                        </Text>
                      </InfoText>
                    )}
                  </DropdownSubItemContent>
                ))}

                {open === optionIndex &&
                  (multiOption || (!multiOption && !option.items.some((item) => item.selected))) &&
                  getUnselectedItems(option).map((item, itemIndex) => {
                    return (
                      <DropdownSubItemContent key={`DropdownSubItem_${optionIndex}_${itemIndex}`}>
                        <DropdownSubItem
                          disabled={item.disabled}
                          onClick={() =>
                            canSelect(option) &&
                            !item.disabled &&
                            onSelect(optionIndex, item.itemIndex, item)
                          }
                        >
                          {item.title}
                        </DropdownSubItem>
                        {item.info && (
                          <InfoText>
                            <Text
                              color="grey"
                              size="xSmall"
                              transform="lowercase"
                              icon={<FontAwesomeIcon icon={faExclamationCircle} />}
                            >
                              {item.info}
                            </Text>
                          </InfoText>
                        )}
                      </DropdownSubItemContent>
                    );
                  })}
              </DropdownSubItemWrapper>
            </DropdownItem>
          </DropdownItemWrapper>
        ))}
      </DropdownContent>
    </DropdownWrapper>
  );
};

export default DropdownSelector;

const DropdownTitle = styled.div<{ dropdownOpen: boolean }>`
  display: flex;
  padding: 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  color: ${({ theme }) => theme.colors.primary};
  width: 100%;

  &:hover {
    cursor: pointer;
  }
`;

const DropdownWrapper = styled.div`
  width: 100%;
  position: relative;
  background-color: ${({ theme }) => theme.colors.white};

  ${DropdownTitle} {
    &:hover {
      cursor: pointer;
      background-color: ${({ theme }) => theme.colors.primary};

      div {
        background-color: ${({ theme }) => theme.colors.primary};
        color: ${({ theme }) => theme.colors.white} !important;
      }
    }
  }
`;

const DropdownToggleIcon = styled.div`
  width: 25px;
  margin-left: 10px;
`;

const DropdownContent = styled.div<{ dropdownOpen: boolean }>`
  width: 100%;
  position: absolute;
  display: ${({ dropdownOpen }) => (!dropdownOpen ? 'none' : 'block')};
  margin-top: 6px;
  max-height: 500px;
  overflow-y: auto;
  padding: 12px 16px;
  background-color: ${({ theme }) => theme.colors.white};
  box-shadow: 0px 3px 6px #00000029;
  border: 0.5px solid ${({ theme }) => theme.colors.regularGrey};
  z-index: 1;
`;

const DropdownTip = styled.div`
  margin-bottom: 22px;
  color: ${({ theme }) => theme.colors.darkGrey};
  font-size: 13px;
`;

const DropdownItemWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const DropdownItem = styled.div`
  width: 100%;
  padding: 10px 0;
  color: ${({ theme }) => theme.colors.primary};
  font-size: 14px;
  letter-spacing: 0.14px;
  text-transform: uppercase;

  &:hover {
    cursor: pointer;
  }
`;

const DropdownItemTitle = styled.div<{ showIcon: boolean }>`
  display: flex;
  justify-content: space-between;
  align-items: center;

  ${({ showIcon }) =>
    !showIcon &&
    `
    svg {
      display: none;
    }
  `}
`;

const DropdownSubItemWrapper = styled.div<{ dropdownItemOpen: boolean }>`
  padding: 10px 0;
  display: ${({ dropdownItemOpen }) => (!dropdownItemOpen ? 'none' : 'block')};
`;

const DropdownSubItemContent = styled.div`
  padding: 10px 0;
`;

const DropdownSubItem = styled.div<{ selected?: boolean; disabled?: boolean }>`
  display: flex;
  align-items: center;
  color: ${({ theme }) => theme.colors.dropdown.subItemColor};
  text-transform: none;

  &:hover {
    color: ${({ theme }) => theme.colors.primary};
    font-weight: 600;
  }

  ${({ theme, selected }) =>
    selected &&
    `
  font-weight: 500;

  svg {
    font-size: 17px;
    margin-right: 12px;
  }

  &:hover {
    color: ${theme.colors.dropdown.subItemColor};
    font-weight: 500;
    cursor: default;
  }

  svg:hover {
    cursor: pointer;
  }
`}

  ${({ theme, disabled }) =>
    disabled &&
    `
  font-weight: 300;
  opacity: 0.7;
  
  &:hover {
    color: ${theme.colors.dropdown.subItemColor};
    cursor: not-allowed;
  }
`}
`;

const SearchWrapper = styled.div`
  margin-bottom: 10px;
`;

const InfoText = styled.div`
  margin-top: 4px;
`;
