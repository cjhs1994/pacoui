/**
 * InfoBox component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle, faChevronRight } from '@fortawesome/pro-light-svg-icons';

import Text from '../Text';
import theme from '../theme';

interface IInfoBox {
  title?: string;
  link?: string;
  padding?: string;
  children: JSX.Element | JSX.Element[];
}

const PureInfoBox = ({ title, link, padding, children }) => {
  return (
    <Wrapper clickable={!!link} padding={padding} hasLink={link ? true : false}>
      <IconWrapper>
        <FontAwesomeIcon icon={faInfoCircle} />
      </IconWrapper>
      <InnerWrapper>
        <ContentWrapper>
          {title && (
            <Title>
              <Text size="article" weight={'bold'} color={theme.colors.primary}>
                {title}
              </Text>
            </Title>
          )}
          <ChildrenWrapper>{children}</ChildrenWrapper>
        </ContentWrapper>
        {link && (
          <LinkIcon>
            <FontAwesomeIcon icon={faChevronRight} />
          </LinkIcon>
        )}
      </InnerWrapper>
    </Wrapper>
  );
};

const InfoBox: React.FC<IInfoBox> = ({ title, link, padding, children }) => {
  if (link) {
    return (
      <WrapperAnchor show={!!link} href={link && link} target="_blank">
        <PureInfoBox title={title} link={link} children={children} padding={padding} />
      </WrapperAnchor>
    );
  } else {
    return <PureInfoBox title={title} link={link} children={children} padding={padding} />;
  }
};

export default InfoBox;

const WrapperAnchor = styled.a<{ show: boolean }>`
  display: ${({ show }) => (show ? 'inline' : 'none')};
`;

const Wrapper = styled.div<{ clickable: boolean; padding?: string; hasLink?: boolean }>`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: ${({ padding }) => (padding ? padding : '18px 30px')};
  background-color: ${({ theme }) => theme.colors.softGrey};
  color: ${({ theme }) => theme.colors.darkGrey};
  border: 1px solid ${({ theme }) => theme.colors.lightGrey};
  cursor: ${({ clickable }) => (clickable ? 'pointer' : 'default')};

  :hover {
    background-color: ${({ theme }) => theme.colors.softLightGrey};
  }

  svg {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 32px;
  }
`;

const InnerWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const IconWrapper = styled.div`
  height: 100%;
  margin-right: 32px;
`;

const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-right: 32px;
`;

const Title = styled.div``;

const ChildrenWrapper = styled.div`
  margin-top: 12px;
  overflow-wrap: anywhere;
`;

const LinkIcon = styled.div`
  svg {
    color: ${({ theme }) => theme.colors.plusDarkGrey};
    font-size: 12px;
  }
`;
