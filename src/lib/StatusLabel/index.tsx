/**
 * StatusLabel component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import Tooltip from '../Tooltip';
import theme from '../theme';

interface IStatusLabel {
  background?: string;
  label: string | React.ReactNode;
}

const StatusLabel: React.FC<IStatusLabel> = ({ background, label }) => {
  return (
    <Tooltip
      content={label}
      trigger={
        <Wrapper background={background}>
          <InnerWrapper>{label}</InnerWrapper>
        </Wrapper>
      }
    />
  );
};

export default StatusLabel;

const Wrapper = styled.div<{ background?: string }>`
  height: 22px;
  max-width: 120px;
  width: fit-content;
  padding: 0 8px;
  background-color: ${({ theme, background }) => (background ? background : theme.colors.grey)};
  color: ${({ theme }) => theme.colors.white};
  font-size: 12px;
  border-radius: 13px;
  display: flex;
  justify-content: center;
  text-align: center;
  align-items: center;

  :hover {
    cursor: not-allowed;
  }
`;

const InnerWrapper = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  word-wrap: break-word;
  display: inline-block;

  & div {
    max-width: 100%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    display: inline-block;
  }
`;
