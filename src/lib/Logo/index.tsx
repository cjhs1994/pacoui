/**
 * Logo component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React from 'react';

import LogoBig from './LogoBig';
import LogoSmall from './LogoSmall';

interface IProps {
  width?: string;
  height?: string;
  small?: boolean;
  fill?: string;
}

function Logo({ small, ...props }: IProps) {
  if (small) {
    return <LogoSmall {...props} />;
  } else {
    return <LogoBig {...props} />;
  }
}

export default Logo;
