/**
 * Logo component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React from 'react';

interface IProps {
  width?: string;
  height?: string;
  fill?: string;
}

function LogoBig(props: IProps) {
  return (
    <svg id="prefix__Layer_1" x={0} y={0} viewBox="0 0 186 46" xmlSpace="preserve" {...props}>
      <style>{`.prefix__st0{fill:${props.fill || '#FFF'}}`}</style>
      <g id="prefix__Grupo_1165" transform="translate(-.49 -.409)">
        <path
          id="prefix__Caminho_589"
          className="prefix__st0"
          d="M38.09 4.94c-.09-.01-.17-.02-.26-.01-.51 0-.99.23-1.31.63l-4.46 5.43c-.69.66-1.61 1.03-2.56 1.03-2 .02-3.64-1.57-3.67-3.57a7.091 7.091 0 00-6.64-7.04c-.35 0-.64.28-.64.62V8.44a3.583 3.583 0 01-3.59 3.57h-.02c-.96 0-1.89-.37-2.59-1.03L7.88 5.56c-.32-.4-.8-.63-1.32-.63-.09 0-.17.01-.26.02-2.87.39-5 2.86-4.96 5.77V39.9c.03 3.26 2.7 5.87 5.96 5.84H37.11c3.26.04 5.93-2.57 5.97-5.83V10.72a5.787 5.787 0 00-4.99-5.78zm-1 34.07a.94.94 0 01-.96.92h-7.98c-1.54.01-2.95.86-3.67 2.22-.34.57-.95.92-1.61.92h-.69c-.7-.01-1.35-.4-1.69-1.02a4.05 4.05 0 00-3.58-2.12H8.26a.946.946 0 01-.96-.92l.01-11.39a.92.92 0 01.9-.95h8.7c1.54.01 2.94.86 3.66 2.22.34.57.95.92 1.61.92h.69c.7-.01 1.35-.4 1.69-1.02a4.055 4.055 0 013.59-2.12h8.02c.51 0 .92.4.93.91v.01l-.01 11.42z"
        />
        <path
          id="prefix__Caminho_590"
          className="prefix__st0"
          d="M97.16 32.77c-1.06-.05-2.04.56-2.48 1.52l-.03-1.32h-.78v9.54h.85v-3.9a2.56 2.56 0 002.44 1.53c2.12 0 3.13-1.72 3.13-3.69s-1.01-3.68-3.13-3.68zm0 6.65c-1.81 0-2.44-1.53-2.44-2.97 0-1.58.57-2.97 2.44-2.97 1.62 0 2.28 1.53 2.28 2.97s-.66 2.97-2.28 2.97z"
        />
        <path
          id="prefix__Caminho_591"
          className="prefix__st0"
          d="M104.29 32.77c-2.13 0-3.29 1.69-3.29 3.68s1.16 3.69 3.29 3.69 3.29-1.69 3.29-3.69-1.16-3.68-3.29-3.68zm0 6.65c-1.63 0-2.44-1.49-2.44-2.97s.81-2.97 2.44-2.97 2.44 1.49 2.44 2.97-.81 2.97-2.44 2.97z"
        />
        <path
          id="prefix__Caminho_592"
          className="prefix__st0"
          d="M113.59 32.77c-2.21 0-3.14 1.85-3.14 3.68 0 1.99.93 3.69 3.14 3.69a2.85 2.85 0 003.04-2.5h-.85a2.119 2.119 0 01-2.19 1.78c-1.63 0-2.28-1.5-2.29-2.74h5.36c.07-1.95-.83-3.91-3.07-3.91zm0 .72c1.29.07 2.28 1.17 2.21 2.46v.03h-4.51c0-1.31 1-2.4 2.3-2.49z"
        />
        <path
          id="prefix__Caminho_593"
          className="prefix__st0"
          d="M138.97 32.77c-1.06-.05-2.04.56-2.48 1.52l-.03-1.32h-.78v9.54h.85v-3.9a2.56 2.56 0 002.44 1.53c2.12 0 3.13-1.72 3.13-3.69s-1.01-3.68-3.13-3.68zm0 6.65c-1.81 0-2.44-1.53-2.44-2.97 0-1.58.57-2.97 2.44-2.97 1.62 0 2.28 1.53 2.28 2.97s-.66 2.97-2.28 2.97z"
        />
        <path
          id="prefix__Caminho_594"
          className="prefix__st0"
          d="M153.2 39.17c-.09.03-.18.05-.27.05a.45.45 0 01-.5-.4V35c0-1.86-1.34-2.24-2.56-2.24-1.59 0-2.74.7-2.82 2.33h.85c.03-1.13.81-1.62 1.86-1.62 1 0 1.82.28 1.82 1.43 0 .77-.39.86-1.09.94-1.84.22-3.74.28-3.74 2.24 0 1.4 1.05 2.04 2.34 2.04a2.66 2.66 0 002.54-1.42c.03.73.16 1.22 1.03 1.22.15 0 .3-.01.44-.04l.11-.15-.01-.56zm-4.05.25c-.8.06-1.49-.54-1.55-1.34v-.03c0-1.36 1.79-1.35 3.24-1.6.23-.04.61-.11.72-.31l.03 1.11a2.237 2.237 0 01-2.29 2.19c-.05-.01-.1-.01-.15-.02z"
        />
        <path
          id="prefix__Caminho_595"
          className="prefix__st0"
          d="M88.96 38.71v-3.7c0-1.86-1.34-2.24-2.56-2.24-1.59 0-2.74.7-2.82 2.33h.85c.03-1.13.81-1.62 1.86-1.62 1 0 1.82.28 1.82 1.43 0 .77-.39.86-1.09.94-1.83.22-3.74.28-3.74 2.24 0 1.4 1.05 2.04 2.33 2.04a2.66 2.66 0 002.54-1.42c.03.73.16 1.22 1.03 1.22.19 0 .37-.01.55-.05v-.72c-.09.03-.18.05-.27.05a.45.45 0 01-.5-.4v-.1zm-3.27.71c-.8.06-1.49-.54-1.55-1.34v-.04c0-1.36 1.79-1.35 3.24-1.6.23-.04.61-.11.71-.31l.03 1.11a2.228 2.228 0 01-2.43 2.18z"
        />
        <path
          id="prefix__Caminho_596"
          className="prefix__st0"
          d="M137.14 25.73l.03.94h1.46v-9.64h-1.54v3.56a2.753 2.753 0 00-2.35-1.09c-1.53 0-3.02 1.11-3.02 3.63 0 2.09 1.07 3.73 3.27 3.73.87.06 1.71-.38 2.15-1.13zm-1.95-.08c-1.34 0-1.93-1.19-1.93-2.39 0-1.25.51-2.54 1.96-2.54 1.18 0 1.93.92 1.93 2.46-.01 1.21-.59 2.47-1.96 2.47z"
        />
        <path
          id="prefix__Caminho_597"
          className="prefix__st0"
          d="M146.27 24.49h-1.46c-.22.74-.93 1.23-1.7 1.16a1.861 1.861 0 01-1.88-2.06h5.13c.39-1.82-.78-3.62-2.6-4.01-1.82-.39-3.62.78-4.01 2.6-.07.33-.09.67-.06 1.01 0 2.15 1.23 3.67 3.41 3.67 1.5.09 2.84-.92 3.17-2.37zm-3.2-3.77c.99.04 1.77.86 1.75 1.85h-3.59c-.01-1.01.79-1.83 1.79-1.85h.05z"
        />
        <path
          id="prefix__Caminho_598"
          className="prefix__st0"
          d="M153.05 22.65c-1.29.15-2.68.43-2.68 2.17a2.116 2.116 0 002.19 2.05c.07 0 .13-.01.2-.02.88.02 1.74-.28 2.42-.85.12.64.56.85 1.19.85.33-.02.66-.08.97-.18v-1.06c-.12.02-.25.03-.38.03-.28 0-.36-.15-.36-.53v-3.59c0-1.47-1.43-2.02-2.79-2.02-1.54 0-3.06.53-3.17 2.32h1.54c.07-.76.68-1.11 1.54-1.11.62 0 1.44.15 1.44.94-.02.92-1 .8-2.11 1zm2 1.67c0 .97-1.05 1.32-1.73 1.32-.54 0-1.41-.2-1.41-.89 0-.81.59-1.05 1.25-1.16a5.58 5.58 0 001.89-.42v1.15z"
        />
        <path
          id="prefix__Caminho_599"
          className="prefix__st0"
          d="M167.74 19.51a3.432 3.432 0 00-3.37 3.69c0 2.15 1.23 3.67 3.41 3.67 1.49.09 2.83-.92 3.16-2.38h-1.46c-.21.75-.93 1.23-1.7 1.16a1.861 1.861 0 01-1.88-2.06h5.13c.35-1.87-.88-3.67-2.74-4.02-.19-.04-.37-.06-.55-.06zm-1.84 3.06c-.07-.99.67-1.86 1.66-1.93a1.802 1.802 0 011.93 1.93h-3.59z"
        />
        <path
          id="prefix__Caminho_600"
          className="prefix__st0"
          d="M182.35 19.51a3.367 3.367 0 00-3.52 3.67 3.524 3.524 0 003.37 3.68c1.95.09 3.59-1.42 3.68-3.37v-.31c.17-1.85-1.2-3.49-3.05-3.66-.16-.02-.32-.02-.48-.01zm0 6.14c-1.38 0-1.98-1.25-1.98-2.47s.61-2.46 1.98-2.46 1.98 1.25 1.98 2.46-.61 2.47-1.98 2.47z"
        />
        <path
          id="prefix__Caminho_601"
          className="prefix__st0"
          d="M101.1 26.86c.88.06 1.71-.38 2.16-1.13l.03.94h1.46v-9.63h-1.54v3.56a2.753 2.753 0 00-2.35-1.09c-1.53 0-3.02 1.11-3.02 3.63-.01 2.09 1.05 3.72 3.26 3.72zm.23-6.14c1.19 0 1.93.92 1.93 2.46 0 1.21-.58 2.47-1.96 2.47-1.33 0-1.93-1.19-1.93-2.39 0-1.26.51-2.54 1.96-2.54z"
        />
        <path
          id="prefix__Caminho_602"
          className="prefix__st0"
          d="M111.75 26.86c.33-.02.66-.08.97-.18v-1.06c-.13.02-.25.03-.38.03-.28 0-.36-.15-.36-.53v-3.59c0-1.47-1.43-2.02-2.8-2.02-1.54 0-3.06.53-3.17 2.32h1.54c.07-.76.68-1.11 1.54-1.11.62 0 1.45.15 1.45.94 0 .9-.99.78-2.09.99-1.3.15-2.68.43-2.68 2.17a2.128 2.128 0 002.18 2.06c.07 0 .14-.01.2-.02.88.02 1.74-.28 2.41-.85.13.64.57.85 1.19.85zm-1.3-2.54c0 .97-1.05 1.32-1.73 1.32-.54 0-1.42-.2-1.42-.89 0-.81.6-1.05 1.25-1.16a5.58 5.58 0 001.89-.42l.01 1.15z"
        />
        <path
          id="prefix__Caminho_603"
          className="prefix__st0"
          d="M116.3 26.86c.88.06 1.71-.38 2.16-1.13l.03.94h1.46v-9.63h-1.54v3.56a2.753 2.753 0 00-2.35-1.09c-1.52 0-3.02 1.11-3.02 3.63-.01 2.09 1.06 3.72 3.26 3.72zm.23-6.14c1.19 0 1.93.92 1.93 2.46 0 1.21-.58 2.47-1.96 2.47-1.34 0-1.93-1.19-1.93-2.39 0-1.26.52-2.54 1.96-2.54z"
        />
        <path
          id="prefix__Caminho_604"
          className="prefix__st0"
          d="M124.42 26.86c1.49.09 2.83-.92 3.16-2.38h-1.46c-.22.74-.93 1.23-1.7 1.16a1.861 1.861 0 01-1.88-2.06h5.13c.39-1.82-.78-3.62-2.6-4.01-1.82-.39-3.62.78-4.01 2.6-.07.33-.09.67-.06 1.01.01 2.16 1.23 3.68 3.42 3.68zm-.04-6.14c.99.04 1.77.86 1.75 1.85h-3.59c-.01-1.01.79-1.83 1.8-1.85h.04z"
        />
        <path
          id="prefix__Caminho_605"
          className="prefix__st0"
          d="M56.7 26.67h1.51v-6.98h-1.54v4.05c0 1.08-.43 1.9-1.71 1.9-.84 0-1.34-.44-1.34-1.67V19.7h-1.53v4.43c0 1.82.78 2.74 2.6 2.74.82-.01 1.57-.45 1.98-1.16l.03.96z"
        />
        <path
          id="prefix__Caminho_606"
          className="prefix__st0"
          d="M64.15 22.29v4.39h1.54v-4.79a2.201 2.201 0 00-2.47-2.37c-.89 0-1.72.47-2.17 1.24l-.03-1.06h-1.46v6.98h1.53v-4.12c-.07-.94.62-1.76 1.56-1.83.04 0 .08-.01.13-.01.92 0 1.34.47 1.37 1.57z"
        />
        <path
          id="prefix__Ret\xE2ngulo_701"
          className="prefix__st0"
          d="M67.08 19.69h1.54v6.98h-1.54z"
        />
        <path
          id="prefix__Ret\xE2ngulo_702"
          className="prefix__st0"
          d="M67.08 17.04h1.54v1.46h-1.54z"
        />
        <path
          id="prefix__Caminho_607"
          className="prefix__st0"
          d="M73.59 26.67l2.49-6.98H74.5l-1.7 5.36-1.8-5.36h-1.68l2.54 6.98h1.73z"
        />
        <path
          id="prefix__Caminho_608"
          className="prefix__st0"
          d="M79.78 19.51a3.432 3.432 0 00-3.37 3.69c0 2.15 1.23 3.67 3.42 3.67 1.49.09 2.83-.92 3.16-2.38h-1.46c-.22.74-.93 1.23-1.7 1.16a1.861 1.861 0 01-1.88-2.06h5.13c.35-1.87-.88-3.67-2.75-4.02-.18-.04-.36-.06-.55-.06zm-1.83 3.06c-.03-.99.74-1.82 1.73-1.85s1.82.74 1.85 1.73v.12h-3.58z"
        />
        <path
          id="prefix__Caminho_609"
          className="prefix__st0"
          d="M85.52 23.35c0-1.54.84-2.39 1.89-2.39.22.01.44.03.66.07v-1.49a3.77 3.77 0 00-.58-.04c-.93.04-1.74.65-2.04 1.54l-.03-1.35h-1.45v6.98h1.54v-3.32z"
        />
        <path
          id="prefix__Caminho_610"
          className="prefix__st0"
          d="M92.03 22.56c-1.13-.25-2.27-.36-2.27-1.12 0-.61.82-.72 1.27-.72.68 0 1.28.2 1.42.93h1.61c-.19-1.55-1.49-2.15-2.92-2.15-1.27 0-2.91.47-2.91 2 0 1.42 1.11 1.82 2.24 2.06s2.24.35 2.28 1.16-.97.92-1.55.92c-.82 0-1.5-.32-1.58-1.22h-1.54c.03 1.65 1.33 2.43 3.09 2.43 1.44 0 3.12-.61 3.12-2.27 0-1.36-1.14-1.77-2.26-2.02z"
        />
        <path
          id="prefix__Ret\xE2ngulo_703"
          className="prefix__st0"
          d="M95.23 19.69h1.54v6.98h-1.54z"
        />
        <path
          id="prefix__Ret\xE2ngulo_704"
          className="prefix__st0"
          d="M95.23 17.04h1.54v1.46h-1.54z"
        />
        <path
          id="prefix__Caminho_611"
          className="prefix__st0"
          d="M162.45 19.69l-1.7 5.36-1.79-5.36h-1.68l2.54 6.98h1.73l2.5-6.98h-1.6z"
        />
        <path
          id="prefix__Ret\xE2ngulo_705"
          className="prefix__st0"
          d="M171.97 19.69h1.54v6.98h-1.54z"
        />
        <path
          id="prefix__Ret\xE2ngulo_706"
          className="prefix__st0"
          d="M171.97 17.04h1.54v1.46h-1.54z"
        />
        <path
          id="prefix__Caminho_612"
          className="prefix__st0"
          d="M179 19.55c-.19-.03-.38-.04-.58-.04-.93.04-1.74.65-2.04 1.54l-.03-1.35h-1.44v6.98h1.53v-3.32c0-1.54.84-2.39 1.89-2.39.22.01.44.03.66.07l.01-1.49z"
        />
        <path
          id="prefix__Caminho_613"
          className="prefix__st0"
          d="M54.15 30.88h-.85v2.09h-1.21v.72h1.21v4.76c-.01 1.19.38 1.56 1.51 1.56.26 0 .5-.03.76-.03v-.72c-.24.03-.48.04-.73.04-.61-.04-.69-.36-.69-.92v-4.7h1.42v-.72h-1.42v-2.08z"
        />
        <path
          id="prefix__Caminho_614"
          className="prefix__st0"
          d="M59.61 32.77c-.96-.03-1.84.53-2.23 1.4l-.03-3.87h-.85v9.63h.85v-4.06c-.1-1.21.79-2.28 2-2.38.06-.01.12-.01.18-.01 1.38 0 1.77.9 1.77 2.1v4.35h.85v-4.47c.01-1.66-.58-2.69-2.54-2.69z"
        />
        <path
          id="prefix__Caminho_615"
          className="prefix__st0"
          d="M66.29 32.77c-2.21 0-3.14 1.85-3.14 3.68 0 1.99.93 3.69 3.14 3.69a2.85 2.85 0 003.04-2.5h-.85a2.119 2.119 0 01-2.19 1.78c-1.63 0-2.28-1.5-2.3-2.74h5.36c.07-1.95-.82-3.91-3.06-3.91zm0 .72c1.29.07 2.28 1.17 2.22 2.46v.02H64a2.45 2.45 0 012.29-2.48z"
        />
        <path
          id="prefix__Caminho_616"
          className="prefix__st0"
          d="M73.17 32.77c-2.13 0-3.29 1.69-3.29 3.68s1.16 3.69 3.29 3.69 3.29-1.69 3.29-3.69-1.16-3.68-3.29-3.68zm0 6.65c-1.63 0-2.44-1.49-2.44-2.97s.81-2.97 2.44-2.97 2.44 1.49 2.44 2.97-.81 2.97-2.44 2.97z"
        />
        <path
          id="prefix__Caminho_617"
          className="prefix__st0"
          d="M78.27 34.61l-.03-1.63h-.78v6.96h.85v-3.71a2.41 2.41 0 012.29-2.51c.1 0 .2 0 .3.01v-.85c-1.17-.09-2.25.62-2.63 1.73z"
        />
        <path
          id="prefix__Ret\xE2ngulo_707"
          className="prefix__st0"
          d="M81.45 32.97h.85v6.96h-.85z"
        />
        <path
          id="prefix__Ret\xE2ngulo_708"
          className="prefix__st0"
          d="M81.45 30.3h.85v1.36h-.85z"
        />
        <path
          id="prefix__Ret\xE2ngulo_709"
          className="prefix__st0"
          d="M108.59 32.97h.85v6.96h-.85z"
        />
        <path
          id="prefix__Ret\xE2ngulo_710"
          className="prefix__st0"
          d="M108.59 30.3h.85v1.36h-.85z"
        />
        <path
          id="prefix__Caminho_618"
          className="prefix__st0"
          d="M120.81 36.12l-1.13-.26c-.58-.15-1.44-.42-1.44-1.17 0-.9.89-1.2 1.65-1.2.88-.12 1.68.49 1.81 1.36.01.05.01.11.01.16h.85a2.288 2.288 0 00-2.31-2.26c-.08 0-.16.01-.24.02-1.23 0-2.62.54-2.62 1.96 0 1.19.84 1.57 1.94 1.86l1.1.24c.77.19 1.52.46 1.52 1.27 0 .97-1.11 1.32-1.9 1.32-1.01.12-1.92-.6-2.04-1.61-.01-.04-.01-.08-.01-.13h-.85c.02 1.39 1.17 2.5 2.56 2.48.1 0 .2-.01.3-.02 1.29 0 2.79-.58 2.79-2.09a2.031 2.031 0 00-1.99-1.93z"
        />
        <path
          id="prefix__Ret\xE2ngulo_711"
          className="prefix__st0"
          d="M123.77 32.97h.85v6.96h-.85z"
        />
        <path
          id="prefix__Ret\xE2ngulo_712"
          className="prefix__st0"
          d="M123.77 30.3h.85v1.36h-.85z"
        />
        <path
          id="prefix__Caminho_619"
          className="prefix__st0"
          d="M129.25 36.12l-1.13-.26c-.58-.15-1.44-.42-1.44-1.17 0-.9.89-1.2 1.65-1.2.88-.12 1.68.49 1.81 1.36.01.05.01.11.01.16h.85a2.288 2.288 0 00-2.31-2.26c-.08 0-.16.01-.24.02-1.23 0-2.62.54-2.62 1.96 0 1.19.84 1.57 1.94 1.86l1.09.24c.77.19 1.52.46 1.52 1.27 0 .97-1.11 1.32-1.9 1.32-1.01.12-1.92-.6-2.04-1.6-.01-.05-.01-.09-.01-.14h-.85a2.525 2.525 0 002.86 2.46c1.3 0 2.79-.58 2.79-2.09a2.008 2.008 0 00-1.98-1.93z"
        />
        <path
          id="prefix__Caminho_620"
          className="prefix__st0"
          d="M143.96 34.61l-.03-1.63h-.78v6.96h.85v-3.71a2.41 2.41 0 012.29-2.51c.1 0 .2 0 .3.01v-.85c-1.17-.1-2.25.62-2.63 1.73z"
        />
        <path
          id="prefix__Caminho_621"
          className="prefix__st0"
          d="M159.33 32.97h-1.07l-1.94 2.65-1.98-2.65h-1.07l2.5 3.35-2.56 3.43v.13c-.04.01-.07.01-.11.02l-.03.04h1.05l2.15-2.93 2.18 2.93h1.08l-2.71-3.63 2.51-3.34z"
        />
        <path
          id="prefix__Caminho_622"
          className="prefix__st0"
          d="M153.2 39.75l-.11.15c.04-.01.07-.01.11-.02v-.13z"
        />
        <path
          id="prefix__Ret\xE2ngulo_713"
          className="prefix__st0"
          d="M160.09 32.97h.85v6.96h-.85z"
        />
        <path
          id="prefix__Ret\xE2ngulo_714"
          className="prefix__st0"
          d="M160.09 30.3h.85v1.36h-.85z"
        />
        <path
          id="prefix__Caminho_623"
          className="prefix__st0"
          d="M165.58 36.12l-1.13-.26c-.58-.15-1.44-.42-1.44-1.17 0-.9.89-1.2 1.65-1.2.88-.12 1.68.49 1.81 1.36.01.05.01.11.01.16h.85a2.294 2.294 0 00-2.31-2.26c-.08 0-.16.01-.24.02-1.23 0-2.62.54-2.62 1.96 0 1.19.84 1.57 1.94 1.86l1.09.24c.77.19 1.52.46 1.52 1.27 0 .97-1.11 1.32-1.9 1.32-1.01.12-1.92-.6-2.04-1.6-.01-.05-.01-.09-.01-.14h-.85c.02 1.39 1.17 2.5 2.56 2.48.1 0 .2-.01.3-.02 1.29 0 2.79-.58 2.79-2.09a2.023 2.023 0 00-1.98-1.93z"
        />
      </g>
    </svg>
  );
}

export default LogoBig;
