import React from 'react';
import styled from 'styled-components';
import { storiesOf } from '@storybook/react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExchange, faTable, faList, faExpand } from '@fortawesome/pro-light-svg-icons';

import Button from '../Button';
import MultiToggle from '../MultiToggle';
import Tooltip from '../Tooltip';

const icons = [
  {
    onClick: () => console.log('Table'),
    children: (
      <Tooltip
        content={'Table'}
        trigger={<FontAwesomeIcon icon={faTable} />}
        position={'top center'}
      />
    ),
    selected: false,
  },
  {
    onClick: () => console.log('List'),
    children: (
      <Tooltip
        content={'List'}
        trigger={<FontAwesomeIcon icon={faList} />}
        position={'top center'}
      />
    ),
    selected: false,
  },
  {
    onClick: () => console.log('Expand'),
    children: (
      <Tooltip
        content={'Expand'}
        trigger={<FontAwesomeIcon icon={faExpand} />}
        position={'top center'}
      />
    ),
    selected: false,
  },
];

storiesOf('Components/Tooltip', module)
  .add('MultiToggle w/ tooltip', () => (
    <Wrapper>
      <MultiToggle content={'icons'} buttons={icons}></MultiToggle>
    </Wrapper>
  ))
  .add('Button w/ tooltip', () => (
    <Wrapper>
      <Tooltip
        content={'Button'}
        trigger={
          <Button>
            <FontAwesomeIcon icon={faExchange} />
          </Button>
        }
      />
    </Wrapper>
  ));

const Wrapper = styled.div`
  background-color: ${({ theme }) => theme.colors.primary};
  width: 500px;
  height: 500px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 auto;
`;
