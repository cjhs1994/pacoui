/**
 * Tooltip component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React, { useState } from 'react';
import styled from 'styled-components';

import { Popup } from 'semantic-ui-react';

interface ITooltip {
  content: string | React.ReactNode;
  trigger: JSX.Element;
  open?: boolean;
  defaultOpen?: boolean;
  position?:
    | 'top center'
    | 'top left'
    | 'top right'
    | 'bottom center'
    | 'bottom left'
    | 'bottom right'
    | 'right center'
    | 'left center';
  disabled?: boolean;
}

const Tooltip: React.FC<ITooltip> = ({ content, trigger, position, defaultOpen, disabled }) => {
  const [isOpen, setIsOpen] = useState(defaultOpen || false);

  return (
    <StyledTooltip
      content={content}
      trigger={trigger}
      onOpen={() => setIsOpen(true)}
      open={isOpen && !disabled}
      basic
      onClose={() => setIsOpen(false)}
      position={position || 'bottom center'}
    />
  );
};

const StyledTooltip = styled(Popup)`
  position: relative;
  &&& {
    background-color: ${({ theme }) => theme.colors.blackLight};
    color: ${({ theme }) => theme.colors.white};
    font-family: Roboto;
    border-color: ${({ theme }) => theme.colors.softRegularGrey};
    border-radius: 0;
    margin: 4px;
    padding: 4px 8px 3px 8px;
    font-size: ${({ theme }) => theme.sizes.small};

    &:before {
      background-color: transparent;
      border-color: transparent;
      box-shadow: none;
    }
  }
`;

export default Tooltip;
