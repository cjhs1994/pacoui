/**
 * RadioList component
 *
 * @author Diogo Guedes <dguedes@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';
import ReactMarkdown from 'react-markdown';
import Text from '../Text';

import theme from '../theme';

interface IProps extends React.HTMLAttributes<HTMLInputElement> {
  inverted?: boolean;
  error?: string;
  options: {
    name: string;
    valueId: number;
    label: string;
    descriptionLabel?: string;
  }[];
  defaultValue?: number;
  value: any;
  onChange: (value: any) => {};
}

const RadioList: React.FC<IProps> = ({
  error,
  options,
  inverted,
  onChange,
  defaultValue,
  value,
  ...props
}) => {
  return (
    <Wrapper>
      <ListWrapper hasDescription={!!options[0].descriptionLabel}>
        {Object.keys(options).map((key, index) => {
          return (
            <RadioButtonWrapper
              key={`radioBtn-${index}`}
              hasDescription={!!options[0].descriptionLabel}
              last={index === options.length - 1}
            >
              <Label hasLabel={options[key].label ? true : false}>
                {options[key].label && typeof options[key].label === 'string' ? (
                  <Text color={inverted ? theme.colors.white : theme.colors.plusDarkGrey}>
                    {options[key].label}
                  </Text>
                ) : (
                  options[key].label
                )}
                <Input
                  inverted={inverted}
                  type="radio"
                  name={options[key].name}
                  onChange={(e) => {
                    onChange(options[key].valueId);
                  }}
                  defaultChecked={defaultValue === options[key].valueId}
                  {...props}
                />
                <CustomInput inverted={inverted} />
              </Label>

              {options[key].descriptionLabel && (
                <DescriptionWrapper>
                  <Text size="xSmall" weight="regular" color="darkGrey">
                    <ReactMarkdown
                      key={`markdown-${options[key].name}`}
                      children={options[key].descriptionLabel}
                      renderers={{ paragraph: 'span' }}
                    />
                  </Text>
                </DescriptionWrapper>
              )}
            </RadioButtonWrapper>
          );
        })}
      </ListWrapper>

      <MessageArea>
        {error && (
          <Text size="xSmall" weight="medium" color="dangerRed">
            {error}
          </Text>
        )}
      </MessageArea>
    </Wrapper>
  );
};
export default RadioList;

const Label = styled.label<{ hasLabel: boolean }>`
  display: block;
  display: flex;
  position: relative;
  padding-top: 4px; /*TODO: fix this to center label with radio button*/
  cursor: pointer;
  padding-left: 28px;
  user-select: none;
  user-select: none;
`;

const CustomInput = styled.span<{ inverted?: boolean }>`
  position: absolute;
  top: 0;
  left: 0;
  height: 20px;
  width: 20px;
  background-color: transparent;
  border-radius: 10px;
  border: 1px solid
    ${({ theme, inverted }) => (inverted ? theme.colors.softGrey : theme.colors.plusDarkGrey)};
  opacity: ${({ inverted }) => (inverted ? 0.6 : 1)};

  :hover {
    border: 1px solid
      ${({ theme, inverted }) => (inverted ? theme.colors.white : theme.colors.primary)};
    opacity: 1;
  }

  :after {
    content: '';
    position: absolute;
    display: none;
    left: 4px;
    top: 4px;
    width: 10px;
    height: 10px;
    border-radius: 5px;
    background-color: ${({ theme }) => theme.colors.primary};
  }
`;

const Input = styled.input<{ inverted?: boolean }>`
  opacity: 0;
  position: absolute;
  cursor: pointer;
  height: 0px;
  width: 0px;

  &:checked + ${CustomInput} {
    border: 1px solid
      ${({ theme, inverted }) => (inverted ? theme.colors.white : theme.colors.primary)};
    opacity: 1;

    :after {
      display: block;
    }
  }
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: flex-start;
`;

const RadioButtonWrapper = styled.div<{ hasDescription?: boolean; last: boolean }>`
  display: flex;
  align-items: ${({ hasDescription }) => (hasDescription ? 'left' : 'center')};
  justify-content: flex-start;
  flex-direction: ${({ hasDescription }) => (hasDescription ? 'column' : 'row')};
  > input {
    margin-right: 8px;
  }

  margin-right: ${({ hasDescription }) => (hasDescription ? '0px' : '16px')};

  margin-bottom: ${({ hasDescription, last }) =>
    hasDescription ? (last ? '0px' : '22px') : '0px'};
`;

const DescriptionWrapper = styled.div`
  margin-top: 12px;

  && a {
    color: ${({ theme }) => theme.colors.primary};

    :hover {
      text-decoration: underline;
    }
  }
`;

const MessageArea = styled.div``;

const ListWrapper = styled.div<{ hasDescription?: boolean }>`
  display: flex;
  align-items: left;
  flex-direction: ${({ hasDescription }) => (hasDescription ? 'column' : 'row')};
`;
