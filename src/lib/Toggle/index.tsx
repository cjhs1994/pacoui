/**
 * Toggle component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import { Checkbox } from 'semantic-ui-react';

import LockOpen from './images/lock-open.svg';
import LockClose from './images/lock-close.svg';

interface IToggle {
  onChange?: (event: React.SyntheticEvent<HTMLElement>, data: { [key: string]: any }) => void;
  checked?: boolean;
}

const Toggle: React.FC<IToggle> = ({ checked, onChange }) => {
  return (
    <Wrapper>
      <StyledToggle toggle onChange={onChange} checked={checked} />
    </Wrapper>
  );
};

export default Toggle;

const Wrapper = styled.div`
  .ui.toggle.checkbox input {
    width: 44px;
    height: 18px;
  }

  .ui.toggle.checkbox .box:before,
  .ui.toggle.checkbox label:before {
    width: 44px;
    height: 18px;
    background: ${({ theme }) => theme.colors.dangerRed} !important;
  }

  .ui.toggle.checkbox input:checked ~ .box:before,
  .ui.toggle.checkbox input:checked ~ label:before {
    background: ${({ theme }) => theme.colors.successGreen} !important;
  }

  .ui.toggle.checkbox label:after {
    width: 18px;
    height: 18px;
    background-image: url('${LockClose}');
    background-size: 8px 9px;
    background-repeat: no-repeat;
    background-position: center;
  }

  .ui.toggle.checkbox input:checked ~ label:after {
    left: 27px;
    background-image: url('${LockOpen}');
    background-size: 8px 9px;
    background-repeat: no-repeat;
    background-position: center;
  }
`;

const StyledToggle = styled(Checkbox)``;
