import React from 'react';
import { storiesOf } from '@storybook/react';

import Toggle from './index';

storiesOf('Components/Toggle', module).add('Toggle', () => <Toggle />);
