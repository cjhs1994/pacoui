import React from 'react';
import { storiesOf } from '@storybook/react';

import Card from './index';
import StatusLabel from '../StatusLabel';

storiesOf('Components/Card', module)
  .add('Card Active', () => (
    <Card
      active
      title="Title example"
      headerRight={<StatusLabel label="Example" background="red" />}
      expandedContent={<>Expanded content Example</>}
    >
      Content
    </Card>
  ))
  .add('Card Inactive', () => (
    <Card
      title="Title example"
      headerRight={<StatusLabel label="Example" background="red" />}
      expandedContent={<>Expanded content Example</>}
    >
      Content
    </Card>
  ));
