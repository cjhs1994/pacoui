/**
 * Card component
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */

import React from 'react';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown } from '@fortawesome/pro-light-svg-icons';

import Text from '../Text';
import Accordion from '../Accordion';
import Button from '../Button';
import theme from '../theme';

interface IProps {
  active?: boolean;
  shadow?: 'soft' | 'medium' | 'strong';
  title?: string | React.ReactNode;
  headerRight?: React.ReactNode;
  expandedContent?: React.ReactNode;
  children: React.ReactNode;
  noPadding?: boolean;
}

const Card: React.FC<IProps> = ({
  active,
  shadow,
  title,
  headerRight,
  expandedContent,
  children,
  noPadding,
}) => {
  const accordionStructure = [
    {
      parent: (
        <ExpandIcon>
          <ButtonComponent action borderless>
            <FontAwesomeIcon size="2x" icon={faAngleDown} />
          </ButtonComponent>
        </ExpandIcon>
      ),
      children: [
        <React.Fragment key={`cardUIComponentChildren`}>{expandedContent}</React.Fragment>,
      ],
    },
  ];

  return (
    <Wrapper noPadding={noPadding} shadow={shadow || 'soft'} active={active}>
      {(title || headerRight) && (
        <HeaderWrapper noPadding={noPadding}>
          <Text transform="uppercase" weight="regular" color="primary" size="medium">
            {title && title}
          </Text>
          {headerRight && headerRight}
        </HeaderWrapper>
      )}
      <Content>{children}</Content>
      {expandedContent && <Accordion structure={accordionStructure} />}
    </Wrapper>
  );
};

export default Card;

interface IWrapper {
  active: boolean | undefined;
  noPadding: boolean | undefined;
  shadow: 'soft' | 'medium' | 'strong';
}

//can't have height of 100%
const Wrapper = styled.div<IWrapper>`
  position: relative;
  width: 100%;
  box-shadow: ${({ shadow, theme }) => {
    switch (shadow) {
      case 'soft':
        return theme.shadows.soft;
      case 'medium':
        return theme.shadows.medium;
      case 'strong':
        return theme.shadows.strong;
    }
  }};
  padding: ${({ noPadding }) => (noPadding ? `0` : `14px 20px 20px 20px`)};
  border: ${({ theme, active }) =>
    active
      ? `1px solid rgba(${theme.colors.primaryRGB[0]}, ${theme.colors.primaryRGB[1]}, ${theme.colors.primaryRGB[2]}, 0.3)`
      : '0'};
  border-top: ${({ theme, active }) => (active ? `3px solid ${theme.colors.primary}` : '0')};
`;

const Content = styled.div``;

const HeaderWrapper = styled.div<{ noPadding: boolean | undefined }>`
  display: flex;
  justify-content: space-between;
  padding: ${({ noPadding }) => (noPadding ? '14px 20px 0 20px' : '0')};
  margin-bottom: ${({ noPadding }) => (noPadding ? '0' : '24px')};
`;

const ExpandIcon = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 12px;
  margin-bottom: -8px;
`;

const ButtonComponent = styled(Button)`
  width: 100%;
`;
