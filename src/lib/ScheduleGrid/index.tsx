/**
 * ScheduleGrid container
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */
import React, { useState, useContext, useRef, useEffect, useMemo } from 'react';
import styled from 'styled-components';
import { withSize } from 'react-sizeme';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowsH, faChevronLeft, faChevronRight } from '@fortawesome/pro-light-svg-icons';
import { faTimes } from '@fortawesome/pro-light-svg-icons';
import moment from 'moment';

import AnimatedBackground from '../AnimatedBackground';

import ClassCard from '../ClassCard';
import Tooltip from '../Tooltip';
import Button from '../Button';
import NoDataMessage from '../NoDataMessage';
import theme from '../theme';

import {
  setIntervals,
  setWeekdays,
  sortClassesByWeek,
  nextWeek,
  prevWeek,
  //getDetailsOrientation,
  getWeekdaysCells,
} from './utils';
import { IScheduleClass } from '../types';

interface IProps {
  t?: any;
  onSelect?: Function;
  weekdays: number;
  disabled?: boolean;
  startTime: string;
  endTime: string;
  intervalInMin: number;
  expandedDay?: number | null;
  onToggleWeekDay?: (weekday: number | null) => void;
  onReady?: (sortedClasses: any[]) => void;
  schedule: {
    ucId: number;
    ucColor: string;
    ucInitials: string;
    ucFullName: string;
    classSchedule: IScheduleClass[];
  }[];
  loading?: boolean;
  size: any;
}

const ScheduleGrid: React.FC<IProps> = ({
  t,
  onSelect,
  schedule,
  disabled,
  loading,
  weekdays,
  startTime,
  endTime,
  intervalInMin,
  expandedDay,
  onToggleWeekDay,
  onReady,
  size,
}) => {
  const [timeCellWidth] = useState(60);
  const cols = useRef(null as number | null);
  const prevScheduleVerifyWeekGrid = useRef(null as any);

  const [cellGap] = useState(3);
  const [cellSplit] = useState(4);

  const [cellWidth, setCellWidth] = useState<number | null>(null);

  const [expandedWeekday, setExpandedWeekday] = useState<number | null>(null);
  const didExpandWeekDay = useRef(true);

  const [maxCellWidth] = useState(200);
  const [cellHeight] = useState(32);
  const [weekdayCellHeight] = useState(38);

  const [hoveredClassId, setHoveredClassId] = useState<number | null>(null);

  const getClassesByCellWidth = (withCols, init) => {
    if (cellWidth !== null && expandedWeekday === null && !init) {
      let sortedClasses;
      if (cellWidth < maxCellWidth) {
        if (withCols) {
          if (cols.current !== 1) {
            cols.current = 1;
            sortedClasses = sortClassesByWeek(
              schedule,
              weekdays,
              timeIntervalsCellSplit,
              1,
              cellSplit,
              expandedWeekday === null && didExpandWeekDay.current
            );
          }
        } else {
          sortedClasses = sortClassesByWeek(
            schedule,
            weekdays,
            timeIntervalsCellSplit,
            1,
            cellSplit,
            expandedWeekday === null && didExpandWeekDay.current
          );
        }
      } else if (cellWidth >= maxCellWidth) {
        if (withCols) {
          if (cols.current !== 2) {
            cols.current = 2;
            sortedClasses = sortClassesByWeek(
              schedule,
              weekdays,
              timeIntervalsCellSplit,
              2,
              cellSplit,
              expandedWeekday === null && didExpandWeekDay.current
            );
          }
        } else {
          sortedClasses = sortClassesByWeek(
            schedule,
            weekdays,
            timeIntervalsCellSplit,
            2,
            cellSplit,
            expandedWeekday === null && didExpandWeekDay.current
          );
        }
      }

      didExpandWeekDay.current = false;
      return sortedClasses;
    }
  };

  const shouldExpandWeekday = (weekday) => {
    if (
      cellWidth !== null &&
      classesByCellWidth &&
      classesByCellWidth[weekday].classes.length > 0
    ) {
      if (cellWidth < maxCellWidth) {
        if (classesByCellWidth[weekday].totalCols > 1) {
          return true;
        }
      } else {
        if (classesByCellWidth[weekday].totalCols > 2) {
          return true;
        }
      }
    }

    return false;
  };

  const canExpandWeekday = (weekday) => {
    if (cellWidth !== null && classesByCellWidth) {
      if (classesByCellWidth[weekday].classes.length > 0) {
        return true;
      }
    }

    return false;
  };

  //if receives prop for expandedDay use
  useEffect(() => {
    if (expandedDay !== undefined) {
      didExpandWeekDay.current = true;
      setExpandedWeekday(expandedDay);
    }
  }, [expandedDay]);

  const timeIntervals = setIntervals(startTime, endTime, intervalInMin);
  const timeIntervalsCellSplit = setIntervals(startTime, endTime, intervalInMin / cellSplit);

  const weekdaysList = setWeekdays(weekdays);

  const sortByWeek = () => {
    let sortedClasses = sortClassesByWeek(
      schedule,
      weekdays,
      timeIntervalsCellSplit,
      null,
      cellSplit,
      false
    );
    return [...sortedClasses];
  };

  const sortedClasses = useMemo(sortByWeek, [
    schedule,
    cellWidth,
    startTime,
    endTime,
    intervalInMin,
  ]);

  const [classesByCellWidth, setClassesByCellWidth] = useState(getClassesByCellWidth(false, true));

  useEffect(() => {
    const newClasses = getClassesByCellWidth(true, false);
    if (newClasses) {
      setClassesByCellWidth(newClasses);
    }
  }, [cellWidth]);

  useEffect(() => {
    setClassesByCellWidth(getClassesByCellWidth(false, false));
  }, [startTime, endTime, intervalInMin]);

  useEffect(() => {
    if (!!classesByCellWidth?.length && !!schedule?.length && prevScheduleVerifyWeekGrid.current) {
      const scheduleClasses = schedule
        .reduce((acc, uc) => [...acc, ...uc.classSchedule], [] as any)
        .filter((classItem) => !classItem.filtered);

      const prevScheduleClasses = prevScheduleVerifyWeekGrid.current
        .reduce((acc, uc) => [...acc, ...uc.classSchedule], [] as any)
        .filter((classItem) => !classItem.filtered);

      const sameItems =
        scheduleClasses.every((scheduleClass) =>
          prevScheduleClasses.some(
            (prevScheduleClass) =>
              prevScheduleClass.classId === scheduleClass.classId &&
              prevScheduleClass.filtered === scheduleClass.filtered
          )
        ) &&
        prevScheduleClasses.every((prevScheduleClass) =>
          scheduleClasses.some(
            (scheduleClass) =>
              prevScheduleClass.classId === scheduleClass.classId &&
              prevScheduleClass.filtered === scheduleClass.filtered
          )
        );

      if (!sameItems) {
        setClassesByCellWidth(getClassesByCellWidth(false, false));
      } else {
        const newClassesByCellWidth = classesByCellWidth.map((classCell) => {
          return {
            ...classCell,
            classes: classCell.classes.map((classItem) => {
              const item = scheduleClasses.find((scheduleClass) => {
                return (
                  scheduleClass.classId === classItem.schedule.classId &&
                  scheduleClass.startTime === classItem.schedule.startTime &&
                  scheduleClass.endTime === classItem.schedule.endTime &&
                  scheduleClass.weekday === classItem.schedule.weekday &&
                  scheduleClass.classRoom === classItem.schedule.classRoom
                );
              });

              return {
                data: {
                  ...classItem.data,
                  periodDays: item.periodDays,
                  selected: item.selected,
                },
                schedule: item,
                position: classItem.position,
              };
            }),
          };
        });

        setClassesByCellWidth(newClassesByCellWidth);
      }
    }
    prevScheduleVerifyWeekGrid.current = schedule;
  }, [schedule]);

  useEffect(() => {
    if (!expandedWeekday) {
      setClassesByCellWidth(getClassesByCellWidth(false, false));
    }
  }, [expandedWeekday]);

  useEffect(() => {
    if (size.width !== null) {
      setCellWidth(
        (size.width - (timeCellWidth + weekdaysList.length * cellGap)) / weekdaysList.length
      );
    }
  }, [size, timeCellWidth, weekdaysList, cellGap]);

  useEffect(() => {
    onReady && onReady(sortedClasses);
  }, [sortedClasses, onReady]);

  //TODO remove translation from this part
  if (!schedule.length && !loading) {
    return (
      <NoDataMessageWrapper>
        <NoDataMessage
          header={t ? t('sgh.noUc_plural') : 'Sem unidades curriculares'}
          width={'50%'}
        />
      </NoDataMessageWrapper>
    );
  }

  return (
    <Wrapper>
      {/*create main background grid*/}
      <Grid
        cellGap={cellGap}
        cellHeight={cellHeight}
        weekdayCellHeight={weekdayCellHeight}
        timeCellWidth={timeCellWidth}
        cols={expandedWeekday !== null ? 1 : weekdays}
        rows={timeIntervals.length}
      >
        <BlankCell />
        {weekdaysList
          .slice(
            expandedWeekday || 0,
            expandedWeekday !== null ? expandedWeekday + 1 : weekdaysList.length
          )
          .map((weekday) => {
            const key = weekdaysList.indexOf(weekday);
            if (expandedWeekday === null) {
              return shouldExpandWeekday(key) ? (
                <Tooltip
                  key={`scheduleGrid_expand_weekday_title_${key}`}
                  content={'Expandir Dia'}
                  trigger={
                    <div>
                      <ExpandButton
                        onClick={() => {
                          if (canExpandWeekday(key)) {
                            onToggleWeekDay && onToggleWeekDay(key);
                            didExpandWeekDay.current = true;
                            setExpandedWeekday(key);
                          }
                        }}
                        color={theme.colors.darkGrey}
                        action
                        borderless
                        data-testid={`schedule_grid_button_expand_weekday`}
                        leftIcon={
                          <ExpandIcon>
                            <FontAwesomeIcon size="lg" icon={faArrowsH} />
                          </ExpandIcon>
                        }
                      >
                        {weekday}
                      </ExpandButton>
                    </div>
                  }
                />
              ) : (
                <Title
                  key={`scheduleGrid_weekday_title_${key}`}
                  data-testid={`schedule_grid_div_expand_weekday`}
                >
                  {weekday}
                </Title>
              );
            } else {
              return (
                <Title key={`scheduleGrid_weekday_title_${key}`}>
                  {expandedWeekday !== null && (
                    <Button
                      onClick={() => {
                        const prevWeekKey = prevWeek(expandedWeekday, weekdaysList, sortedClasses);
                        onToggleWeekDay && onToggleWeekDay(prevWeekKey);
                        didExpandWeekDay.current = true;
                        setExpandedWeekday(prevWeekKey);
                      }}
                      color={theme.colors.darkGrey}
                      borderless
                      data-testid={`schedule_grid_button_prev_weekday`}
                    >
                      <FontAwesomeIcon icon={faChevronLeft} />
                    </Button>
                  )}

                  <TitleExpanded
                    key={`scheduleGrid_weekday_toggle_title_${key}`}
                    data-testid={`schedule_grid_div_toggle_weekday`}
                  >
                    {weekday}
                  </TitleExpanded>

                  {expandedWeekday !== null && (
                    <Button
                      onClick={() => {
                        const nextWeekKey = nextWeek(expandedWeekday, weekdaysList, sortedClasses);
                        onToggleWeekDay && onToggleWeekDay(nextWeekKey);
                        didExpandWeekDay.current = true;
                        setExpandedWeekday(nextWeekKey);
                      }}
                      color={theme.colors.darkGrey}
                      borderless
                      data-testid={`schedule_grid_button_next_weekday`}
                    >
                      <FontAwesomeIcon icon={faChevronRight} />
                    </Button>
                  )}

                  <CloseButton>
                    <Tooltip
                      content={'Colapsar Dia'}
                      trigger={
                        <Button
                          onClick={() => {
                            setExpandedWeekday(null);
                          }}
                          size={theme.sizes.xLarge}
                          color={theme.colors.darkGrey}
                          borderless
                          data-testid={`schedule_grid_button_collapse_weekday`}
                        >
                          <FontAwesomeIcon icon={faTimes} />
                        </Button>
                      }
                    />
                  </CloseButton>
                </Title>
              );
            }
          })}

        {timeIntervals.map((time, timeKey) => (
          <React.Fragment key={`scheduleGrid_row_items_${timeKey}`}>
            <RowItem>{time}</RowItem>
            {timeKey !== timeIntervals.length - 1 &&
              weekdaysList
                .slice(
                  expandedWeekday || 0,
                  expandedWeekday !== null ? expandedWeekday + 1 : weekdaysList.length
                )
                .map((weekday) => (
                  <React.Fragment
                    key={`scheduleGrid_row_items_${timeKey}_cell_${weekdaysList.indexOf(weekday)}`}
                  >
                    {loading && <AnimatedBackground height={`${cellHeight}px`} width="100%" />}
                    {!loading && <EmptyCell light={timeKey % 2 === 0} />}
                  </React.Fragment>
                ))}
          </React.Fragment>
        ))}
      </Grid>

      {expandedWeekday === null && (
        <GridLayer top={weekdayCellHeight} cellHeight={cellHeight} cols={weekdays} rows={1}>
          {/*create new grid layer over background grid*/}
          {/*cellSplit determines how many cells are needed when cells are divided into parts*/}
          {/*map all the class cells to fit a schedule*/}
          {classesByCellWidth &&
            classesByCellWidth.map((weeklyClasses, weekdayKey) => (
              <GridWeekday
                cellGap={cellGap}
                cellSplit={cellSplit}
                cellHeight={cellHeight / cellSplit}
                key={`scheduleGrid_layer_weekday_${weekdayKey}`}
                rows={timeIntervals.length * cellSplit}
                cols={weeklyClasses.totalColsToShow}
              >
                {weeklyClasses.classes.map((classUnit, classKey) => {
                  return (
                    <Cell
                      key={`scheduleGrid_layer_weekday_${weekdayKey}_class_${classKey}`}
                      rows={classUnit.position.rows}
                      cols={classUnit.position.cols}
                    >
                      <ClassCard
                        onSelect={(classItem) => {
                          onSelect && onSelect(classItem);
                        }}
                        startTime={moment(classUnit.schedule.startTime, 'HH:mm').format('HH:mm')}
                        endTime={moment(classUnit.schedule.endTime, 'HH:mm').format('HH:mm')}
                        disabled={
                          disabled ||
                          classUnit.schedule.disabled ||
                          classUnit.schedule.allocated.state === 'assigned' ||
                          classUnit.schedule.allocated.state === 'automatic'
                        }
                        locked={classUnit.schedule.locked}
                        full={classUnit.schedule.full}
                        classItem={{
                          ucId: classUnit.data.id,
                          groupId: classUnit.data.groupId,
                          ucInitials: classUnit.data.initials,
                          color: classUnit.data.color,
                          classType: classUnit.schedule.classType,
                          className: classUnit.schedule.className,
                          classRoom: classUnit.schedule.classRoom,
                          irregularMessage: classUnit.schedule.irregularMessage,
                          classId: classUnit.schedule.classId,
                          selected: classUnit.schedule.selected,
                          filtered: classUnit.schedule.filtered,
                          periodDays: classUnit.data.periodDays,
                          allocation: classUnit.schedule.allocation,
                          allocated: classUnit.schedule.allocated,
                        }}
                        detailsOrientation="vertical"
                        onHover={(id) => setHoveredClassId(id)}
                        isHovered={hoveredClassId === classUnit.schedule.classId}
                        data-testid="schedule_grid_class_card"
                      />
                    </Cell>
                  );
                })}
              </GridWeekday>
            ))}
        </GridLayer>
      )}

      {expandedWeekday !== null && (
        <GridLayer
          cellHeight={cellHeight / cellSplit}
          cellGap={cellGap}
          cellSplit={cellSplit}
          top={weekdayCellHeight}
          bottom={cellHeight}
          cols={sortedClasses[expandedWeekday].classes.length}
          rows={timeIntervals.length * cellSplit}
          expanded
        >
          {/*map all the class cells to fit a schedule*/}
          {sortedClasses[expandedWeekday].classes.map((classUnit, classKey) => (
            <CellExpanded
              key={`scheduleGrid_layer_expanded_weekday_${expandedWeekday}_class_${classKey}`}
              rows={classUnit.position.rows}
              cols={classUnit.position.cols}
            >
              <ClassCard
                onSelect={(classItem) => {
                  onSelect && onSelect(classItem);
                }}
                disabled={
                  disabled ||
                  classUnit.schedule.disabled ||
                  classUnit.schedule.allocated.state === 'assigned' ||
                  classUnit.schedule.allocated.state === 'automatic'
                }
                locked={classUnit.schedule.locked}
                full={classUnit.schedule.full}
                onHover={(id) => setHoveredClassId(id)}
                isHovered={hoveredClassId === classUnit.schedule.classId}
                startTime={moment(classUnit.schedule.startTime, 'HH:mm').format('HH:mm')}
                endTime={moment(classUnit.schedule.endTime, 'HH:mm').format('HH:mm')}
                classItem={{
                  ucId: classUnit.data.id,
                  groupId: classUnit.data.groupId,
                  ucInitials: classUnit.data.initials,
                  color: classUnit.data.color,
                  periodDays: classUnit.data.periodDays,
                  classType: classUnit.schedule.classType,
                  className: classUnit.schedule.className,
                  irregularMessage: classUnit.schedule.irregularMessage,
                  classRoom: classUnit.schedule.classRoom,
                  classId: classUnit.schedule.classId,
                  selected: classUnit.schedule.selected,
                  filtered: classUnit.schedule.filtered,
                  allocation: classUnit.schedule.allocation,
                  allocated: classUnit.schedule.allocated,
                }}
                detailsOrientation="vertical"
                data-testid="schedule_grid_class_card"
              />
            </CellExpanded>
          ))}
        </GridLayer>
      )}
    </Wrapper>
  );
};

export default withSize({ refreshMode: 'debounce', refreshRate: 120 })(ScheduleGrid);

const Wrapper = styled.div`
  position: relative;
`;

interface IGrid {
  cols: number;
  rows: number;
  cellHeight: number;
  timeCellWidth: number;
  cellGap: number;
  weekdayCellHeight: number;
}

const Grid = styled.div<IGrid>`
  ${({ timeCellWidth, weekdayCellHeight, rows, cellHeight, cols, cellGap }) => `
    display: grid;
    grid-template-columns: ${timeCellWidth}px repeat(${cols}, minmax(100px, 1fr) );
    grid-template-rows: ${weekdayCellHeight}px repeat(${rows}, ${cellHeight}px [col-start]);
    column-gap: ${cellGap}px;
    row-gap: ${cellGap}px;
  `}
`;

const BlankCell = styled.div``;

const EmptyCell = styled.div<{ light: boolean }>`
  ${({ light, theme }) => `
    background-color: ${
      light ? theme.scheduleGrid.colors.lightGrey : theme.scheduleGrid.colors.darkGrey
    };
  `}
`;

interface ITitle {
  canExpand?: boolean;
}

const Title = styled.div<ITitle>`
  ${({ canExpand, theme }) => `
    ${canExpand ? `cursor: pointer;` : ``}
    text-align: center;
    text-transform: uppercase;
    color: ${theme.colors.primary};
    letter-spacing: 0px;
    font-size: 16px;
    display: inline-flex;
    align-items:center;
    justify-content:center;
  `}
`;

const RowItem = styled.div`
  ${({ theme }) => `
    text-align: right;
    padding-right: 12px;
    color: ${theme.colors.darkGrey};
    font-size: 12px;
    font-weight: 500;
    display: flex;
    justify-content: flex-end;
    align-items: flex-start;
    margin-top: -10px;
  `}
`;

interface ICell {
  rows: { from: number; to: number };
  cols: { from: number; to: number };
}

const Cell = styled.div<ICell>`
  ${({ rows, cols }) => `
    grid-row: ${rows.from - 1} / ${rows.to - 1};
    grid-column: ${cols.from} / ${cols.to};
    display: flex;
  `}
`;

const CellExpanded = styled.div<ICell>`
  ${({ rows, cols }) => `
    grid-row: ${rows.from - 1} / ${rows.to - 1};
    grid-column: ${cols.from} / ${cols.from + 1};
    display: flex;

    @keyframes slideInFromLeft {
      0% {
        width: 0%;
        opacity: 0;
      }
      100% {
        width: 100%;
        opacity: 1;
      }
    }

    animation: 0.5s ease-in-out 0s 1 slideInFromLeft;
  `}
`;

interface IGridLayer {
  cols: number;
  rows: number;
  cellHeight: number;
  top: number;
  bottom?: number;
  expanded?: boolean;
  cellSplit?: number;
  cellGap?: number;
}

const GridLayer = styled.div<IGridLayer>`
  ${({ cols, top, bottom, expanded }) => `
    position: absolute;
    top: ${top}px;
    bottom: ${bottom || 0}px;
    left: 63px;
    width: calc(100% - 63px);
    display: grid;
    overflow-y: ${expanded ? `hidden` : `unset`};
    overflow-x: ${expanded ? `auto` : `unset`};
    grid-template-columns: repeat(${cols}, ${expanded ? `180px` : `minmax(100px, 1fr)`} );
    margin-bottom: -10px;
    column-gap: ${expanded ? `0` : `3px`};
    row-gap: 3px;
  `}

  ${({ expanded, rows, cellSplit, cellHeight, cellGap }) =>
    expanded &&
    `
    row-gap: 0px;
  grid-template-rows: ${getWeekdaysCells(rows, cellSplit, cellHeight, cellGap)};

  `}
`;

interface IGridWeekday {
  cols: number;
  rows: number;
  cellHeight: number;
  cellSplit: number;
  cellGap: number;
}

const GridWeekday = styled.div<IGridWeekday>`
  ${({ rows, cols, cellSplit, cellGap, cellHeight }) => `
    display: grid;
    grid-template-columns: repeat(${cols}, minmax(100px, 1fr));
    grid-template-rows: ${getWeekdaysCells(rows, cellSplit, cellHeight, cellGap)};

    @keyframes slideInFromLeft {
      0% {
        width: 0%;
        opacity: 0;
      }
      100% {
        width: 100%;
        opacity: 1;
      }
    }

    animation: 0.5s ease-in-out 0s 1 slideInFromLeft;
  `}
`;

const TitleExpanded = styled.div`
  text-transform: uppercase;
  padding: 0 42px 0 42px;
  letter-spacing: 0px;
  font-size: 16px;
  text-transform: uppercase;
  color: ${({ theme }) => theme.colors.primary};
  letter-spacing: 0px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
`;

const ExpandButton = styled(Button)`
  text-transform: uppercase;
  width: 100%;
  letter-spacing: 0px;
  font-size: 16px;
`;

const NoDataMessageWrapper = styled.div`
  /*height: 422px;*/
  display: flex;
  justify-content: center;
  padding-top: 100px;
`;

const CloseButton = styled.div`
  position: absolute;
  right: 0;
`;

const ExpandIcon = styled.div`
  height: 32px;
  width: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
