import React from 'react';
import { storiesOf } from '@storybook/react';
import ScheduleGrid from './index';

storiesOf('Containers/ScheduleGrid', module)
  .add('Grid during seriation', () => (
    <ScheduleGrid
      weekdays={7}
      startTime={'9:00'}
      endTime={'20:00'}
      intervalInMin={60}
      schedule={SCHEDULE}
    />
  ))
  .add('Grid after seriation', () => (
    <ScheduleGrid
      weekdays={7}
      startTime={'9:00'}
      endTime={'20:00'}
      intervalInMin={60}
      schedule={SCHEDULE_AFTER_SERIATION}
    />
  ));

export const SCHEDULE = [
  {
    ucInitials: 'C1',
    ucId: 3838,
    ucFullName: 'Cálculo I',
    ucColor: '#3b3b3b',
    classSchedule: [
      {
        classId: 14,
        weekday: 1,
        startTime: '11:00',
        endTime: '13:00',
        period: '2,4',
        classType: 'TP',
        className: '1',
        classRoom: '2.2.21',
        locked: false,
        overlaps: false,
        irregularMessage: '',
        selected: false,
        filtered: false,
        allocated: { state: 'notAssigned' } as const,
        allocation: 'certain' as 'likely' | 'uncertain' | 'unlikely',
      },
      {
        classId: 15,
        weekday: 2,
        startTime: '14:00',
        endTime: '16:00',
        period: '1,3',
        classType: 'TP',
        className: '1',
        classRoom: '2.2.21',
        locked: false,
        overlaps: false,
        irregularMessage: '',
        selected: false,
        filtered: false,
        allocated: { state: 'notAssigned' } as const,
        allocation: 'uncertain' as 'likely' | 'uncertain' | 'unlikely',
      },
      {
        classId: 16,
        weekday: 2,
        startTime: '14:00',
        endTime: '16:00',
        period: '1,3',
        classType: 'TP',
        className: '1',
        classRoom: '2.2.21',
        locked: false,
        selected: false,
        overlaps: false,
        irregularMessage: '',
        filtered: false,
        allocated: { state: 'notAssigned' } as const,
        allocation: 'unlikely' as 'likely' | 'uncertain' | 'unlikely',
      },
      {
        classId: 17,
        weekday: 5,
        startTime: '11:00',
        endTime: '13:00',
        period: null,
        classType: 'TP',
        className: '1',
        classRoom: '2.2.21',
        locked: false,
        overlaps: false,
        irregularMessage: '',
        selected: false,
        filtered: false,
        allocated: { state: 'notAssigned' } as const,
        allocation: 'uncertain' as 'likely' | 'uncertain' | 'unlikely',
      },
    ],
  },
  {
    ucInitials: 'O1',
    ucId: 5050,
    ucFullName: 'Opção 1',
    ucColor: '#3b3b3b',
    classSchedule: [],
  },
  {
    ucInitials: 'P1',
    ucId: 6534,
    ucFullName: 'Programação I',
    ucColor: '#3b3b3b',
    classSchedule: [],
  },
];

export const SCHEDULE_AFTER_SERIATION = [
  {
    ucInitials: 'C1',
    ucId: 3838,
    ucFullName: 'Cálculo I',
    ucColor: '#3b3b3b',
    classSchedule: [
      {
        classId: 14,
        weekday: 1,
        startTime: '11:00',
        endTime: '13:00',
        period: '2,4',
        classType: 'TP',
        className: '1',
        classRoom: '2.2.21',
        locked: false,
        overlaps: false,
        irregularMessage: '',
        selected: false,
        filtered: true,
        allocated: { state: 'assigned' } as const,
        allocation: 'certain' as 'likely' | 'uncertain' | 'unlikely',
      },
      {
        classId: 15,
        weekday: 2,
        startTime: '14:00',
        endTime: '16:00',
        period: '1,3',
        classType: 'TP',
        className: '1',
        classRoom: '2.2.21',
        overlaps: false,
        irregularMessage: '',
        locked: false,
        selected: false,
        filtered: false,
        allocated: { state: 'assigned' } as const,
        allocation: 'unlikely' as 'likely' | 'uncertain' | 'unlikely',
      },
      {
        classId: 16,
        weekday: 2,
        startTime: '14:00',
        endTime: '16:00',
        period: '1,3',
        classType: 'TP',
        className: '1',
        classRoom: '2.2.21',
        locked: false,
        overlaps: false,
        irregularMessage: '',
        selected: false,
        filtered: false,
        allocated: { state: 'assigned' } as const,
        allocation: 'certain' as 'likely' | 'uncertain' | 'unlikely',
      },
    ],
  },
  {
    ucInitials: 'O1',
    ucId: 5050,
    ucFullName: 'Opção 1',
    ucColor: '#3b3b3b',
    classSchedule: [],
  },
  {
    ucInitials: 'P1',
    ucId: 6534,
    ucFullName: 'Programção I',
    ucColor: '#3b3b3b',
    classSchedule: [],
  },
];
