/**
 * ScheduleGrid container utils
 *
 * @author Carlos Silva <csilva@ubiwhere.com>
 *
 */
import moment from 'moment';

export const setIntervals = (startTime, endTime, intervalInMin) => {
  let intervals: any[] = [];
  let start = moment(startTime, 'HH:mm');
  let end = moment(endTime, 'HH:mm');

  while (start <= end) {
    intervals.push(moment(start).format('HH:mm'));
    start.add(intervalInMin, 'minutes');
  }

  return intervals;
};

export const setWeekdays = (weekdays) => {
  let weekdaysList: any[] = moment.weekdays().map((weekday) => weekday.substring(0, 3));
  weekdaysList.push(weekdaysList.shift());
  weekdaysList = weekdaysList.slice(0, weekdays);
  return weekdaysList;
};

export const formatTime = (time) => {
  return moment(moment(time, 'HH:mm')).format('HH:mm');
};

export const isBefore = (startTime, endTime) => {
  return moment(startTime, 'HH:mm').isBefore(moment(endTime, 'HH:mm'));
};

export const isAfter = (startTime, endTime) => {
  if (startTime === endTime) {
    return true;
  }
  return moment(startTime, 'HH:mm').isAfter(moment(endTime, 'HH:mm'));
};

export const nextWeek = (expandedWeekday, weekdaysList, sortedClasses) => {
  let newKey = expandedWeekday;

  const noClassesThenIncrement = () => {
    newKey++;

    if (newKey > weekdaysList.length - 1) {
      newKey = 0;
    }
    if (sortedClasses[newKey].classes.length === 0) {
      noClassesThenIncrement();
    }
  };

  noClassesThenIncrement();

  return newKey;
};

export const prevWeek = (expandedWeekday, weekdaysList, sortedClasses) => {
  let newKey = expandedWeekday;

  const noClassesThenDecrement = () => {
    newKey--;

    if (newKey < 0) {
      newKey = weekdaysList.length - 1;
    }

    if (sortedClasses[newKey].classes.length === 0) {
      noClassesThenDecrement();
    }
  };

  noClassesThenDecrement();

  return newKey;
};

export const indexInInterval = (interval, time) => {
  time = moment(moment(time, 'HH:mm')).format('HH:mm');
  let foundKey = interval.indexOf(time);

  if (foundKey < 0) {
    for (const key in interval) {
      if (!isBefore(interval[key], time)) {
        if (parseInt(key) === 0) {
          foundKey = 0;
        } else {
          foundKey = parseInt(key) - 1;
        }
        break;
      }
    }
  }

  return foundKey;
};

export const sortClassesByWeek = (
  schedule,
  weekdays,
  timeInterval,
  maxCols,
  cellSplit,
  collapsedRow
) => {
  let weekdayClasses: any[] = [];

  //push empty weekdays
  for (let i = 0; i < weekdays; i++) {
    weekdayClasses.push({
      totalCols: 1,
      totalColsToShow: 1,
      lastCol: 0,
      timeCapException: null,
      classes: [],
    });
  }

  //sort by weekday and reformat object structure
  const sortedWeekClasses = schedule
    .map((uc) => [
      ...[
        ...uc.classSchedule.map((classItem) => ({
          ...classItem,
          ucId: uc.ucId,
          groupId: uc.groupId,
          ucInitials: uc.ucInitials,
          ucColor: uc.ucColor,
        })),
      ],
    ])
    .flat(1)
    .sort((a, b) => {
      const isAbefore = isBefore(b.startTime, a.startTime);
      return isAbefore ? 1 : -1;
    });
  /*.sort((a, b) => {
      //sortWeekClasses by selected in order to first show selected classes
      const isAbefore = isBefore(b.startTime, a.startTime);

      if (collapsedRow) {
        if (b.selected || isAbefore) {
          return 1;
        }
        if (a.selected || !isAbefore) {
          return -1;
        }
      }
    })*/

  sortedWeekClasses.forEach((classUnit) => {
    const classWeekday = classUnit.weekday === 0 ? 6 : classUnit.weekday - 1;
    if (!classUnit.filtered && classWeekday < weekdays) {
      let shouldRender = true;

      const lastClassUnit =
        weekdayClasses[classWeekday].classes[weekdayClasses[classWeekday].classes.length - 1] ||
        null;

      let timeCapException = null;

      if (lastClassUnit) {
        //check timeCap exception first
        if (!weekdayClasses[classWeekday].timeCapException) {
          timeCapException = lastClassUnit.schedule.endTime;
        } else {
          timeCapException = isBefore(
            lastClassUnit.schedule.endTime,
            weekdayClasses[classWeekday].timeCapException
          )
            ? weekdayClasses[classWeekday].timeCapException
            : lastClassUnit.schedule.endTime;
        }
      }

      weekdayClasses[classWeekday].timeCapException = timeCapException;

      //it overlaps
      if (
        lastClassUnit &&
        isBefore(classUnit.startTime, lastClassUnit.schedule.endTime) &&
        !isBefore(classUnit.endTime, lastClassUnit.schedule.startTime)
      ) {
        lastClassUnit.position.cols.to = weekdayClasses[classWeekday].totalColsToShow;
        //increase while max cols not reached
        if (maxCols && weekdayClasses[classWeekday].totalColsToShow < maxCols) {
          weekdayClasses[classWeekday].totalColsToShow++;
          weekdayClasses[classWeekday].totalCols++;
          //normalize previously added to respect new number of cols
          weekdayClasses[classWeekday].classes.forEach((classItem) => {
            classItem.position.cols.to = classItem.position.cols.to + 1;
          });
        }

        if (maxCols && weekdayClasses[classWeekday].lastCol >= maxCols) {
          shouldRender = false;
        } else {
          weekdayClasses[classWeekday].lastCol++;
        }
      } else {
        //timecap loophole exception for previous added items
        if (
          timeCapException &&
          !isAfter(classUnit.startTime, timeCapException) &&
          willOverlap(
            classUnit,
            sortedWeekClasses.filter((sortedClass) => sortedClass.weekday === classUnit.weekday)
          )
        ) {
          if (!maxCols) {
            weekdayClasses[classWeekday].lastCol++;
          } else {
            shouldRender = false;
          }
        } else {
          weekdayClasses[classWeekday].lastCol = 1;
        }
      }

      if (shouldRender) {
        let indexStart = indexInInterval(timeInterval, classUnit.startTime);
        let indexEnd = indexInInterval(timeInterval, classUnit.endTime);

        const rows = {
          from: indexStart + Math.floor(indexStart / cellSplit) + 3,
          to: indexEnd + Math.ceil(indexEnd / cellSplit) + 2,
        };

        const cols = {
          from: weekdayClasses[classWeekday].lastCol,
          to: weekdayClasses[classWeekday].totalColsToShow + 1,
        };

        weekdayClasses[classWeekday].classes.push({
          data: {
            id: classUnit.ucId,
            groupId: classUnit.groupId,
            initials: classUnit.ucInitials,
            periodDays: classUnit.periodDays,
            color: classUnit.ucColor,
            selected: classUnit.selected,
            locked: classUnit.locked,
          },
          schedule: classUnit,
          position: { cols, rows },
        });
      } else {
        weekdayClasses[classWeekday].totalCols++;
      }
    }
  });

  return weekdayClasses;
};

export const willOverlap = (classUnit, classItems) => {
  return classItems.some(
    (classItem) =>
      isBefore(classItem.startTime, classUnit.endTime) &&
      isAfter(classItem.endTime, classUnit.startTime)
  );
};

export const getDetailsOrientation = (startTime: string, endTime: string) => {
  const hoursDuration = moment(endTime, 'HH:mm:ss').diff(moment(startTime, 'HH:mm:ss'), 'hours');
  if (hoursDuration <= 1) {
    return 'horizontal';
  } else {
    return 'vertical';
  }
};

export const getWeekdaysCells = (rows, cellSplit, cellHeight, cellGap) => {
  const arrayRows: string[] = [`repeat(1, ${cellGap}px [col-start])`];

  for (let i = 0; i <= rows / cellSplit; i++) {
    arrayRows.push(
      `repeat(${cellSplit}, ${cellHeight}px [col-start]) repeat(1, ${cellGap}px [col-start])`
    );
  }

  return arrayRows.join(' ');
};
