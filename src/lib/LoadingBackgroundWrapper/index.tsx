import React from 'react';
import styled from 'styled-components';

import AnimatedBackground from '../AnimatedBackground';

import theme from '../theme';

interface IProps {
  loading?: boolean;
  solidBackground?: boolean;
  maskProps?: { lineBreak?: string; widths?: string[] };
  children?: any;
  length?: number;
}

const LoadingBackgroundWrapper: React.FC<IProps> = ({
  solidBackground,
  children,
  loading,
  maskProps,
  length,
}) => {
  const createLoadingStructure = (n) => {
    let loadingStruture: any = [];

    for (let key = 0; key < n; key++) {
      loadingStruture.push(
        <AnimatedBackground
          key={`loading_background_wrapper_${key}`}
          height="25px"
          width={
            maskProps?.widths
              ? maskProps?.widths[key] || maskProps?.widths[maskProps?.widths.length - 1]
              : '100%'
          }
        />
      );
    }

    return loadingStruture;
  };

  if (!loading && children) {
    return children;
  }

  return (
    <Wrapper lineBreak={maskProps?.lineBreak} solidBackground={solidBackground}>
      {children &&
        !length &&
        children.props.children.map((child, key) => (
          <AnimatedBackground
            key={`loading_background_wrapper_${key}`}
            height="25px"
            width={
              maskProps?.widths
                ? maskProps?.widths[key] || maskProps?.widths[maskProps?.widths.length - 1]
                : '100%'
            }
          />
        ))}
      {length && createLoadingStructure(length)}
    </Wrapper>
  );
};

export default LoadingBackgroundWrapper;

const Wrapper = styled.div<{ lineBreak?: string; solidBackground?: boolean }>`
  background-color: ${({ theme, solidBackground }) =>
    solidBackground ? theme.colors.softGrey : 'transparent'};
  padding: ${({ solidBackground }) => (solidBackground ? '20px' : '0')};

  > div:not(:first-child) {
    margin-top: ${({ lineBreak }) => (lineBreak ? lineBreak : '0')};
  }
`;
