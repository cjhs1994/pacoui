/**
 * RestrictAccessOverlay component
 *
 * @author Rafael Guedes <rguedes@ubiwhere.com>
 *
 */

import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

import Lottie from 'lottie-react-web';
import animation from './animation.json';

import Text from '../Text';
import theme from '../theme';

interface IRestrictAccessOverlayInterface {
  t?: any;
  restrictionStatus: string;
  restrictionMotive?: string | React.ReactNode | null;
  onComplete: () => void;
}

const RestrictAccessOverlay: React.FC<IRestrictAccessOverlayInterface> = ({
  t,
  restrictionStatus,
  restrictionMotive,
  onComplete,
}) => {
  const [animationComplete, setAnimationComplete] = useState(false);

  const getFrames = () => {
    if (restrictionStatus === 'allowed') {
      return [326, 418];
    } else if (restrictionStatus === 'restricted') {
      return [705, 823];
    } else {
      return [0, 325];
    }
  };

  useEffect(() => {
    setAnimationComplete(false);
  }, [restrictionStatus]);

  return (
    <Wrapper>
      <Lottie
        options={{
          loop: restrictionStatus ? false : true,
          animationData: animation,
        }}
        width={'180px'}
        height={'180px'}
        segments={getFrames()}
        eventListeners={[
          {
            eventName: 'complete',
            callback: () => {
              setAnimationComplete(true);
              onComplete();
            },
          },
        ]}
      />
      {animationComplete && restrictionStatus === 'restricted' && (
        <>
          <RestrictAccessMessage>
            <Text size="large" weight="medium" color="dangerRed">
              {t ? t('generic.restrictedAccess') : 'Acesso restrito'}
            </Text>
          </RestrictAccessMessage>
          {restrictionMotive && (
            <RestrictAccessMotive>
              <Text size="medium" color="dangerRed">
                {restrictionMotive}
              </Text>
            </RestrictAccessMotive>
          )}
        </>
      )}
    </Wrapper>
  );
};

export default RestrictAccessOverlay;

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.white};
`;

const RestrictAccessMessage = styled.div`
  margin-top: 40px;
`;

const RestrictAccessMotive = styled.div`
  text-align: center;
  margin-top: 12px;
`;
